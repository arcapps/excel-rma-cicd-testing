import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProblemPageRoutingModule } from './problem-routing.module';

import { ProblemPage } from './problem.page';
import { MaterialModule } from '../../material/material.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { CommonComponentModule } from 'src/app/common/components/common-component.module';

@NgModule({
  imports: [
    CommonComponentModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ProblemPageRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    MatGridListModule,
  ],
  declarations: [ProblemPage],
})
export class ProblemPageModule {}
