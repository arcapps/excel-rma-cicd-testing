import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ProblemService } from '../services/problem/problem.service';
import { ProblemDataSource } from './problem-datasource';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PopoverController } from '@ionic/angular';
import { AddProblemPage } from '../add-problem/add-problem.page';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-problem',
  templateUrl: './problem.page.html',
  styleUrls: ['./problem.page.scss'],
})
export class ProblemPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: ProblemDataSource;
  displayedColumns = ['sr_no', 'problem_name', 'delete'];
  search: string = '';
  breakpoint: number;
  firstColumn: number;
  secondColumn: number;
  thirdColumn: number;

  constructor(
    private readonly location: Location,
    private readonly problemService: ProblemService,
    private readonly popoverController: PopoverController,
    private readonly route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.dataSource = new ProblemDataSource(this.problemService);
    this.dataSource.loadItems();
    this.loadResize();
  }
  onResize(event) {
    // eslint-disable-next-line
    if (event.target.innerWidth >= 1344) {
      this.firstColumn = 1;
      this.secondColumn = 8.5;
      this.thirdColumn = 1.5;
      return;
    }
    if (event.target.innerWidth >= 1024 && event.target.innerWidth < 1343) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (event.target.innerWidth <= 1024 && event.target.innerWidth > 768) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (event.target.innerWidth < 768) {
      this.firstColumn = 12;
      this.secondColumn = 12;
      this.thirdColumn = 12;
      return;
    }
  }
  loadResize() {
    if (window.innerWidth >= 1344) {
      this.firstColumn = 1;
      this.secondColumn = 8.5;
      this.thirdColumn = 1.5;
      return;
    }
    if (window.innerWidth >= 1024 && window.innerWidth < 1343) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (window.innerWidth <= 1024 && window.innerWidth > 768) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (window.innerWidth < 768) {
      this.firstColumn = 12;
      this.secondColumn = 12;
      this.thirdColumn = 12;
      return;
    }
  }
  navigateBack() {
    this.location.back();
  }

  async addProblem() {
    const popover = await this.popoverController.create({
      component: AddProblemPage,
      componentProps: {
        passedFrom: 'add',
      },
      cssClass: 'wider-popover',
    });

    await popover.present();
    popover.onDidDismiss().then(res => {
      if (res.data.success)
        this.dataSource.loadItems(
          this.search,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
        );
    });
  }

  async updateProblem(uuid: string) {
    const popover = await this.popoverController.create({
      component: AddProblemPage,
      componentProps: {
        passedFrom: 'update',
        uuid,
      },
      cssClass: 'wider-popover',
    });

    await popover.present();
    popover.onDidDismiss().then(res => {
      if (res.data.success)
        this.dataSource.loadItems(
          this.search,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
        );
    });
  }

  deleteProblem(uuid: string) {
    this.problemService.deleteProblem(uuid).subscribe({
      next: () => {
        this.dataSource.loadItems(
          this.search,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
        );
      },
    });
  }

  setFilter() {
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.search,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize,
    );
  }

  getUpdate(event: any) {
    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      this.search,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize,
    );
  }
  isVisible: boolean = false;
  ProblemList() {
    const SalseInvoiceSidebarID = document.getElementById('ProblemList');
    const problemFilter = document.getElementById('problemFilter');
    const problemClose = document.getElementById('problemClose');
    const problemTable = document.getElementById('problemTable');

    if (SalseInvoiceSidebarID.classList.contains('active')) {
      this.isVisible = false;
      SalseInvoiceSidebarID.classList.remove('active');
      problemClose.style.display = 'none';
      problemFilter.style.display = 'block';
      problemTable.style.height = '75.5vh';
    } else {
      SalseInvoiceSidebarID.classList.add('active');
      this.isVisible = true;
      problemClose.style.display = 'block';
      problemFilter.style.display = 'none';
      problemTable.style.height = '69vh';
    }
  }
}
