import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';

import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { CsvJsonService } from '../../api/csv-json/csv-json.service';
import { ValidateInputSelected } from '../../common/pipes/validators';
import {
  STOCK_AVAILABILITY_CSV_FILE,
  STOCK_AVAILABILITY_DOWNLOAD_HEADERS,
  WAREHOUSES,
} from '../../constants/app-string';
import { SalesService } from '../../sales-ui/services/sales.service';
import { StockLedgerService } from '../services/stock-ledger/stock-ledger.service';
import { StockAvailabilityDataSource } from './stock-availability-datasource';
@Component({
  selector: 'app-stock-availability',
  templateUrl: './stock-availability.page.html',
  styleUrls: ['./stock-availability.page.scss'],
})
export class StockAvailabilityPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource: StockAvailabilityDataSource;
  defaultCompany: string;
  displayedColumns = [
    'sr_no',
    'item_name',
    'item_code',
    'item_group',
    'item_brand',
    'warehouse',
    'actual_qty',
  ];
  sortQuery: any = {};
  stockAvailabilityForm: UntypedFormGroup = new UntypedFormGroup({
    item_name: new UntypedFormControl(),
    warehouse: new UntypedFormControl(),
    item_group: new UntypedFormControl(),
    item_brand: new UntypedFormControl(),
    zero_stock: new UntypedFormControl(),
  });
  filteredItemNameList: any[];
  filteredWarehouseList: any[];
  filteredItemGroupList: any[];
  filteredItemBrandList: any[];
  validateInput: any = ValidateInputSelected;

  get f() {
    return this.stockAvailabilityForm.controls;
  }

  constructor(
    private readonly location: Location,
    private readonly salesService: SalesService,
    private readonly stockLedgerService: StockLedgerService,
    private readonly route: ActivatedRoute,
    private readonly csvService: CsvJsonService,
  ) {}

  ngOnInit() {
    this.setAutoComplete();
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.dataSource = new StockAvailabilityDataSource(this.stockLedgerService);
    // this.setFilter();
  }

  navigateBack() {
    this.location.back();
  }

  setAutoComplete() {
    this.f.item_name.valueChanges
      .pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(value => {
          return this.salesService.getItemList(value);
        }),
      )
      .subscribe(res => (this.filteredItemNameList = res));

    this.f.warehouse.valueChanges
      .pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(value => {
          return this.salesService.getStore().getItemAsync(WAREHOUSES, value);
        }),
      )
      .subscribe(res => (this.filteredWarehouseList = res));

    this.f.item_group.valueChanges
      .pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(value => {
          return this.salesService.getItemGroupList(value);
        }),
        switchMap(data => {
          return of(data);
        }),
      )
      .subscribe(res => (this.filteredItemGroupList = res));

    this.f.item_brand.valueChanges
      .pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(value => {
          return this.salesService.getItemBrandList(value);
        }),
        switchMap(data => {
          return of(data);
        }),
      )
      .subscribe(res => (this.filteredItemBrandList = res));
  }

  clearFilters() {
    this.stockAvailabilityForm.reset();
    this.dataSource.reset();
    // this.paginator.pageIndex = 0;
    // this.paginator.pageSize = 30;

    // this.dataSource.loadItems(
    //   this.paginator.pageIndex,
    //   this.paginator.pageSize,
    //   undefined,
    //   this.sortQuery,
    // );
  }

  getUpdate(event) {
    const query: any = {};
    if (this.f.item_name.value)
      query.item_code = this.f.item_name.value.item_code;
    if (this.f.warehouse.value) query.warehouse = this.f.warehouse.value;
    if (this.f.item_group.value) query.item_group = this.f.item_group.value;
    if (this.f.item_brand.value) query.item_brand = this.f.item_brand.value;
    if (this.f.zero_stock.value) query.zero_stock = this.f.zero_stock.value;

    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
      this.sortQuery,
    );
  }

  setFilter(event?: any) {
    const query: any = {};
    if (this.f.item_name.value)
      query.item_code = this.f.item_name.value.item_code;
    if (this.f.warehouse.value) query.warehouse = this.f.warehouse.value;
    if (this.f.item_group.value) query.item_group = this.f.item_group.value;
    if (this.f.item_brand.value) query.item_brand = this.f.item_brand.value;
    if (this.f.zero_stock.value) query.zero_stock = this.f.zero_stock.value;

    if (event) {
      for (const key of Object.keys(event)) {
        if (key === 'active' && event.direction !== '') {
          this.sortQuery[event[key]] = event.direction;
        }
      }
    }

    this.sortQuery =
      Object.keys(this.sortQuery).length === 0
        ? { item_code: 'DESC' }
        : this.sortQuery;

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
      this.sortQuery,
    );
  }

  downloadServiceInvoices() {
    const result: any = this.serializeStockAvailabilityObject(
      this.dataSource.data,
    );
    this.csvService.downloadAsCSV(
      result,
      STOCK_AVAILABILITY_DOWNLOAD_HEADERS,
      `${STOCK_AVAILABILITY_CSV_FILE}`,
    );
  }

  serializeStockAvailabilityObject(data: any) {
    const serializedArray: any = [];
    data.forEach(element => {
      if (
        element.item?.item_name &&
        element.item?.item_code &&
        element.item?.item_group &&
        element.item?.brand &&
        element.warehouse &&
        element.stock_availability
      ) {
        const obj1: any = {
          item_name: element.item.item_name,
          item_code: element.item.item_code,
          item_group: element.item.item_group,
          brand: element.item.brand,
          warehouse: element.warehouse,
          stock_availability: element.stock_availability,
        };
        serializedArray.push(obj1);
      }
    });
    return serializedArray;
  }

  getItemName(option: any) {
    return option ? option.item_name : '';
  }
  isVissible: boolean = false;
  stocAvality() {
    const filterShowHideID = document.getElementById('stock_avality');
    const AvailabilityFilter = document.getElementById('AvailabilityFilter');
    const AvailabilityClose = document.getElementById('AvailabilityClose');
    const AvailabilityTable = document.getElementById('AvailabilityTable');

    if (filterShowHideID.classList.contains('active')) {
      filterShowHideID.classList.remove('active');
      this.isVissible = false;
      AvailabilityClose.style.display = 'none';
      AvailabilityFilter.style.display = 'block';
      AvailabilityTable.style.height = '76vh';
    } else {
      filterShowHideID.classList.add('active');
      this.isVissible = true;
      AvailabilityClose.style.display = 'block';
      AvailabilityFilter.style.display = 'none';
      AvailabilityTable.style.height = '69vh';
    }
  }
}
