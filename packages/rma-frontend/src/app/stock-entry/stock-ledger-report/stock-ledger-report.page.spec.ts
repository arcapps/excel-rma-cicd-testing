import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { of } from 'rxjs';
import { CsvJsonService } from '../../api/csv-json/csv-json.service';
import { MaterialModule } from '../../material/material.module';
import { SalesService } from '../../sales-ui/services/sales.service';
import { StockEntryService } from '../services/stock-entry/stock-entry.service';
import { StockLedgerService } from '../services/stock-ledger/stock-ledger.service';
import { StockLedgerReportPage } from './stock-ledger-report.page';

describe('StockLedgerReportPage', () => {
  let component: StockLedgerReportPage;
  let fixture: ComponentFixture<StockLedgerReportPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [StockLedgerReportPage],
        imports: [
          IonicModule.forRoot(),
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          RouterTestingModule,
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: SalesService,
            useValue: {
              getItemList: () => of([{}]),
              getLedgerCount: (...args) => of(0),
              getStockLedger: () => of(),
              getStore: () => ({
                getItemAsync: (...args) => of([]),
              }),
              getItemGroupList: (...args) => of([]),
              getItemBrandList: (...args) => of([]),
            },
          },
          {
            provide: StockEntryService,
            useValue: {
              getVoucherTypeList: (...args) => of([]),
            },
          },
          {
            provide: StockLedgerService,
            useValue: {
              listLedgerReport: (...args) => of([]),
            },
          },
          {
            provide: CsvJsonService,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(StockLedgerReportPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
