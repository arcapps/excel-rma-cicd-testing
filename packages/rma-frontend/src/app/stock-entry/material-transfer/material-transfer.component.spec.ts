import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  Renderer2,
} from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { of } from 'rxjs';
import { TimeService } from '../../api/time/time.service';
import { MaterialModule } from '../../material/material.module';
import { SalesService } from '../../sales-ui/services/sales.service';
import { SettingsService } from '../../settings/settings.service';
import { StockEntryService } from '../services/stock-entry/stock-entry.service';
import { MaterialTransferComponent } from './material-transfer.component';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

describe('MaterialTransferComponent', () => {
  let component: MaterialTransferComponent;
  let fixture: ComponentFixture<MaterialTransferComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [MaterialTransferComponent],
        imports: [
          IonicModule.forRoot(),
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          BrowserDynamicTestingModule,
          HttpClientTestingModule,
          RouterTestingModule,
        ],
        providers: [
          {
            provide: SalesService,
            useValue: {
              getCustomerList: (...args) => of([{}]),
              getSalesInvoice: (...args) =>
                of({ items: [], delivered_items_map: {} }),
              getWarehouseList: (...args) => of([{}]),
              getDefaultTerritory: (...args) => of([{}]),
              getStore: () => ({
                getItem: (...args) => Promise.resolve('ITEM'),
                getItemAsync: (...args) => of({}),
              }),
            },
          },
          {
            provide: TimeService,
            useValue: {},
          },
          {
            provide: StockEntryService,
            useValue: {
              getFilteredAccountingDimensions: (...args) => of([{}]),
            },
          },
          {
            provide: Renderer2,
            useValue: {},
          },
          {
            provide: SettingsService,
            useValue: {
              relayAccountsOperation: (...args) => of([]),
            },
          },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      }).compileComponents();

      fixture = TestBed.createComponent(MaterialTransferComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
