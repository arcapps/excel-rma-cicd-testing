import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { StorageService } from '../api/storage/storage.service';
import { ValidateInputSelected } from '../common/pipes/validators';
import { DEFAULT_COMPANY } from '../constants/storage';
import { SalesService } from '../sales-ui/services/sales.service';
import { CreditLimitDataSource } from './credit-limit-datasource';
import { UpdateCreditLimitComponent } from './update-credit-limit/update-credit-limit.component';

@Component({
  selector: 'app-credit-limit',
  templateUrl: './credit-limit.page.html',
  styleUrls: ['./credit-limit.page.scss'],
})
export class CreditLimitPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: CreditLimitDataSource;
  displayedColumns = [
    'name',
    'customer_name',
    'credit_limits',
    'extended_credit_limit',
    'expiry_date',
    'set_by',
    'set_on',
  ];
  filteredCustomerList: any[];
  customerProfileForm: UntypedFormGroup = new UntypedFormGroup({
    customer: new UntypedFormControl(),
  });
  validateInput: any = ValidateInputSelected;
  id: string = '';
  firstColumn: number;
  secondColumn: number;
  thirdColumn: number;
  get f() {
    return this.customerProfileForm.controls;
  }

  constructor(
    private readonly location: Location,
    private readonly salesService: SalesService,
    private readonly storage: StorageService,
    private readonly popoverController: PopoverController,
    private readonly route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.dataSource = new CreditLimitDataSource(this.salesService);
    this.dataSource.loadItems(undefined, undefined, 0, 30);

    this.f.customer.valueChanges
      .pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(value => {
          return this.salesService
            .getCustomerList(value)
            .pipe(map(res => res.docs));
        }),
      )
      .subscribe(res => (this.filteredCustomerList = res));
    this.loadResize();
  }
  onResize(event) {
    // eslint-disable-next-line

    if (event.target.innerWidth >= 1421) {
      this.firstColumn = 1;
      this.secondColumn = 9;
      this.thirdColumn = 2;
      return;
    }
    if (event.target.innerWidth >= 1024 && event.target.innerWidth < 1420) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (event.target.innerWidth <= 1024 && event.target.innerWidth > 768) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (event.target.innerWidth < 768) {
      this.firstColumn = 12;
      this.secondColumn = 12;
      this.thirdColumn = 12;
      return;
    }
  }
  loadResize() {
    if (window.innerWidth >= 1421) {
      this.firstColumn = 1;
      this.secondColumn = 9;
      this.thirdColumn = 2;
      return;
    }
    if (window.innerWidth >= 1024 && window.innerWidth < 1420) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (window.innerWidth <= 1024 && window.innerWidth > 768) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (window.innerWidth < 768) {
      this.firstColumn = 12;
      this.secondColumn = 12;
      this.thirdColumn = 12;
      return;
    }
  }

  clearFilters() {
    this.customerProfileForm.reset();
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;
    this.dataSource.loadItems(
      undefined,
      undefined,
      this.paginator.pageIndex || undefined,
      this.paginator.pageSize || undefined,
    );
  }

  navigateBack() {
    this.location.back();
  }

  async updateCreditLimitDialog(row?: any) {
    const defaultCompany = await this.storage.getItem(DEFAULT_COMPANY);
    const creditLimits: { credit_limit: number; company: string }[] =
      row.credit_limits || [];
    let creditLimit = 0;
    for (const limit of creditLimits) {
      if (limit.company === defaultCompany) {
        creditLimit = limit.credit_limit;
      }
    }
    const popover = await this.popoverController.create({
      component: UpdateCreditLimitComponent,
      componentProps: {
        uuid: row.uuid,
        customer: row.name,
        baseCreditLimit: row.baseCreditLimitAmount || 0,
        currentCreditLimit: creditLimit,
        expiryDate: row.tempCreditLimitPeriod,
      },
    });
    popover.onDidDismiss().then(() => {
      this.dataSource.loadItems();
    });
    return await popover.present();
  }

  setFilter(event: any) {
    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      event.option.value,
      this.sort.direction,
      event.pageIndex || 0,
      event.pageSize || 30,
    );
  }

  getUpdate(event: any) {
    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      event.option.value,
      undefined,
      event?.pageIndex || undefined,
      event?.pageSize || undefined,
    );
  }
  isVisible: boolean = false;
  customarlList() {
    const SalseInvoiceSidebarID = document.getElementById('customarlList');
    const CustomerFilter = document.getElementById('CustomerFilter');
    const CustomerClose = document.getElementById('CustomerClose');
    const CustomerTable = document.getElementById('CustomerTable');

    if (SalseInvoiceSidebarID.classList.contains('active')) {
      this.isVisible = false;
      SalseInvoiceSidebarID.classList.remove('active');
      CustomerFilter.style.display = 'block';
      CustomerClose.style.display = 'none';
      CustomerTable.style.height = '75.5vh';
    } else {
      this.isVisible = true;
      SalseInvoiceSidebarID.classList.add('active');
      CustomerFilter.style.display = 'none';
      CustomerClose.style.display = 'block';
      CustomerTable.style.height = '69vh';
    }
  }
}
