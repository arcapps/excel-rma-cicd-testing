// import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
// import { TestBed, waitForAsync } from '@angular/core/testing';

// import { RouterTestingModule } from '@angular/router/testing';
// import { Platform } from '@ionic/angular';

// import { HttpClientTestingModule } from '@angular/common/http/testing';
// import { Subscription, of } from 'rxjs';
// import { STORAGE_TOKEN } from './api/storage/storage.service';
// import { AppComponent } from './app.component';
// import { AppService } from './app.service';

// @Pipe({ name: 'formatTime' })
// class MockPipe implements PipeTransform {
//   transform(value: string) {
//     return value;
//   }
// }

// describe('AppComponent', () => {
//   beforeEach(
//     waitForAsync(() => {
//       TestBed.configureTestingModule({
//         declarations: [AppComponent, MockPipe],
//         schemas: [CUSTOM_ELEMENTS_SCHEMA],
//         providers: [
//           {
//             provide: AppService,
//             useValue: {
//               getMessage: (...args) => of({}),
//               setInfoLocalStorage: (...args) => null,
//               checkUserProfile: () => of({ roles: [] }),
//             },
//           },
//           {
//             provide: Platform,
//             useValue: {
//               ready: () => Promise.resolve(),
//               backButton: {
//                 subscribeWithPriority: (...args) => new Subscription(),
//               },
//             },
//           },
//           {
//             provide: STORAGE_TOKEN,
//             useValue: {
//               getItem: (...args) => Promise.resolve('ITEM'),
//             },
//           },
//         ],
//         imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule],
//       }).compileComponents();
//     }),
//   );

//   it('should create the app', async () => {
//     const fixture = TestBed.createComponent(AppComponent);
//     const app = fixture.debugElement.componentInstance;
//     expect(app).toBeTruthy();
//   });
// });

import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { Platform } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Subscription, of } from 'rxjs';

import { STORAGE_TOKEN } from './api/storage/storage.service';
import { AppComponent } from './app.component';
import { AppService } from './app.service';

@Pipe({ name: 'formatTime' })
class MockPipe implements PipeTransform {
  transform(value: string) {
    return value;
  }
}

describe('AppComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AppComponent, MockPipe],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          // Provide a mock for AppService
          {
            provide: AppService,
            useValue: {
              getMessage: (...args) => of({}),
              setInfoLocalStorage: (...args) => null,
              checkUserProfile: () => of({ roles: [] }),
            },
          },
          // Provide a mock for Platform
          {
            provide: Platform,
            useValue: {
              ready: () => Promise.resolve(),
              backButton: {
                subscribeWithPriority: (...args) => new Subscription(),
              },
            },
          },
          // Provide a mock for STORAGE_TOKEN
          {
            provide: STORAGE_TOKEN,
            useValue: {
              getItem: (...args) => Promise.resolve('ITEM'),
            },
          },
        ],
        imports: [
          RouterTestingModule.withRoutes([]),
          HttpClientTestingModule,
          MatDialogModule,
        ],
      }).compileComponents();
    }),
  );

  // Your test cases go here
  it('should create the app', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
