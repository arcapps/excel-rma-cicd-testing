export interface BrandLimit {
  uuid?: string | number;
  brand_name: string;
  customer_name: string;
  limit_amount: string;
}
