import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { PERMISSION_STATE } from '../../../constants/permission-roles';
import { CsvJsonService } from '../../../api/csv-json/csv-json.service';
import {
  CSV_FILE_TYPE,
  DELIVERED_SERIALS_DATE_FIELDS,
  SERIAL_DOWNLOAD_HEADERS,
} from '../../../constants/app-string';
import { PurchaseService } from '../../../purchase-ui/services/purchase.service';
import { SalesService } from '../../../sales-ui/services/sales.service';
import { CommonDeliveredSerialsDataSource } from './delivered-serials-datasource';
import { StockEntryService } from '../../../stock-entry/services/stock-entry/stock-entry.service';

@Component({
  selector: 'app-delivered-serials',
  templateUrl: './delivered-serials.component.html',
  styleUrls: ['./delivered-serials.component.scss'],
})
export class DeliveredSerialsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input()
  deliveredSerialsState: DeliveredSerialsState = {
    deliveredSerialsDisplayedColumns: [],
  };
  index: number = 0;
  size: number = 10;
  permissionState = PERMISSION_STATE;
  deliveredSerialsSearch: string;
  deliveredSerialsDataSource: CommonDeliveredSerialsDataSource;
  constructor(
    private readonly salesService: SalesService,
    private readonly stockService: StockEntryService,
    private readonly purchaseService: PurchaseService,
    private readonly csvService: CsvJsonService,
  ) {}

  ngOnInit() {
    this.deliveredSerialsDataSource = new CommonDeliveredSerialsDataSource(
      this.salesService,
      this.purchaseService,
      this.stockService,
    );
  }
  downloadSerials() {
    this.csvService.downloadAsCSV(
      this.deliveredSerialsDataSource.data,
      SERIAL_DOWNLOAD_HEADERS,
      `${this.deliveredSerialsState.uuid || ''}${CSV_FILE_TYPE}`,
    );
  }

  getDeliveredSerials() {
    this.deliveredSerialsDataSource.loadItems(
      this.deliveredSerialsState,
      this.deliveredSerialsSearch,
      0,
      this.paginator.pageSize,
    );
  }

  setFilter() {
    this.getDeliveredSerials();
  }

  getUpdate(event) {
    this.index = event.pageIndex;
    this.size = event.pageSize;
    this.deliveredSerialsDataSource.loadItems(
      this.deliveredSerialsState,
      this.deliveredSerialsSearch,
      this.index,
      this.size,
    );
  }

  getDateCell(cell) {
    return DELIVERED_SERIALS_DATE_FIELDS.includes(cell) ? true : false;
  }
  formatName(itemName: string): string {
    // Remove underscores and convert to uppercase
    return itemName.replace(/_/g, ' ').toUpperCase();
  }

  displayNameMappings: { [key: string]: string } = {
    sr_no: 'Serial Number',
    serial_no: 'Serial Number',
    item_name: 'Item Name',
    warehouse: 'Warehouse',
    purchaseWarrantyDate: 'Purchase Warranty Date',
    purchasedOn: 'Purchased On',
    salesWarrantyDate: 'Sales Warranty Date',
    soldOn: 'Sold On',
  };
}

export interface DeliveredSerialsState {
  type?: string;
  uuid?: string;
  deliveredSerialsDisplayedColumns?: string[];
}
