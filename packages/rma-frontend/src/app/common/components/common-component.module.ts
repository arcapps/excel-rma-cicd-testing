import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MaterialModule } from '../../material/material.module';
import { AppCommonModule } from '../app-common.module';
import { AssignSerialComponent } from './assign-serial/assign-serial.component';
import { DeliveredSerialsComponent } from './delivered-serials/delivered-serials.component';
import { AddSalesInvoiceBtnComponent } from './add-sales-invoice/add-sales-invoice.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AssignSerialComponent,
    DeliveredSerialsComponent,
    AddSalesInvoiceBtnComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    IonicModule,
    AppCommonModule,
    FormsModule,
    RouterModule,
  ],
  providers: [],
  exports: [
    AssignSerialComponent,
    DeliveredSerialsComponent,
    AddSalesInvoiceBtnComponent,
  ],
})
export class CommonComponentModule {}
