import { Component, OnInit } from '@angular/core';
import { PERMISSION_STATE } from 'src/app/constants/permission-roles';

@Component({
  selector: 'app-sales-invoice-btn',
  templateUrl: './add-sales-invoice-btn.component.html',
  styleUrls: ['./add-sales-invoice.component.scss'],
})
export class AddSalesInvoiceBtnComponent implements OnInit {
  permissionState: any = PERMISSION_STATE;

  constructor() {}

  ngOnInit() {}
}
