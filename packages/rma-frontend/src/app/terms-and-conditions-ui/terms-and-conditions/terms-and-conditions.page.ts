import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { Location } from '@angular/common';
import { AddTermsAndConditionsPage } from '../add-terms-and-conditions/add-terms-and-conditions.page';
import { TermsAndConditionsService } from '../services/TermsAndConditions/terms-and-conditions.service';
import { TermsAndConditionsDataSource } from './terms-and-conditions-datasource';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.page.html',
  styleUrls: ['./terms-and-conditions.page.scss'],
})
export class TermsAndConditionsPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: TermsAndConditionsDataSource;
  displayedColumns = ['sr_no', 'terms_and_conditions_name', 'delete'];
  search: string;
  firstColumn: number;
  secondColumn: number;
  thirdColumn: number;
  constructor(
    private location: Location,
    private readonly termsAndConditionsService: TermsAndConditionsService,
    private popoverController: PopoverController,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.search = '';
    this.dataSource = new TermsAndConditionsDataSource(
      this.termsAndConditionsService,
    );
    this.dataSource.loadItems();
    this.loadResize();
  }
  onResize(event) {
    // eslint-disable-next-line
    if (event.target.innerWidth >= 1344) {
      this.firstColumn = 1.5;
      this.secondColumn = 8;
      this.thirdColumn = 1.5;
      return;
    }
    if (event.target.innerWidth >= 1024 && event.target.innerWidth < 1343) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (event.target.innerWidth <= 1024 && event.target.innerWidth > 768) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (event.target.innerWidth < 768) {
      this.firstColumn = 12;
      this.secondColumn = 12;
      this.thirdColumn = 12;
      return;
    }
  }
  loadResize() {
    if (window.innerWidth >= 1344) {
      this.firstColumn = 1.5;
      this.secondColumn = 8;
      this.thirdColumn = 1.5;
      return;
    }
    if (window.innerWidth >= 1024 && window.innerWidth < 1343) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (window.innerWidth <= 1024 && window.innerWidth > 768) {
      this.firstColumn = 2;
      this.secondColumn = 6;
      this.thirdColumn = 4;
      return;
    }
    if (window.innerWidth < 768) {
      this.firstColumn = 12;
      this.secondColumn = 12;
      this.thirdColumn = 12;
      return;
    }
  }
  navigateBack() {
    this.location.back();
  }

  async addTermsAndConditions() {
    const popover = await this.popoverController.create({
      component: AddTermsAndConditionsPage,
      componentProps: {
        passedFrom: 'add',
      },
      cssClass: 'wider-popover',
    });

    await popover.present();
    popover.onDidDismiss().then(res => {
      if (res.data.success)
        this.dataSource.loadItems(
          this.search,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
        );
    });
  }

  async updateTermsAndConditions(uuid: string) {
    const popover = await this.popoverController.create({
      component: AddTermsAndConditionsPage,
      componentProps: {
        passedFrom: 'update',
        uuid,
      },
      cssClass: 'wider-popover',
    });

    await popover.present();
    popover.onDidDismiss().then(res => {
      if (res.data.success)
        this.dataSource.loadItems(
          this.search,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
        );
    });
  }

  deleteTermsAndConditions(uuid: string) {
    this.termsAndConditionsService.deleteTermsAndConditions(uuid).subscribe({
      next: res => {
        this.dataSource.loadItems(
          this.search,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
        );
      },
    });
  }

  setFilter(event?) {
    this.dataSource.loadItems(
      this.search,
      this.sort.direction,
      event?.pageIndex || 0,
      event?.pageSize || 30,
    );
  }
  isVissible: boolean = false;
  tramaAndCondation() {
    const SalseInvoiceSidebarID = document.getElementById('tramaAndCondation');
    const tramsFilter = document.getElementById('tramsFilter');
    const tramsClose = document.getElementById('tramsClose');
    const tramsTable = document.getElementById('tramsTable');

    if (SalseInvoiceSidebarID.classList.contains('active')) {
      this.isVissible = false;
      SalseInvoiceSidebarID.classList.remove('active');
      tramsClose.style.display = 'none';
      tramsFilter.style.display = 'block';
      tramsTable.style.height = '75.5vh';
    } else {
      this.isVissible = true;
      SalseInvoiceSidebarID.classList.add('active');
      tramsClose.style.display = 'block';
      tramsFilter.style.display = 'none';
      tramsTable.style.height = '69vh';
    }
  }
}
