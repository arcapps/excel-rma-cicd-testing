import { Component, OnInit } from '@angular/core';
import { InvoiceWarrantyDataSource } from './invoice-warranty-datasource';
import { ActivatedRoute } from '@angular/router';
import { SalesService } from '../../services/sales.service';

@Component({
  selector: 'sales-invoice-warranty',
  templateUrl: './invoice-warranty.component.html',
  styleUrls: ['./invoice-warranty.component.scss'],
})
export class InvoiceWarrantyComponent implements OnInit {
  invoiceUuid: string;
  displayedColumns = [
    'claimNo',
    'claimedItem',
    'serial',
    'claimsReceivedDate',
    'status',
    'deliveryDate',
    'createdBy',
  ];

  dataSource: InvoiceWarrantyDataSource;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly salesService: SalesService,
  ) {}

  ngOnInit() {
    this.dataSource = new InvoiceWarrantyDataSource();
    this.invoiceUuid = this?.route?.snapshot?.params?.invoiceUuid;
    this.getWarrantyClaims();
  }

  getWarrantyClaims() {
    this.salesService.getWarrantyClaimsBySINV(this.invoiceUuid).subscribe({
      next: res => {
        if (!res) {
          this.dataSource.loadItems([]);
          return;
        }
        this.dataSource.loadItems(res);
      },
      error: err => {},
    });
  }
  getTime(timeString: string) {
    const time = timeString.split(':');
    const hours = Number(time[0]);
    const minutes = Number(time[1]);
    const seconds = Number(time[2].split('.')[0]); // Get the seconds without milliseconds

    // Determine if it's AM or PM
    const ampm = hours >= 12 ? 'PM' : 'AM';

    // Convert to 12-hour format
    const tm = hours % 12 === 0 ? 12 : hours % 12;
    // Pad single-digit hours, minutes, and seconds with leading zeros
    const formattedTime = `${tm
      .toString()
      .padStart(2, '0')}:${minutes
      .toString()
      .padStart(2, '0')}:${seconds.toString().padStart(2, '0')} ${ampm}`;
    return formattedTime;
  }
}
