import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { ValidateInputSelected } from '../../common/pipes/validators';
import { SalesService } from '../services/sales.service';
import { SalesReturnService } from '../view-sales-invoice/sales-return/sales-return.service';
import { SalesReturnListDataSource } from './sales-return-list.datasource';

@Component({
  selector: 'app-sales-return',
  templateUrl: './sales-return.page.html',
  styleUrls: ['./sales-return.page.scss'],
})
export class SalesReturnPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  statusList = ['Canceled', 'Returned'];
  total = 0;
  dataSource: SalesReturnListDataSource;
  customerList: any;
  displayedColumns = [
    'sr_no',
    'name',
    'posting_date',
    'title',
    'total',
    'status',
    'owner',
    'modified_by',
  ];
  filters: any = [['is_return', '=', '1']];
  countFilter: any = [['Delivery Note', 'is_return', '=', '1']];
  salesReturnForm: UntypedFormGroup = new UntypedFormGroup({
    start_date: new UntypedFormControl(),
    end_date: new UntypedFormControl(),
    customer: new UntypedFormControl(),
    name: new UntypedFormControl(),
    status: new UntypedFormControl(),
  });
  filteredCustomerList: any[];
  validateInput: any = ValidateInputSelected;

  get f() {
    return this.salesReturnForm.controls;
  }

  constructor(
    private readonly location: Location,
    private readonly salesReturnService: SalesReturnService,
    private readonly salesService: SalesService,
    private readonly route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.dataSource = new SalesReturnListDataSource(this.salesReturnService);
    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.filters,
      this.countFilter,
    );
    this.dataSource.totalSubject.subscribe({
      next: total => {
        this.total = total;
      },
    });

    this.salesReturnForm
      .get('customer')
      .valueChanges.pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(300),
        switchMap(value => {
          return this.salesService
            .getCustomerList(value)
            .pipe(map(res => res.docs));
        }),
      )
      .subscribe(val => (this.filteredCustomerList = val));
  }
  navigateBack() {
    this.location.back();
  }
  getUpdate(event) {
    this.filters = [];
    this.countFilter = [];
    this.filters.push(['Delivery Note', 'is_return', '=', '1']);
    this.countFilter.push(['Delivery Note', 'is_return', '=', '1']);

    if (this.f.customer.value) {
      this.filters.push(['customer_name', 'like', `${this.f.customer.value}`]);
      this.countFilter.push([
        'Delivery Note',
        'customer_name',
        'like',
        `${this.f.customer.value}`,
      ]);
    }
    if (this.f.name.value) {
      this.filters.push(['name', 'like', `%${this.f.name.value}%`]);
      this.countFilter.push([
        'Delivery Note',
        'name',
        'like',
        `${this.f.name.value}`,
      ]);
    }

    if (this.f.status.value) {
      this.filters.push(['docstatus', '=', this.getStatus()]);
      this.countFilter.push([
        'Delivery Note',
        'docstatus',
        '=',
        this.getStatus(),
      ]);
    }

    if (this.f.start_date.value && this.f.end_date.value) {
      const fromDate = this.getParsedDate(this.f.start_date.value);
      const toDate = this.getParsedDate(this.f.end_date.value);
      this.filters.push(['creation', 'Between', [fromDate, toDate]]);
      this.countFilter.push([
        'Delivery Note',
        'creation',
        'Between',
        [fromDate, toDate],
      ]);
    }

    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      event.pageIndex,
      event.pageSize,
      this.filters,
      this.countFilter,
    );
  }

  setFilter() {
    this.filters = [];
    this.countFilter = [];
    this.filters.push(['Delivery Note', 'is_return', '=', '1']);
    this.countFilter.push(['Delivery Note', 'is_return', '=', '1']);

    if (this.f.customer.value) {
      this.filters.push(['customer_name', 'like', `${this.f.customer.value}`]);
      this.countFilter.push([
        'Delivery Note',
        'customer_name',
        '=',
        `${this.f.customer.value}`,
      ]);
    }
    if (this.f.name.value) {
      this.filters.push(['name', 'like', `%${this.f.name.value}%`]);
      this.countFilter.push([
        'Delivery Note',
        'name',
        'like',
        `${this.f.name.value}`,
      ]);
    }

    if (this.f.status.value) {
      this.filters.push(['docstatus', '=', this.getStatus()]);
      this.countFilter.push([
        'Delivery Note',
        'docstatus',
        '=',
        this.getStatus(),
      ]);
    }

    if (this.f.start_date.value && this.f.end_date.value) {
      const fromDate = this.getParsedDate(this.f.start_date.value);
      const toDate = this.getParsedDate(this.f.end_date.value);
      this.filters.push(['creation', 'Between', [fromDate, toDate]]);
      this.countFilter.push([
        'Delivery Note',
        'creation',
        'Between',
        [fromDate, toDate],
      ]);
    }

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.filters,
      this.countFilter,
    );
  }

  getStatus() {
    return this.f.status.value === 'Canceled' ? 2 : 1;
  }

  getParsedDate(value: string) {
    const date = new Date(value);
    return [
      date.getFullYear(),
      date.getMonth() + 1,
      // +1 as index of months start's from 0
      date.getDate(),
    ].join('-');
  }

  clearFilters() {
    this.salesReturnForm.reset();
    this.setFilter();
  }
  isVisible: boolean = false;

  returnSalesFilter() {
    const SalseInvoiceSidebarID = document.getElementById(
      'return_salse_filter',
    );
    const returnFilter = document.getElementById('returnFilter');
    const returnClose = document.getElementById('returnClose');
    const returnTable = document.getElementById('returnTable');

    if (SalseInvoiceSidebarID.classList.contains('active')) {
      SalseInvoiceSidebarID.classList.remove('active');
      this.isVisible = false;
      returnClose.style.display = 'none';
      returnFilter.style.display = 'block';
      returnTable.style.height = '78vh';
    } else {
      SalseInvoiceSidebarID.classList.add('active');
      this.isVisible = true;
      returnClose.style.display = 'block';
      returnFilter.style.display = 'none';
      returnTable.style.height = '70.5vh';
    }
  }

  timeFunc(timeString: string) {
    const time = timeString.split(':');
    const hh = Number(time[0]) === 0 ? '1' : time[0];
    const mm = time[1];
    const ampm = Number(time[0]) > 12 ? 'PM' : 'AM';
    const fullTime =
      (Number(hh) > 12 ? Number(hh) - 12 : Number(hh)) + ':' + mm + ':' + ampm;

    return fullTime;
  }
  getTime(timeString: string) {
    const time = timeString.split(':');
    const hours = Number(time[0]);
    const minutes = Number(time[1]);
    const seconds = Number(time[2].split('.')[0]); // Get the seconds without milliseconds

    // Determine if it's AM or PM
    const ampm = hours >= 12 ? 'PM' : 'AM';

    // Convert to 12-hour format
    const tm = hours % 12 === 0 ? 12 : hours % 12;

    // Pad single-digit hours, minutes, and seconds with leading zeros
    const formattedTime = `${tm
      .toString()
      .padStart(2, '0')}:${minutes
      .toString()
      .padStart(2, '0')}:${seconds.toString().padStart(2, '0')} ${ampm}`;

    return formattedTime;
  }
}
