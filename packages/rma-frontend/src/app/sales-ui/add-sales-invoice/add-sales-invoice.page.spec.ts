import { Location } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import {
  STORAGE_TOKEN,
  StorageService,
} from '../../api/storage/storage.service';
import { MaterialModule } from '../../material/material.module';
import { ItemPriceService } from '../services/item-price.service';
import { SalesService } from '../services/sales.service';
import { AddSalesInvoicePage } from './add-sales-invoice.page';

@Pipe({ name: 'curFormat' })
class MockPipe implements PipeTransform {
  transform(value: string) {
    return value;
  }
}

describe('AddSalesInvoicePage', () => {
  let component: AddSalesInvoicePage;
  let fixture: ComponentFixture<AddSalesInvoicePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AddSalesInvoicePage, MockPipe],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        imports: [
          RouterTestingModule,
          HttpClientTestingModule,
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
        ],
        providers: [
          {
            provide: Location,
            useValue: {},
          },
          {
            provide: SalesService,
            useValue: {
              createSalesInvoice: (...args) => of({}),
              getSalesInvoice: (...args) => of({}),
              getCustomer: (...args) => of({}),
              getCustomerList: (...args) => of([]),
              getWarehouseList: (...args) => of([]),
              getDefaultTerritory: (...args) => of([]),
              getStore: () => ({
                getItem: (...args) => Promise.resolve('Item'),
                getItems: (...args) => Promise.resolve({}),
                getItemAsync: (...args) => of({}),
              }),
              getApiInfo: () => of({}),
            },
          },
          {
            provide: ItemPriceService,
            useValue: {
              getStockBalance: (...args) => of({}),
              getStore: () => ({
                getItem: (...args) => Promise.resolve('Item'),
                getItems: (...args) => Promise.resolve({}),
              }),
            },
          },
          {
            provide: StorageService,
            useValue: {
              getItem: (...args) => Promise.resolve('ITEM'),
            },
          },
          { provide: STORAGE_TOKEN, useValue: {} },
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSalesInvoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
