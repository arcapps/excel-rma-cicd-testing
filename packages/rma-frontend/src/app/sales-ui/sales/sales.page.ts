import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { of } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { SalesInvoice } from '../../common/interfaces/sales.interface';
import { ValidateInputSelected } from '../../common/pipes/validators';
import {
  INVOICE_DELIVERY_STATUS,
  INVOICE_STATUS,
} from '../../constants/app-string';
import { MY_FORMATS } from '../../constants/date-format';
import { PERMISSION_STATE } from '../../constants/permission-roles';
import { SalesService } from '../services/sales.service';
import { SalesInvoiceDataSource } from './sales-invoice-datasource';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.page.html',
  styleUrls: ['./sales.page.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class SalesPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: SalesInvoiceDataSource;
  displayedColumns = [
    'sr_no',
    'name',
    'status',
    'posting_date',
    'customer_name',
    'total',
    'delivered_percent',
    'territory',
    'remarks',
    'delivered_by',
    'timeStamp',
  ];
  invoiceStatus: string[] = Object.values(INVOICE_STATUS);
  statusColor = {
    Draft: 'blue',
    'To Deliver': '#4d2500',
    Completed: 'green',
    Rejected: 'red',
    Submitted: '#4d2500',
    Canceled: 'red',
  };
  campaignStatus: string[] = ['Yes', 'No', 'All'];
  delivery_statuses: string[] = Object.values(INVOICE_DELIVERY_STATUS);
  status: string = 'All';
  total: number = 0;
  dueTotal: number = 0;
  bufferValue: number = 70;
  disableRefresh: boolean = false;
  campaign: string = 'All';
  sortQuery: any = {};
  salesInvoiceList: Array<SalesInvoice>;

  salesForm: UntypedFormGroup = new UntypedFormGroup({
    customer_name: new UntypedFormControl(),
    start_date: new UntypedFormControl(),
    end_date: new UntypedFormControl(),
    salesPerson: new UntypedFormControl(),
    invoice_number: new UntypedFormControl(),
    branch: new UntypedFormControl(),
    campaign: new UntypedFormControl(),
    status: new UntypedFormControl(),
    delivery_status: new UntypedFormControl(),
  });

  filteredSalesPersonList: any[];
  filteredCustomerList: any[];
  filteredTerritoryList: any[];

  validateInput: any = ValidateInputSelected;
  permissionState = PERMISSION_STATE;

  get f() {
    return this.salesForm.controls;
  }

  constructor(
    private readonly salesService: SalesService,
    private readonly location: Location,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.setAutoComplete();
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.dataSource = new SalesInvoiceDataSource(this.salesService);
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map((event: any) => {
          if (event.url === '/sales')
            this.dataSource.loadItems(undefined, undefined, undefined, {
              status: this.status,
            });
          return event;
        }),
      )
      .subscribe({
        next: () => {
          this.getTotal();
        },
        error: () => {},
      });
    this.dataSource.disableRefresh.subscribe({
      next: res => {
        this.disableRefresh = res;
      },
    });
  }

  setAutoComplete() {
    this.f.salesPerson.valueChanges
      .pipe(
        startWith(''),
        switchMap(value => {
          return this.salesService.getSalesPersonList(value);
        }),
        switchMap((data: any[]) => {
          const salesPersons = [];
          data.forEach(person =>
            person.name !== 'Sales Team'
              ? salesPersons.push(person.name)
              : null,
          );
          return of(salesPersons);
        }),
      )
      .subscribe(res => (this.filteredSalesPersonList = res));

    this.salesForm
      .get('customer_name')
      .valueChanges.pipe(
        startWith(''),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(value => {
          return this.salesService
            .getCustomerList(value)
            .pipe(map(res => res.docs));
        }),
      )
      .subscribe(res => (this.filteredCustomerList = res));

    this.salesForm
      .get('branch')
      .valueChanges.pipe(
        startWith(''),
        switchMap(value => {
          return this.salesService.getStore().getItemAsync('territory', value);
        }),
      )
      .subscribe(res => (this.filteredTerritoryList = res));
  }

  getTotal() {
    this.dataSource.total.subscribe({
      next: total => {
        this.total = total;
      },
    });
    this.dataSource.dueAmountTotal.subscribe({
      next: dueTotal => {
        this.dueTotal = dueTotal;
      },
    });
  }

  syncOutstandingAmount() {
    this.dataSource.syncOutstandingAmount().subscribe({
      next: () => {},
    });
  }

  getUpdate(event: any) {
    const query: any = {};
    if (this.f.customer_name.value)
      query.customer_name = this.f.customer_name.value;
    if (this.f.status.value) query.status = this.f.status.value;
    if (this.f.invoice_number.value) query.name = this.f.invoice_number.value;
    if (this.f.salesPerson.value) query.sales_team = this.f.salesPerson.value;
    if (this.f.branch.value) query.territory = this.f.branch.value;
    if (this.campaign) {
      if (this.campaign === 'Yes') {
        query.isCampaign = true;
      } else if (this.campaign === 'No') {
        query.isCampaign = false;
      }
    }
    if (this.f.start_date.value && this.f.end_date.value) {
      query.fromDate = new Date(this.f.start_date.value).setHours(0, 0, 0, 0);
      query.toDate = new Date(this.f.end_date.value).setHours(23, 59, 59, 59);
    }

    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      this.sortQuery,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
    );
  }

  extractFirst40Letters(word: string): string {
    if (word?.length > 150) {
      return word.substr(0, 150);
    } else {
      return word;
    }
  }

  getStringTime(stringTime: string) {
    const newDate = new Date();
    const [hours, minutes, seconds] = stringTime.split(':');
    newDate.setHours(+hours);
    newDate.setMinutes(Number(minutes));
    newDate.setSeconds(Number(seconds));
    return newDate;
  }

  clearFilters() {
    this.salesForm.reset();
    this.status = 'All';
    this.campaign = 'All';

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;
    this.dataSource.loadItems(
      undefined,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      {
        status: this.status,
      },
    );
  }

  setFilter(event?: any) {
    const query: any = {};
    if (this.f.customer_name.value)
      query.customer_name = this.f.customer_name.value;
    if (this.status) query.status = this.status;
    if (this.f.salesPerson.value) query.sales_team = this.f.salesPerson.value;
    if (this.f.invoice_number.value) query.name = this.f.invoice_number.value;
    if (this.f.branch) query.territory = this.f.branch.value;
    if (this.f.delivery_status.value) {
      query.delivery_status = this.f.delivery_status.value;
    }
    if (this.campaign) {
      if (this.campaign === 'Yes') {
        query.isCampaign = true;
      } else if (this.campaign === 'No') {
        query.isCampaign = false;
      }
    }
    if (this.f.start_date.value && this.f.end_date.value) {
      query.fromDate = new Date(this.f.start_date.value).setHours(0, 0, 0, 0);
      query.toDate = new Date(this.f.end_date.value).setHours(23, 59, 59, 59);
    }

    this.sortQuery = {};
    if (event) {
      for (const key of Object.keys(event)) {
        if (key === 'active' && event.direction !== '') {
          this.sortQuery[event[key]] = event.direction;
        }
      }
    }
    this.sortQuery =
      Object.keys(this.sortQuery).length === 0
        ? { created_on: 'DESC' }
        : this.sortQuery;

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.sortQuery,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
    );
  }

  navigateBack() {
    this.location.back();
  }

  statusChange(status: string) {
    if (status === 'All') {
      this.dataSource.loadItems();
    } else {
      this.status = status;
      this.setFilter();
    }
  }

  getDate(date: string) {
    if (!date) return '';
    return new Date(date);
  }

  getTime(times: string) {
    const time = times.split(':');
    const hours = Number(time[0]);
    const minutes = Number(time[1]);
    const seconds = Number(time[2].split('.')[0]); // Get the seconds without milliseconds

    // Determine if it's AM or PM
    const ampm = hours >= 12 ? 'PM' : 'AM';

    // Convert to 12-hour format
    const tm = hours % 12 === 0 ? 12 : hours % 12;

    // Pad single-digit hours, minutes, and seconds with leading zeros
    const formattedTime = `${tm
      .toString()
      .padStart(2, '0')}:${minutes
      .toString()
      .padStart(2, '0')}:${seconds.toString().padStart(2, '0')} ${ampm}`;
    return formattedTime;
  }

  getPostingTime(date) {
    const originalDate = new Date(date);
    const hours = originalDate.getUTCHours();
    const minutes = originalDate.getUTCMinutes();
    const seconds = originalDate.getUTCSeconds();
    const formattedTime = `${(hours % 12 || 12)
      .toString()
      .padStart(2, '0')}:${minutes
      .toString()
      .padStart(2, '0')}:${seconds.toString().padStart(2, '0')} ${
      hours >= 12 ? 'PM' : 'AM'
    }`;
    return formattedTime;
  }

  statusOfCampaignChange(campaign: string) {
    if (campaign === 'All') {
      this.dataSource.loadItems();
    } else {
      this.campaign = campaign;
      this.setFilter();
    }
  }

  getStatusColor(status: string) {
    return { color: this.statusColor[status] };
  }
  isVisible: boolean = false;

  filterHandler() {
    const SalesInvoiceSidebarID = document.getElementById('sales_filter');
    const close = document.getElementById('close');
    const filter = document.getElementById('filter');
    const salesInvoicePage = document.getElementById('salesInvoicePage');
    const salesInvoiceTable = document.getElementById('salesInvoiceTable');

    if (SalesInvoiceSidebarID.classList.contains('active')) {
      this.isVisible = false;
      SalesInvoiceSidebarID.classList.remove('active');
      close.style.display = 'none';
      filter.style.display = 'block';
      salesInvoicePage.style.height = '84vh';
      salesInvoiceTable.style.height = '78vh';
    } else {
      SalesInvoiceSidebarID.classList.add('active');
      this.isVisible = true;
      filter.style.display = 'none';
      close.style.display = 'block';
      salesInvoicePage.style.height = '64vh';
      salesInvoiceTable.style.height = '60vh';
    }
  }
  timeFunc(timeString: string) {
    const time = timeString.split(':');
    const hh = Number(time[0]) === 0 ? '1' : time[0];
    const mm = time[1];
    const ampm = Number(time[0]) > 12 ? 'PM' : 'AM';
    const fullTime =
      (Number(hh) > 12 ? Number(hh) - 12 : Number(hh)) + ':' + mm + ':' + ampm;

    return fullTime;
  }
}
