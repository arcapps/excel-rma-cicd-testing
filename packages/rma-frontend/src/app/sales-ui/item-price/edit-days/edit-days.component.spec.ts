import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SatPopover } from '@ncstate/sat-popover';
import { MaterialModule } from '../../../material/material.module';
import { EditDaysComponent } from './edit-days.component';

describe('EditDateComponent', () => {
  let component: EditDaysComponent;
  let fixture: ComponentFixture<EditDaysComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [EditDaysComponent],
        // imports: [IonicModule.forRoot()]
        imports: [
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [{ provide: SatPopover, useValue: {} }],
      }).compileComponents();
    }),
  );
  beforeEach(() => {
    fixture = TestBed.createComponent(EditDaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
