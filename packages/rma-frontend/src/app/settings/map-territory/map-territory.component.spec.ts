import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavParams, PopoverController } from '@ionic/angular';
import { EMPTY, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../material/material.module';
import { MapTerritoryComponent } from './map-territory.component';
import { MapTerritoryService } from './map-territory.service';

describe('MapTerritoryComponent', () => {
  let component: MapTerritoryComponent;
  let fixture: ComponentFixture<MapTerritoryComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          FormsModule,
          ReactiveFormsModule,
          MaterialModule,
          HttpClientTestingModule,
          BrowserAnimationsModule,
        ],
        declarations: [MapTerritoryComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: MapTerritoryService,
            useValue: {
              relayTerritories: (...args) => switchMap(res => of(res)),
              relayWarehouses: (...args) => switchMap(res => of(res)),
              create: (...args) => EMPTY,
              update: (...args) => EMPTY,
            },
          },
          { provide: PopoverController, useValue: {} },
          { provide: NavParams, useValue: { data: {} } },
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(MapTerritoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
