import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PopoverController, ToastController } from '@ionic/angular';
import { debounceTime, startWith, switchMap } from 'rxjs/operators';
import { PermissionManager } from '../api/permission/permission.service';
import { ValidateInputSelected } from '../common/pipes/validators';
import {
  CLOSE,
  SHORT_DURATION,
  UPDATE_ERROR,
  UPDATE_SUCCESSFUL,
} from '../constants/app-string';
import { MapTerritoryComponent } from './map-territory/map-territory.component';
import { SettingsService } from './settings.service';
import { TerritoryDataSource } from './territory-datasource';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  hideSASecret: boolean = true;
  hideSAApiSecret: boolean = true;
  group: boolean = false;

  companySettingsForm = new UntypedFormGroup({
    authServerURL: new UntypedFormControl(),
    appURL: new UntypedFormControl(),
    warrantyAppURL: new UntypedFormControl(),
    defaultCompany: new UntypedFormControl(),
    frontendClientId: new UntypedFormControl(),
    backendClientId: new UntypedFormControl(),
    serviceAccountUser: new UntypedFormControl(),
    serviceAccountSecret: new UntypedFormControl(),
    sellingPriceList: new UntypedFormControl(),
    timeZone: new UntypedFormControl(),
    validateStock: new UntypedFormControl(),
    transferWarehouse: new UntypedFormControl(),
    debtorAccount: new UntypedFormControl(),
    serviceAccountApiKey: new UntypedFormControl(),
    serviceAccountApiSecret: new UntypedFormControl(),
    posProfile: new UntypedFormControl(),
    headerImageURL: new UntypedFormControl(),
    headerWidth: new UntypedFormControl(),
    footerImageURL: new UntypedFormControl(),
    footerWidth: new UntypedFormControl(),
    faviconURL: new UntypedFormControl(),
    backdatedInvoices: new UntypedFormControl(),
    backdatedInvoicesForDays: new UntypedFormControl(),
    updateSalesInvoiceStock: new UntypedFormControl(),
    updatePurchaseInvoiceStock: new UntypedFormControl(),
    posAppURL: new UntypedFormControl(),
    brandWiseCreditLimit: new UntypedFormControl(),
  });
  validateInput: any = ValidateInputSelected;

  companies: unknown[] = [];
  sellingPriceLists: unknown[];
  timezones: unknown[] = [];
  accounts: any[] = [];
  warehouses: unknown[] = [];
  posProfiles: unknown[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  territoryDataSource: TerritoryDataSource;

  displayedColumns = ['name', 'warehouse'];
  search: string = '';

  constructor(
    private readonly location: Location,
    private readonly service: SettingsService,
    private readonly toastController: ToastController,
    private readonly popoverController: PopoverController,
    private readonly permissionManager: PermissionManager,
  ) {}

  ngOnInit() {
    this.service.getSettings().subscribe({
      next: res => {
        this.companySettingsForm
          .get('authServerURL')
          .setValue(res.authServerURL);
        this.companySettingsForm.get('appURL').setValue(res.appURL);
        this.companySettingsForm
          .get('defaultCompany')
          .setValue(res.defaultCompany);
        this.companySettingsForm
          .get('frontendClientId')
          .setValue(res.frontendClientId);
        this.companySettingsForm
          .get('backendClientId')
          .setValue(res.backendClientId);
        this.companySettingsForm
          .get('serviceAccountUser')
          .setValue(res.serviceAccountUser);
        this.companySettingsForm
          .get('serviceAccountSecret')
          .setValue(res.serviceAccountSecret);
        this.companySettingsForm
          .get('sellingPriceList')
          .setValue(res.sellingPriceList);
        this.companySettingsForm.get('timeZone').setValue(res.timeZone);
        this.companySettingsForm
          .get('validateStock')
          .setValue(res.validateStock);
        this.companySettingsForm
          .get('debtorAccount')
          .setValue(res.debtorAccount);
        this.companySettingsForm
          .get('transferWarehouse')
          .setValue(res.transferWarehouse);
        this.companySettingsForm
          .get('serviceAccountApiKey')
          .setValue(res.serviceAccountApiKey);
        this.companySettingsForm
          .get('serviceAccountApiSecret')
          .setValue(res.serviceAccountApiSecret);
        this.companySettingsForm
          .get('warrantyAppURL')
          .setValue(res.warrantyAppURL);
        this.companySettingsForm.get('posAppURL').setValue(res.posAppURL);
        this.companySettingsForm.get('posProfile').setValue(res.posProfile);
        this.companySettingsForm
          .get('headerImageURL')
          .setValue(res.headerImageURL);
        this.companySettingsForm.get('headerWidth').setValue(res.headerWidth);
        this.companySettingsForm
          .get('footerImageURL')
          .setValue(res.footerImageURL);
        this.companySettingsForm.get('footerWidth').setValue(res.footerWidth);
        this.companySettingsForm
          .get('backdatedInvoices')
          .setValue(res.backdatedInvoices);
        this.companySettingsForm
          .get('backdatedInvoicesForDays')
          .setValue(res.backdatedInvoicesForDays);
        this.companySettingsForm
          .get('updateSalesInvoiceStock')
          .setValue(res.updateSalesInvoiceStock);
        this.companySettingsForm
          .get('updatePurchaseInvoiceStock')
          .setValue(res.updatePurchaseInvoiceStock);
        this.companySettingsForm
          .get('brandWiseCreditLimit')
          .setValue(res.brandWiseCreditLimit);

        if (res.brand?.faviconURL) {
          this.companySettingsForm
            .get('faviconURL')
            .setValue(res.brand.faviconURL);
          this.service.setFavicon(res.brand.faviconURL);
        }
      },
    });

    this.territoryDataSource = new TerritoryDataSource(this.service);
    this.territoryDataSource.loadItems(
      undefined,
      undefined,
      undefined,
      undefined,
      this.group,
    );

    this.companySettingsForm
      .get('defaultCompany')
      .valueChanges.pipe(
        debounceTime(500),
        startWith(''),
        this.service.relayCompaniesOperation(),
      )
      .subscribe(res => (this.companies = res));

    this.companySettingsForm
      .get('sellingPriceList')
      .valueChanges.pipe(
        debounceTime(500),
        startWith(''),
        this.service.relaySellingPriceListsOperation(),
      )
      .subscribe(res => (this.sellingPriceLists = res));

    this.companySettingsForm
      .get('timeZone')
      .valueChanges.pipe(
        debounceTime(500),
        startWith(''),
        this.service.relayTimeZoneOperation(),
      )
      .subscribe(res => (this.timezones = res));

    this.companySettingsForm
      .get('debtorAccount')
      .valueChanges.pipe(
        startWith(''),
        debounceTime(500),
        switchMap(value => {
          return this.service.relayAccountsOperation(value);
        }),
      )
      .subscribe(res => (this.accounts = res));

    this.companySettingsForm
      .get('transferWarehouse')
      .valueChanges.pipe(
        debounceTime(500),
        startWith(''),
        this.service.relayWarehousesOperation(),
      )
      .subscribe(res => (this.warehouses = res));

    this.companySettingsForm
      .get('posProfile')
      .valueChanges.pipe(
        debounceTime(500),
        startWith(''),
        this.service.relayPosProfiles(),
      )
      .subscribe(res => (this.posProfiles = res));
  }

  navigateBack() {
    this.location.back();
  }

  get f() {
    return this.companySettingsForm.controls;
  }

  updateSettings() {
    this.service
      .updateSettings(
        this.companySettingsForm.get('authServerURL').value,
        this.companySettingsForm.get('appURL').value,
        this.companySettingsForm.get('posAppURL').value,
        this.companySettingsForm.get('defaultCompany').value,
        this.companySettingsForm.get('frontendClientId').value,
        this.companySettingsForm.get('backendClientId').value,
        this.companySettingsForm.get('serviceAccountUser').value,
        this.companySettingsForm.get('serviceAccountSecret').value,
        this.companySettingsForm.get('sellingPriceList').value,
        this.companySettingsForm.get('timeZone').value,
        this.companySettingsForm.get('validateStock').value,
        this.companySettingsForm.get('debtorAccount').value,
        this.companySettingsForm.get('transferWarehouse').value,
        this.companySettingsForm.get('serviceAccountApiKey').value,
        this.companySettingsForm.get('serviceAccountApiSecret').value,
        this.companySettingsForm.get('warrantyAppURL').value,
        this.companySettingsForm.get('posProfile').value,
        this.companySettingsForm.get('headerImageURL').value,
        this.companySettingsForm.get('headerWidth').value,
        this.companySettingsForm.get('footerImageURL').value,
        this.companySettingsForm.get('footerWidth').value,
        this.companySettingsForm.get('backdatedInvoices').value,
        this.companySettingsForm.get('backdatedInvoicesForDays').value,
        this.companySettingsForm.get('updateSalesInvoiceStock').value,
        this.companySettingsForm.get('updatePurchaseInvoiceStock').value,
        this.companySettingsForm.get('brandWiseCreditLimit').value,
        {
          faviconURL: this.companySettingsForm.get('faviconURL').value,
        },
      )
      .subscribe({
        next: () => {
          this.service.setFavicon(this.f.faviconURL.value);
          this.service.setUpdateStock(
            this.f.updateSalesInvoiceStock.value,
            this.f.updatePurchaseInvoiceStock.value,
          );
          this.permissionManager.setGlobalPermissions(
            this.f.backdatedInvoices.value,
            this.f.backdatedInvoicesForDays?.value,
          );
          this.toastController
            .create({
              message: UPDATE_SUCCESSFUL,
              duration: SHORT_DURATION,
              buttons: [{ text: CLOSE }],
            })
            .then(toast => toast.present());
        },
        error: () => {
          this.toastController
            .create({
              message: UPDATE_ERROR,
              duration: SHORT_DURATION,
              buttons: [{ text: CLOSE }],
            })
            .then(toast => toast.present());
        },
      });
  }

  getUpdate(event) {
    this.territoryDataSource.loadItems(
      this.search,
      this.sort.direction,
      event.pageIndex,
      event.pageSize,
      this.group,
    );
  }

  setFilter(value?) {
    this.territoryDataSource.loadItems(
      this.search,
      this.sort.direction,
      0,
      30,
      value !== undefined ? value : this.group,
    );
  }

  getChips(warehouse) {
    if (typeof warehouse === 'string') {
      return [warehouse];
    }
    return warehouse;
  }

  async mapTerritory(uuid?: string, territory?: string, warehouse?: string) {
    if (this.group) {
      this.toastController
        .create({
          message:
            'Territory cannot be edited as group, please deselect group.',
          duration: SHORT_DURATION,
          buttons: [{ text: CLOSE }],
        })
        .then(toast => toast.present());
      return;
    }
    const popover = await this.popoverController.create({
      component: MapTerritoryComponent,
      componentProps: { uuid, territory, warehouse },
    });
    popover.onDidDismiss().then(() => {
      this.territoryDataSource.loadItems(
        undefined,
        undefined,
        undefined,
        undefined,
        this.group,
      );
    });
    return await popover.present();
  }

  rmaServerTab() {
    const rmaServerTab = document.getElementById('rmaServerTab');
    const rmaTabDown = document.getElementById('rmaTabDown');
    const rmaTabUp = document.getElementById('rmaTabUp');

    if (rmaServerTab.classList.contains('active')) {
      rmaServerTab.classList.remove('active');
      rmaServerTab.style.height = '0';
      rmaServerTab.style.padding = '0';
      rmaTabDown.style.display = 'block';
      rmaTabUp.style.display = 'none';
    } else {
      rmaServerTab.classList.add('active');
      rmaServerTab.style.height = 'fit-content';
      rmaServerTab.style.padding = '20px';
      rmaTabDown.style.display = 'none';
      rmaTabUp.style.display = 'block';
    }
  }
  printSettingTab() {
    const printSettingTab = document.getElementById('printSettingTab');
    const printTabDown = document.getElementById('printTabDown');
    const printTabUp = document.getElementById('printTabUp');

    if (printSettingTab.classList.contains('active')) {
      printSettingTab.classList.remove('active');
      printSettingTab.style.height = '0';
      printSettingTab.style.padding = '0';
      printTabDown.style.display = 'block';
      printTabUp.style.display = 'none';
    } else {
      printSettingTab.classList.add('active');
      printSettingTab.style.height = 'fit-content';
      printSettingTab.style.padding = '20px';
      printTabDown.style.display = 'none';
      printTabUp.style.display = 'block';
    }
  }

  territoryTab() {
    const territoryTab = document.getElementById('territoryTab');
    const territoryTabDown = document.getElementById('territoryTabDown');
    const territoryTabUp = document.getElementById('territoryTabUp');

    if (territoryTab.classList.contains('active')) {
      territoryTab.classList.remove('active');
      territoryTab.style.height = '0';
      territoryTab.style.padding = '0';
      territoryTabDown.style.display = 'block';
      territoryTabUp.style.display = 'none';
    } else {
      territoryTab.classList.add('active');
      territoryTab.style.height = 'fit-content';
      territoryTab.style.padding = '20px';
      territoryTabDown.style.display = 'none';
      territoryTabUp.style.display = 'block';
    }
  }
}
