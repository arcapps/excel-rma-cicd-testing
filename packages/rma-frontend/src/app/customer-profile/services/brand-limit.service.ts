import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, from, map, switchMap } from 'rxjs';
import { StorageService } from '../../../app/api/storage/storage.service';
import {
  ACCESS_TOKEN,
  AUTHORIZATION,
  BEARER_TOKEN_PREFIX,
} from '../../../app/constants/storage';
import { RELAY_CUSTOMER_ENDPOINT } from '../../../app/constants/url-strings';
import { BrandLimit } from '../../common/interfaces/brand-limit.interface';

@Injectable({
  providedIn: 'root',
})
export class BrandLimitService {
  private brand_limits: BrandLimit[] = [];
  private brand_subject = new BehaviorSubject<BrandLimit[]>([]);
  private isEdit = new BehaviorSubject<boolean>(false);

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
  ) {}

  createBrandLimit(brand_limit: BrandLimit): void {
    brand_limit.uuid = new Date().getTime();
    this.brand_limits.push(brand_limit);
    this.brand_subject.next(this.brand_limits);
  }

  getBrandLimit(customer_name: string) {
    const url = `${RELAY_CUSTOMER_ENDPOINT}/${customer_name}`;
    return this.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<any[]>(url, {
          headers,
        });
      }),
    );
  }

  deleteBrandLimit(id: string | number) {
    this.brand_limits = this.brand_limits.filter(
      brand_limit => brand_limit.uuid !== id,
    );
    this.brand_subject.next(this.brand_limits);
  }

  editBrandLimit(data: BrandLimit) {
    this.isEdit.next(false);
  }

  getHeaders() {
    return from(this.storage.getItem(ACCESS_TOKEN)).pipe(
      map(token => {
        return {
          [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
        };
      }),
    );
  }
}
