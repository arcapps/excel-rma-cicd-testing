import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AppCommonModule } from '../../../app/common/app-common.module';
import { MaterialModule } from '../../../app/material/material.module';
import { BrandWiseCreditLimitRoutingModule } from './brand-wise-credit-limit-routing.module';
import { BrandWiseCreditLimitComponent } from './brand-wise-credit-limit.component';
import { CommonComponentModule } from 'src/app/common/components/common-component.module';

@NgModule({
  declarations: [BrandWiseCreditLimitComponent],
  imports: [
    CommonComponentModule,
    AppCommonModule,
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    BrandWiseCreditLimitRoutingModule,
    MaterialModule,
  ],
})
export class BrandWiseCreditLimitModule {}
