import { Location } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { of } from 'rxjs';

import { PurchaseService } from '../services/purchase.service';
import { ViewPurchaseInvoicePage } from './view-purchase-invoice.page';

describe('ViewPurchaseInvoicePage', () => {
  let component: ViewPurchaseInvoicePage;
  let fixture: ComponentFixture<ViewPurchaseInvoicePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ViewPurchaseInvoicePage],
        imports: [IonicModule.forRoot(), RouterTestingModule],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: Location,
            useValue: {},
          },
          {
            provide: PurchaseService,
            useValue: {
              getPurchaseInvoice: (...args) => of({}),
            },
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(ViewPurchaseInvoicePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
