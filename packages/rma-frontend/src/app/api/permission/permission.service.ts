import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { from, lastValueFrom, of, throwError } from 'rxjs';
import { switchMap, catchError, map } from 'rxjs/operators';
import { SYSTEM_MANAGER, USER_ROLE } from '../../constants/app-string';
import {
  PERMISSION_STATE,
  PermissionRoles,
  settingPermissions,
} from '../../constants/permission-roles';
import {
  BACKDATE_PERMISSION,
  BACKDATE_PERMISSION_FOR_DAYS,
} from '../../constants/storage';
import { StorageService } from '../storage/storage.service';

export const PermissionState = {
  create: 'create',
  read: 'read',
  update: 'update',
  delete: 'delete',
};

@Injectable({
  providedIn: 'root',
})
export class PermissionManager {
  constructor(private readonly storageService: StorageService) {}

  // module = something like "sales_invoice" , state = something like "create"

  getPermission(module: string, state: string) {
    return of({}).pipe(
      switchMap(() => {
        return from(this.storageService.getItem(USER_ROLE));
      }),
      map(roles => roles.split(',')),
      switchMap((roles: string[]) => {
        if (roles && roles.length) {
          return this.validateRoles(roles, module, state);
        }
        return throwError(() => 'Retry');
      }),
      switchMap((existing_roles: string[]) => {
        if (existing_roles && existing_roles.length) {
          return of(true);
        }
        return of(false);
      }),
      // repeat({ delay: 500, count: 10 }),
      catchError(() => {
        return of(false);
      }),
    );
  }

  validateRoles(user_roles: string[], module: string, state: string) {
    const roles = [];
    if (state === 'active') {
      roles.push(...this.getActiveRoles(module));
    } else {
      try {
        roles.push(SYSTEM_MANAGER);
        roles.push(...PermissionRoles[module][state]);
      } catch {
        return throwError(() => 'Module and state dose not exist.');
      }
    }
    return of(_.intersection(user_roles, roles));
  }

  getActiveRoles(module): any[] {
    const roles = new Set();
    roles.add(SYSTEM_MANAGER);
    Object.keys(PermissionState).forEach(key => {
      if (PermissionRoles[module] && PermissionRoles[module][key]) {
        PermissionRoles[module][key].forEach((role: string) => {
          roles.add(role);
        });
      }
    });
    return Array.from(roles);
  }

  setupPermissions() {
    Object.keys(PERMISSION_STATE).forEach(module => {
      Object.keys(PERMISSION_STATE[module]).forEach(async context => {
        PERMISSION_STATE[module][context] = await lastValueFrom(
          this.getPermission(module, context),
        );
      });
    });
  }

  setGlobalPermissions(
    backdated_permissions: boolean = false,
    backdated_permissions_for_days: number = 0,
  ) {
    this.storageService.setItem(
      BACKDATE_PERMISSION,
      backdated_permissions.toString(),
    ),
      (settingPermissions.backdated_permissions = backdated_permissions);
    this.storageService.setItem(
      BACKDATE_PERMISSION_FOR_DAYS,
      backdated_permissions_for_days.toString(),
    ),
      (settingPermissions.backdated_permissions_for_days = backdated_permissions_for_days);
  }
}

export class PermissionStateInterface {
  [key: string]: {
    create?: boolean;
    read?: boolean;
    update?: boolean;
    active?: boolean;
    accept?: boolean;
    delete?: boolean;
    submit?: boolean;
  };
}
