import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { IonicModule } from '@ionic/angular';
import { CsvJsonService } from '../../../api/csv-json/csv-json.service';
import { MaterialModule } from '../../../material/material.module';
import { SalesService } from '../../../sales-ui/services/sales.service';
import { PurchaseService } from '../../services/purchase/purchase.service';
import { StockEntryService } from '../../services/stock-entry/stock-entry.service';
import { DeliveredSerialsComponent } from './delivered-serials.component';

describe('DeliveredSerialsComponent', () => {
  let component: DeliveredSerialsComponent;
  let fixture: ComponentFixture<DeliveredSerialsComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [DeliveredSerialsComponent],
        imports: [
          IonicModule.forRoot(),
          MaterialModule,
          FormsModule,
          BrowserAnimationsModule,
          NoopAnimationsModule,
          ReactiveFormsModule,
        ],
        providers: [
          {
            provide: SalesService,
            useValue: {},
          },
          {
            provide: PurchaseService,
            useValue: {},
          },
          {
            provide: CsvJsonService,
            useValue: {},
          },
          {
            provide: StockEntryService,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(DeliveredSerialsComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
