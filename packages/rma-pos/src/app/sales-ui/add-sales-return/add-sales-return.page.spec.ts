import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Location } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  Pipe,
  PipeTransform,
  Renderer2,
} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { CsvJsonService } from '../../api/csv-json/csv-json.service';
import { MaterialModule } from '../../material/material.module';
import { SalesService } from '../services/sales.service';
import { AddSalesReturnPage } from './add-sales-return.page';

@Pipe({ name: 'curFormat' })
class MockPipe implements PipeTransform {
  transform(value: string) {
    return value;
  }
}

describe('AddSalesReturnPage', () => {
  let component: AddSalesReturnPage;
  let fixture: ComponentFixture<AddSalesReturnPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AddSalesReturnPage, MockPipe],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        imports: [
          IonicModule.forRoot(),
          RouterTestingModule,
          HttpClientTestingModule,
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
        ],
        providers: [
          {
            provide: Location,
            useValue: {},
          },
          {
            provide: MatSnackBar,
            useValue: {},
          },
          {
            provide: CsvJsonService,
            useValue: {},
          },
          {
            provide: Renderer2,
            useValue: {},
          },
          {
            provide: SalesService,
            useValue: {
              relaySalesInvoice: (...args) => of({}),
              getSalesInvoice: (...args) =>
                of({
                  items: [],
                  delivered_items_map: {},
                  returned_items_map: {},
                }),
              getWarehouseList: (...args) => of([{}]),
              getDeliveryNoteNames: (...args) => of([{ name: '' }]),
              getStore: () => ({
                getItem: (...args) => Promise.resolve('ITEM'),
                getItemAsync: (...args) => of([]),
              }),
            },
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(AddSalesReturnPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
