import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MaterialModule } from '../../../material/material.module';
import { InvoiceWarrantyComponent } from './invoice-warranty.component';

describe('InvoiceWarrantyComponent', () => {
  let component: InvoiceWarrantyComponent;
  let fixture: ComponentFixture<InvoiceWarrantyComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [InvoiceWarrantyComponent],
        imports: [MaterialModule],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceWarrantyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
