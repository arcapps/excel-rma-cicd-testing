import {
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  Renderer2,
} from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { CsvJsonService } from '../../../api/csv-json/csv-json.service';
import { TimeService } from '../../../api/time/time.service';
import { MaterialModule } from '../../../material/material.module';
import { SalesService } from '../../services/sales.service';
import { ViewSalesInvoicePage } from '../view-sales-invoice.page';
import { SerialsComponent } from './serials.component';

describe('SerialsComponent', () => {
  let component: SerialsComponent;
  let fixture: ComponentFixture<SerialsComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SerialsComponent],
        imports: [
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          RouterTestingModule.withRoutes([]),
        ],
        providers: [
          {
            provide: MatSnackBar,
            useValue: {},
          },
          {
            provide: SalesService,
            useValue: {
              getSalesInvoice: (...args) =>
                of({ items: [], delivered_items_map: {} }),
              getWarehouseList: (...args) => of([{}]),
              relaySalesInvoice: (...args) => of({}),
              getStore: () => ({
                getItem: (...args) => Promise.resolve('ITEM'),
              }),
            },
          },
          {
            provide: ViewSalesInvoicePage,
            useValue: {},
          },
          {
            provide: Renderer2,
            useValue: {},
          },
          {
            provide: CsvJsonService,
            useValue: {},
          },
          {
            provide: TimeService,
            useValue: {},
          },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SerialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
