import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule } from '@ionic/angular';
import { MaterialModule } from '../../material/material.module';
import { JobsService } from '../jobs-service/jobs.service';
import { ViewSingleJobPage } from './view-single-job.page';

describe('ViewSingleJobPage', () => {
  let component: ViewSingleJobPage;
  let fixture: ComponentFixture<ViewSingleJobPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ViewSingleJobPage],
        imports: [
          IonicModule.forRoot(),
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
        ],
        providers: [
          {
            provide: MatDialogRef,
            useValue: {},
          },
          {
            provide: JobsService,
            useValue: {},
          },
          { provide: MAT_DIALOG_DATA, useValue: {} },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();

      fixture = TestBed.createComponent(ViewSingleJobPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
