import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type TermsAndConditionsDocument = HydratedDocument<TermsAndConditions>;

@Schema({ collection: 'terms_and_conditions' })
export class TermsAndConditions {
  @Prop()
  uuid: string;

  @Prop()
  terms_and_conditions: string;
}

export const TermsAndConditionsSchema =
  SchemaFactory.createForClass(TermsAndConditions);
