import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
// import { TermsAndConditions } from './terms-and-conditions/terms-and-conditions.entity';
import { TermsAndConditionsService } from './terms-and-conditions/terms-and-conditions.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TermsAndConditionsSchema } from '../schema/terms-and-conditions.schema';

@Module({
  // imports: [TypeOrmModule.forFeature([TermsAndConditions])],
  imports: [
    MongooseModule.forFeature([
      { name: 'TermsAndConditions', schema: TermsAndConditionsSchema },
    ]),
  ],
  providers: [TermsAndConditionsService],
  exports: [TermsAndConditionsService],
})
export class TermsAndConditionsEntitiesModule {}
