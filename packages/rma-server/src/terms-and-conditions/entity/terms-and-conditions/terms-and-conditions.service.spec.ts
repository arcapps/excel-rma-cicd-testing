import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { TermsAndConditionsService } from './terms-and-conditions.service';

describe('RequestStateService', () => {
  let service: TermsAndConditionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TermsAndConditionsService,
        {
          provide: getModelToken('TermsAndConditions'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TermsAndConditionsService>(TermsAndConditionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
