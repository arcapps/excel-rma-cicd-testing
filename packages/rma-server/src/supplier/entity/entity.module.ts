import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
// import { Supplier } from './supplier/supplier.entity';
import { SupplierService } from './supplier/supplier.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SupplierSchema } from '../schema/supplier.schema';

@Module({
  imports: [
    // TypeOrmModule.forFeature([Supplier]),
    MongooseModule.forFeature([{ name: 'Supplier', schema: SupplierSchema }]),
  ],
  providers: [SupplierService],
  exports: [SupplierService],
})
export class SupplierEntitiesModule {}
