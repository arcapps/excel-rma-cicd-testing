import { Supplier } from '../../schema/supplier.schema';
import { IEvent } from '@nestjs/cqrs';

export class SupplierRemovedEvent implements IEvent {
  constructor(public supplier: Supplier) {}
}
