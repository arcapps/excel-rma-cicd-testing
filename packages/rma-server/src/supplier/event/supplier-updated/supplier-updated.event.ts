import { Supplier } from '../../schema/supplier.schema';
import { IEvent } from '@nestjs/cqrs';

export class SupplierUpdatedEvent implements IEvent {
  constructor(public updatePayload: Supplier) {}
}
