import { IEvent } from '@nestjs/cqrs';
import { Supplier } from '../../schema/supplier.schema';

export class SupplierAddedEvent implements IEvent {
  constructor(public supplier: Supplier, public clientHttpRequest: any) {}
}
