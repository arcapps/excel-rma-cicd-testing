import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export type SupplierDocument = HydratedDocument<Supplier>;

@Schema({ collection: 'supplier' })
export class Supplier {
  @Prop()
  uuid: string;

  @Prop({ unique: true })
  name: string;

  @Prop()
  owner: string;

  @Prop()
  supplier_name: string;

  @Prop()
  country: string;

  @Prop()
  default_bank_account: string;

  @Prop()
  tax_id: string;

  @Prop()
  tax_category: string;

  @Prop()
  supplier_type: string;

  @Prop()
  is_internal_supplier: string;

  @Prop()
  represents_company: string;

  @Prop()
  pan: string;

  @Prop()
  disabled: string;

  @Prop()
  docstatus: string;

  @Prop()
  gst_category: string;

  @Prop()
  export_type: string;

  @Prop()
  isSynced: boolean;

  constructor() {
    if (!this.uuid) this.uuid = uuidv4();
  }
}

export const SupplierSchema = SchemaFactory.createForClass(Supplier);
