// import { InjectRepository } from '@nestjs/typeorm';
// import { StockEntry } from './stock-entry.entity';
import { Injectable } from '@nestjs/common';
// import { MongoRepository } from 'typeorm';
import { STOCK_ENTRY_LIST_ITEM_SELECT_KEYS } from '../../constants/app-strings';
import { PARSE_REGEX } from '../../constants/app-strings';
// import { PermissionStateInterface } from '../../constants/agenda-job';
import { Observable } from 'rxjs';
// import { switchMap } from 'rxjs/operators';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StockEntry, StockEntryDocument } from '../schema/stock-entry.schema';
@Injectable()
export class StockEntryService {
  constructor(
    // @InjectRepository(StockEntry)
    // private readonly stockEntryRepository: MongoRepository<StockEntry>,
    @InjectModel('StockEntry')
    private stockEntryModel: Model<StockEntryDocument>,
  ) {}

  async find(query?) {
    return await this.stockEntryModel.find(query);
  }

  async create(stockEntry: StockEntry) {
    return await new this.stockEntryModel(stockEntry).save();
  }

  async findOne(options): Promise<StockEntry> {
    return await this.stockEntryModel.findOne(options.where);
  }

  async list(skip, take, sort, permissionState: any, filter_query?) {
    permissionState = permissionState._doc || permissionState;
    let sortQuery;
    const query: any = {};
    let permissionQuery: any = {
      items: {
        $elemMatch: {},
      },
    };

    try {
      sortQuery = JSON.parse(sort);
    } catch (error) {
      sortQuery = {
        _id: 'DESC',
      };
    }

    for (const key of Object.keys(sortQuery)) {
      if (!sortQuery[key]) {
        delete sortQuery[key];
      } else {
        sortQuery[key] = sortQuery[key].toUpperCase();
      }
    }

    if (filter_query.names) {
      query.names = { $regex: PARSE_REGEX(filter_query.names), $options: 'i' };
      delete filter_query.names;
    }

    if (filter_query.s_warehouse) {
      permissionQuery.items.$elemMatch.s_warehouse = {
        $in: [filter_query.s_warehouse],
      };
      delete filter_query.s_warehouse;
    }

    if (filter_query.t_warehouse) {
      permissionQuery.items.$elemMatch.t_warehouse = {
        $in: [filter_query.t_warehouse],
      };
      delete filter_query.t_warehouse;
    }

    if (permissionState.warehouses) {
      permissionQuery.items.$elemMatch.$or = [
        { t_warehouse: { $in: permissionState.warehouses } },
        { s_warehouse: { $in: permissionState.warehouses } },
      ];
    }
    if (permissionState.territories) {
      permissionQuery.territory = { $in: permissionState.territories };
    }

    if (filter_query?.fromDate && filter_query?.toDate) {
      query.createdAt = {
        $gte: new Date(filter_query.fromDate),
        $lte: new Date(filter_query.toDate),
      };
      delete filter_query.fromDate;
      delete filter_query.toDate;
    }

    if (filter_query?.warrantyClaimUuid) {
      query.warrantyClaimUuid = filter_query.warrantyClaimUuid;
      delete filter_query.warrantyClaimUuid;
    }

    if (permissionQuery?.items) {
      permissionQuery = this.getElementMatchCondition(permissionQuery);
    }

    const $and: any[] = [
      filter_query ? this.getFilterQuery(filter_query) : {},
      query,
      permissionQuery,
    ];

    const where: { $and: any } = { $and };

    const select: string[] = this.getSelectKeys();

    // this is to override the type or typeorm select, it dose not support child objects in query builder.
    const db: any = this.stockEntryModel;

    // const results = await db
    //   .find()
    //   .where(where)
    //   .skip(skip)
    //   .limit(take)
    //   .sort(sortQuery)
    //   .select(select);

    const countPromise = db.count(where);
    const resultsPromise = db
      .find()
      .where(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery)
      .select(select);

    const [countResult, results] = await Promise.all([
      countPromise,
      resultsPromise,
    ]);

    return {
      docs: results || [],
      length: countResult,
      offset: skip,
    };
  }

  getSelectKeys() {
    const select = STOCK_ENTRY_LIST_ITEM_SELECT_KEYS.map(key => `items.${key}`);

    select.push(...Object.keys(this.stockEntryModel.schema.paths));
    select.splice(select.indexOf('items'), 1);
    return select;
  }

  getElementMatchCondition(query: string) {
    return {
      $or: [query, { items: { $exists: true, $eq: [] } }],
    };
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (query[key]) {
        if (typeof query[key] === 'string') {
          query[key] = { $regex: PARSE_REGEX(query[key]), $options: 'i' };
        } else {
          delete query[key];
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.stockEntryModel.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.stockEntryModel.updateOne(query, options);
  }

  async insertMany(query, options?) {
    return await this.stockEntryModel.insertMany(query, options);
  }

  asyncAggregate(query): Observable<any> {
    const promise = this.stockEntryModel.aggregate(query);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }
}
