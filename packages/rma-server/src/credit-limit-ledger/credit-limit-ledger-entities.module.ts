import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CreditLimitLedgerService } from './credit-limit-ledger-service/credit-limit-ledger.service';
import { CreditLimitLedgerSchema } from './schema/credit-limit-ledger.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'CreditLimitLedger', schema: CreditLimitLedgerSchema },
    ]),
  ],
  providers: [CreditLimitLedgerService],
  exports: [CreditLimitLedgerService],
})
export class CreditLimitLedgerEntitiesModule {}
