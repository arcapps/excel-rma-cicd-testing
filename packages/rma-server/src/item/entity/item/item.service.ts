// import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
// import { MongoRepository } from 'typeorm';
import { PARSE_REGEX } from '../../../constants/app-strings';
import { Observable } from 'rxjs';

// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Item, ItemDocument } from '../../schema/item.schema';

@Injectable()
export class ItemService {
  constructor(
    // @InjectRepository(Item)
    // private readonly itemRepository: MongoRepository<Item>,
    @InjectModel('Item') private itemModel: Model<ItemDocument>,
  ) {}

  async find(query?) {
    return await this.itemModel.find(query);
  }

  async create(customerPayload: Item) {
    // const customer = new Item();
    // Object.assign(customer, customerPayload);
    return await new this.itemModel(customerPayload).save();
  }

  async findOne(options) {
    return await this.itemModel.findOne(options.where);
  }

  async list(skip, take, sort, filterQuery?) {
    let sortQuery;
    try {
      sortQuery = JSON.parse(sort);
    } catch {
      sortQuery = { name: 'asc' };
    }

    for (const key of Object.keys(sortQuery)) {
      sortQuery[key] = sortQuery[key].toUpperCase();
      if (!sortQuery[key]) {
        delete sortQuery[key];
      }
    }

    try {
      filterQuery = JSON.parse(filterQuery);
      delete filterQuery.bundle_items;
    } catch {
      filterQuery = {};
    }

    const $and: any[] = [filterQuery ? this.getFilterQuery(filterQuery) : {}];

    const where: { $and: any } = { $and };

    const results = await this.itemModel
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);
    return {
      docs: results || [],
      length: await this.itemModel.count(where),
      offset: skip,
    };
  }

  asyncAggregate(query) {
    const promise = this.itemModel.aggregate(query);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  // asyncAggregate(query) {
  //   return of(this.itemModel.aggregate(query)).pipe(
  //     switchMap((aggregateData: any) => {
  //       return aggregateData.toArray();
  //     }),
  //   );
  // }

  listBrands(limit: number, search: string) {
    return this.asyncAggregate([
      { $match: { brand: { $regex: search, $options: 'i' } } },
      {
        $group: { _id: '$brand' },
      },
      {
        $limit: limit,
      },
      {
        $group: {
          _id: 1,
          brands: { $addToSet: '$_id' },
        },
      },
    ]);
  }

  listItemGroup() {
    return this.asyncAggregate([
      {
        $group: { _id: '$item_group' },
      },
    ]);
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.itemModel.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.itemModel.updateOne(query, options);
  }

  async count(query) {
    return await this.itemModel.count(query);
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (query[key]) {
        if (['bundle_items', 'is_stock_item'].includes(key)) {
          return;
        }
        if (key === 'status' && query[key] === 'All') {
          delete query[key];
        } else {
          if (typeof query[key] === 'string') {
            query[key] = { $regex: PARSE_REGEX(query[key]), $options: 'i' };
          } else {
            delete query[key];
          }
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }
}
