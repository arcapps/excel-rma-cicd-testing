import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { ItemBundleItemWebhookInterface } from '../entity/item/item-webhook-interface';

export type ItemDocument = HydratedDocument<Item>;

@Schema({ collection: 'item' })
export class Item {
  @Prop()
  uuid: string;

  @Prop()
  creation: string;

  @Prop()
  modified: string;

  @Prop()
  name: string;

  @Prop()
  owner: string;

  @Prop()
  modified_by: string;

  @Prop()
  docstatus: number;

  @Prop({ unique: true })
  item_code: string;

  @Prop()
  item_name: string;

  @Prop()
  item_group: string;

  @Prop()
  stock_uom: string;

  @Prop()
  disabled: number;

  @Prop()
  description: string;

  @Prop()
  shelf_life_in_days: number;

  @Prop()
  end_of_life: string;

  @Prop()
  default_material_request_type: string;

  @Prop()
  has_serial_no: number;

  @Prop()
  has_variants: number;

  @Prop()
  is_purchase_item: number;

  @Prop()
  min_order_qty: number;

  @Prop()
  safety_stock: number;

  @Prop()
  last_purchase_rate: number;

  @Prop()
  country_of_origin: string;

  @Prop()
  is_sales_item: number;

  @Prop()
  taxes: any[];

  @Prop()
  attributes: any[];

  @Prop()
  uoms: Uom[];

  @Prop()
  item_defaults: ItemDefaults[];

  @Prop()
  isSynced: boolean;

  @Prop()
  minimumPrice: number;

  @Prop()
  purchaseWarrantyMonths: number;

  @Prop()
  salesWarrantyMonths: number;

  @Prop()
  brand: string;

  @Prop()
  is_stock_item: number;

  @Prop()
  mrp: number;

  @Prop()
  bundle_items: ItemBundleItemWebhookInterface[];

  @Prop()
  excel_serials: string;

  @Prop()
  valuation_rate: number;
}

export class Barcodes {
  name: string;
  idx: number;
  docstatus: number;
  barcode: string;
  barcode_type: string;
}

export class Uom {
  name: string;
  idx: number;
  docstatus: number;
  conversion_factor: number;
  uom: string;
  doctype: string;
}
export class ItemDefaults {
  company: string;
  default_warehouse: string;
  doctype: string;
}

export const ItemSchema = SchemaFactory.createForClass(Item);
