import { IEvent } from '@nestjs/cqrs';
import { Item } from '../../schema/item.schema';

export class MinimumItemPriceSetEvent implements IEvent {
  constructor(public readonly item: Item) {}
}
