import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ItemAggregateService } from '../../aggregates/item-aggregate/item-aggregate.service';
import { RetrieveItemByItemGroupQuery } from './retrieve-item-by-item-group-.query';

@QueryHandler(RetrieveItemByItemGroupQuery)
export class RetrieveItemByItemGroupHandler
  implements IQueryHandler<RetrieveItemByItemGroupQuery>
{
  constructor(private readonly manager: ItemAggregateService) {}

  async execute(query: RetrieveItemByItemGroupQuery) {
    const { req, item_group } = query;
    return await this.manager.retrieveItemByItemGroup(item_group, req);
  }
}
