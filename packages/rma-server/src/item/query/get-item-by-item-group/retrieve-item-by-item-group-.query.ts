import { IQuery } from '@nestjs/cqrs';

export class RetrieveItemByItemGroupQuery implements IQuery {
  constructor(public readonly item_group: string, public readonly req: any) {}
}
