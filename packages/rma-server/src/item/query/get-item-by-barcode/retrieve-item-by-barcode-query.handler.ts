import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ItemAggregateService } from '../../aggregates/item-aggregate/item-aggregate.service';
import { RetrieveItemByItemBarcodeQuery } from './retrieve-item-by-barcode-.query';

@QueryHandler(RetrieveItemByItemBarcodeQuery)
export class RetrieveItemByBarcodeHandler
  implements IQueryHandler<RetrieveItemByItemBarcodeQuery>
{
  constructor(private readonly manager: ItemAggregateService) {}

  async execute(query: RetrieveItemByItemBarcodeQuery) {
    const { req, barcode } = query;
    return await this.manager.retrieveItemByBarCode(barcode, req);
  }
}
