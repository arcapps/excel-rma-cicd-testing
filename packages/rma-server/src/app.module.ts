import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { CommandModule } from './command/command.module';
import { CommonDepModule } from './common-dep/common-dep.module';
import { ConfigModule } from './config/config.module';
import {
  getCacheDBConnectionString,
  getDefaultDbConnectionString,
} from './constants/mongo.connection';
import { CreditLimitLedgerModule } from './credit-limit-ledger/credit-limit-ledger.module';
import { CreditNoteModule } from './credit-note/credit-note-invoice.module';
import { CustomerModule } from './customer/customer.module';
import { DeliveryNoteModule } from './delivery-note/delivery-note.module';
import { DeliveryNoteEntitiesModule } from './delivery-note/entity/delivery-note-entity.module';
import { DirectModule } from './direct/direct.module';
import { ErrorLogModule } from './error-log/error-logs-invoice.module';
import { ItemModule } from './item/item.module';
import { PrintModule } from './print/print-module';
import { ProblemModule } from './problem/problem.module';
import { PurchaseInvoiceModule } from './purchase-invoice/purchase-invoice.module';
import { PurchaseOrderModule } from './purchase-order/purchase-order.module';
import { PurchaseReceiptModule } from './purchase-receipt/purchase-receipt.module';
import { ReportModule } from './report/report.module';
import { ReturnVoucherModule } from './return-voucher/return-voucher-invoice.module';
import { SalesInvoiceModule } from './sales-invoice/sales-invoice.module';
import { SerialNoModule } from './serial-no/serial-no.module';
import { ServiceInvoiceModule } from './service-invoice/service-invoice.module';
import { StockEntryModule } from './stock-entry/stock-entry.module';
import { StockLedgerModule } from './stock-ledger/stock-ledger.module';
import { SupplierModule } from './supplier/supplier.module';
import { SyncEntitiesModule } from './sync/entities/sync-entity.module';
import { SyncModule } from './sync/sync.module';
import { SystemSettingsModule } from './system-settings/system-settings.module';
import { TermsAndConditionsModule } from './terms-and-conditions/terms-and-conditions.module';
import { WarrantyClaimModule } from './warranty-claim/warranty-claim.module';

@Module({
  imports: [
    CommonDepModule,
    MongooseModule.forRoot(getDefaultDbConnectionString()),
    MongooseModule.forRoot(getCacheDBConnectionString()),

    AuthModule,
    CommandModule,
    ConfigModule,
    CreditLimitLedgerModule,
    CreditNoteModule,
    CustomerModule,
    DeliveryNoteEntitiesModule,
    DeliveryNoteModule,
    DirectModule,
    ErrorLogModule,
    ItemModule,
    PrintModule,
    ProblemModule,
    PurchaseInvoiceModule,
    PurchaseOrderModule,
    PurchaseReceiptModule,
    ReportModule,
    ReturnVoucherModule,
    SalesInvoiceModule,
    SerialNoModule,
    ServiceInvoiceModule,
    StockEntryModule,
    StockLedgerModule,
    SupplierModule,
    SyncEntitiesModule,
    SyncModule,
    SystemSettingsModule,
    TermsAndConditionsModule,
    WarrantyClaimModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
