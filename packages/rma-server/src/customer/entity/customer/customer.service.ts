import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, of, switchMap } from 'rxjs';
import {
  ACCEPT,
  APPLICATION_JSON_CONTENT_TYPE,
  // APP_JSON,
  AUTHORIZATION,
  // BEARER_HEADER_VALUE_PREFIX,
  CONTENT_TYPE,
  PARSE_REGEX,
  TOKEN_HEADER_VALUE_PREFIX,
} from '../../../constants/app-strings';
import { FRAPPE_API_GET_CUSTOMER_ENDPOINT } from '../../../constants/routes';
import { CreditLimitLedgerService } from '../../../credit-limit-ledger/credit-limit-ledger-service/credit-limit-ledger.service';
import { CreditLimitLedger } from '../../../credit-limit-ledger/schema/credit-limit-ledger.schema';
import { Customer, CustomerDocument } from '../../schemas/customer.schema';
import {
  BrandWiseCreditLimitDto,
  CustomerWebhookDto,
} from './customer-webhook-interface';

@Injectable()
export class CustomerService {
  constructor(
    // @InjectRepository(Customer)
    // private readonly customerRepository: MongoRepository<Customer>,
    @InjectModel('Customer') private customerModel: Model<CustomerDocument>,
    private readonly creditLimitLedgerSvc: CreditLimitLedgerService,
    private readonly http: HttpService,
  ) {}

  async find(query?) {
    return await this.customerModel.find(query);
  }

  async create(customerPayload: Customer) {
    const customer = new Customer();
    Object.assign(customer, customerPayload);

    return await new this.customerModel(customer).save();
  }

  async findOne(options) {
    return await this.customerModel.findOne(options.where);
  }
  // async list(skip, take, search, sort, territories: string[]) {
  //   const sortQuery = { name: sort };
  //   const nameExp = { $regex: PARSE_REGEX(search), $options: 'i' };
  //   const columns = this.customerRepository.manager.connection
  //     .getMetadata(Customer)
  //     .ownColumns.map(column => column.propertyName);

  //   const $or = columns.map(field => {
  //     const filter = {};
  //     filter[field] = nameExp;
  //     return filter;
  //   });
  //   const customerQuery =
  //     territories && territories.length !== 0
  //       ? { territory: { $in: territories } }
  //       : {};

  //   const $and: any[] = [{ $or }, customerQuery];

  //   const where: { $and: any } = { $and };

  //   const results = await this.customerRepository.find({
  //     skip,
  //     take,
  //     where,
  //     order: sortQuery,
  //   });

  //   return {
  //     docs: results || [],
  //     length: await this.customerRepository.count(where),
  //     offset: skip,
  //   };
  // }

  async getBrandLimits(customer_name) {
    const res = await this.customerModel
      .findOne({ name: customer_name })
      .select({ brand_limits: 1, others_credit_limits: 1 });
    if (!res) return [];
    return res;
  }

  getServiceAccountApiHeaders(settings) {
    const headers: any = {};
    headers[AUTHORIZATION] = TOKEN_HEADER_VALUE_PREFIX;
    headers[AUTHORIZATION] += settings.serviceAccountApiKey + ':';
    headers[AUTHORIZATION] += settings.serviceAccountApiSecret;
    headers[CONTENT_TYPE] = APPLICATION_JSON_CONTENT_TYPE;
    headers[ACCEPT] = APPLICATION_JSON_CONTENT_TYPE;
    return headers;
  }

  updateCustomerLimit(
    request: any,
    settings: any,
    creditLimits: BrandWiseCreditLimitDto[],
    customer: string,
    voucherNo: string,
    voucherType: string,
    addLimit: boolean,
  ) {
    if (settings.brandWiseCreditLimit === false) {
      return of(null);
    }

    const headers = this.getServiceAccountApiHeaders(settings);
    return this.http
      .get(
        settings.authServerURL +
          `${FRAPPE_API_GET_CUSTOMER_ENDPOINT}/${customer}`,
        {
          headers,
        },
      )
      .pipe(
        switchMap(erpCustomerResponse => {
          const erpNextCustomer: CustomerWebhookDto =
            erpCustomerResponse.data.data;

          const payload = this.mapToNewBrandWiseAllocationsForErpNext(
            creditLimits,
            erpNextCustomer.custom_brand_wise_allocations,
            addLimit,
          );

          const ledgerEntries = this.mapCreditLedgerEntries(
            erpNextCustomer.custom_brand_wise_allocations,
            creditLimits,
            customer,
            voucherNo,
            voucherType,
            addLimit,
          );

          let otherBrandItems = creditLimits.slice(0);
          let otherBrandsTotal = 0;

          if (erpNextCustomer.custom_brand_wise_allocations) {
            otherBrandItems = otherBrandItems.filter(
              x =>
                erpNextCustomer.custom_brand_wise_allocations.find(
                  brandWiseAllocation => brandWiseAllocation.brand === x.brand,
                ) === undefined,
            );
          }

          otherBrandsTotal = otherBrandItems
            .map(x => x.limit)
            .reduce((acc, curr) => acc + curr, 0);

          payload.custom_other_brands_limit = addLimit
            ? erpNextCustomer.custom_other_brands_limit + otherBrandsTotal
            : erpNextCustomer.custom_other_brands_limit - otherBrandsTotal;

          const itemsTotalAmount = creditLimits
            .map(x => x.limit)
            .reduce((acc, curr) => acc + curr, 0);

          payload.excel_remaining_balance = addLimit
            ? erpNextCustomer.excel_remaining_balance + itemsTotalAmount
            : erpNextCustomer.excel_remaining_balance - itemsTotalAmount;
          return this.http
            .put(
              settings.authServerURL +
                `${FRAPPE_API_GET_CUSTOMER_ENDPOINT}/${customer}`,
              payload,
              {
                headers,
              },
            )
            .pipe(
              switchMap(res => {
                const data = res.data.data;
                const mongoPayload = this.mapToBrandWiseLimitsForMongo(data);

                return from(
                  this.updateCreditLimitInMongo(customer, mongoPayload),
                );
              }),
              switchMap(() => {
                return this.createLedgerEntries(ledgerEntries);
              }),
            );
        }),
      );
  }

  async createLedgerEntries(ledgerEntries: CreditLimitLedger[]) {
    for (const entry of ledgerEntries) {
      this.creditLimitLedgerSvc.createLedgerEntry(entry);
    }
  }

  mapCreditLedgerEntries(
    prevBrandAllocations: BrandWiseCreditLimitDto[],
    requestBrandLimits: BrandWiseCreditLimitDto[],
    customer: string,
    voucherNo: string,
    voucherType: string,
    addLimit: boolean,
  ): CreditLimitLedger[] {
    const ledgerEntries = [];

    for (const brandLimit of requestBrandLimits) {
      const prevAllocation = prevBrandAllocations.find(
        prev => prev.brand === brandLimit.brand,
      );

      if (!prevAllocation) {
        continue;
      }

      ledgerEntries.push({
        brand_id: brandLimit.brand,
        customer_id: customer,
        actual_qty: brandLimit.limit * (addLimit ? 1 : -1),
        balance_qty: prevAllocation.limit,
        doc_id: voucherNo,
        doc_type: voucherType,
        created_at: new Date(),
      });
    }

    return ledgerEntries;
  }

  async updateCreditLimitInMongo(customerName: string, payload) {
    return await this.updateOne({ name: customerName }, { $set: payload });
  }

  mapToBrandWiseLimitsForMongo(payload) {
    const brand_limits = payload.custom_brand_wise_allocations.map(
      creditLimit => {
        return {
          brand_id: creditLimit.brand,
          limit: creditLimit.limit,
        };
      },
    );

    return {
      brand_limits,
      others_credit_limits: payload.custom_other_brands_limit,
    };
  }

  mapToNewBrandWiseAllocationsForErpNext(
    requestItems: BrandWiseCreditLimitDto[],
    erpNextCreditLimits: BrandWiseCreditLimitDto[],
    addLimit: boolean,
  ) {
    const custom_brand_wise_allocations = erpNextCreditLimits.slice(0);
    const brands = [...new Set(requestItems.map(x => x.brand))];
    const groupedItems = brands.map(brand => ({
      brand,
      limit: requestItems
        .filter(x => x.brand === brand)
        .map(x => x.limit)
        .reduce((acc, curr) => acc + curr, 0),
    }));

    groupedItems.forEach(item => {
      const allocatedBrand = erpNextCreditLimits.find(limit => {
        return limit.brand === item.brand;
      });

      if (allocatedBrand) {
        allocatedBrand.limit = addLimit
          ? allocatedBrand.limit + item.limit
          : allocatedBrand.limit - item.limit;
      }
    });

    return {
      custom_brand_wise_allocations,
      custom_other_brands_limit: 0,
      excel_remaining_balance: 0,
    };
  }

  async list(skip, take, search, sort, territories: string[]) {
    const sortQuery = { name: sort };
    const columns = Object.keys(this.customerModel.schema.paths);
    const $or = this.handleSearchPatterns(columns, search);
    const customerQuery =
      territories && territories.length !== 0
        ? { territory: { $in: territories } }
        : {};

    const $and: any[] = [{ $or }, customerQuery];

    const where: { $and: any } = { $and };

    const results = await this.customerModel
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);

    return {
      docs: results || [],
      length: await this.customerModel.count(where),
      offset: skip,
    };
  }

  handleSearchPatterns(columns, search) {
    const searchQuery: any[] = [];

    columns.forEach(field => {
      const fieldType = this.customerModel.schema.paths[field].instance;
      const filter = {};

      if (fieldType === 'String') {
        filter[field] = { $regex: PARSE_REGEX(search), $options: 'i' };
      } else if (fieldType === 'Number') {
        const numericSearch = parseFloat(search);
        if (!isNaN(numericSearch)) {
          filter[field] = numericSearch;
        }
      } else if (fieldType === 'Array') {
        const arraySearch = Array.isArray(search) ? search : [search];
        filter[field] = { $in: arraySearch };
      }

      // Check if the filter object is not empty before pushing it into the array
      if (Object.keys(filter).length > 0) {
        searchQuery.push(filter);
      }
    });

    return searchQuery;
  }

  async deleteOne(query: any, options?): Promise<any> {
    return await this.customerModel.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.customerModel.updateOne(query, options);
  }
}
