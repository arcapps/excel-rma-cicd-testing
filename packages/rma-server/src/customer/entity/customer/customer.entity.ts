import { Column, Entity, Index, ObjectId, ObjectIdColumn } from 'typeorm';

export class BrandWiseCreditLimit {
  brand_id: string;
  limit: number;
}

export class CustomerCreditLimit {
  credit_limit: number;
  company: string;
}

@Entity()
export class Customer {
  @ObjectIdColumn()
  _id: ObjectId;

  @Column()
  uuid: string;

  @Column()
  @Index({ unique: true })
  name: string;

  @Column()
  company: string;

  @Column()
  owner: string;

  @Column()
  sales_team: any[];

  @Column()
  customer_name: string;

  @Column()
  customer_type: string;

  @Column()
  gst_category: string;

  @Column()
  customer_group: string;

  @Column()
  payment_terms: string;

  @Column()
  credit_days: number;

  @Column()
  territory: string;

  @Column()
  credit_limits: CustomerCreditLimit[];

  @Column()
  isSynced: boolean;

  @Column()
  baseCreditLimitAmount: number;

  @Column()
  tempCreditLimitPeriod: Date;

  @Column()
  creditLimitSetBy: string;

  @Column()
  creditLimitSetByFullName: string;

  @Column()
  creditLimitUpdatedOn: Date;

  @Column()
  excel_remaining_balance: number;

  @Column()
  brand_limits: BrandWiseCreditLimit[];

  @Column()
  others_credit_limits: number;
}
