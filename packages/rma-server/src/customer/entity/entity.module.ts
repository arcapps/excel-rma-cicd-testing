import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CreditLimitLedgerService } from '../../credit-limit-ledger/credit-limit-ledger-service/credit-limit-ledger.service';
import { CreditLimitLedgerSchema } from '../../credit-limit-ledger/schema/credit-limit-ledger.schema';
import { CustomerSchema } from '../schemas/customer.schema';
import { TerritorySchema } from '../schemas/territory.schema';
import { CustomerService } from './customer/customer.service';
import { TerritoryService } from './territory/territory.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Customer', schema: CustomerSchema }]),
    MongooseModule.forFeature([{ name: 'Territory', schema: TerritorySchema }]),
    MongooseModule.forFeature([
      { name: 'CreditLimitLedger', schema: CreditLimitLedgerSchema },
    ]),
  ],
  providers: [CustomerService, TerritoryService, CreditLimitLedgerService],
  exports: [CustomerService, TerritoryService],
})
export class CustomerEntitiesModule {}
