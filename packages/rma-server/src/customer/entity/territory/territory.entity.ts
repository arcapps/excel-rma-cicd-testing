import { Column, ObjectIdColumn, BaseEntity, ObjectId, Entity } from 'typeorm';

@Entity()
export class Territory extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectId;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  warehouse: string;
}
