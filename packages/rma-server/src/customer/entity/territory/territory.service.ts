import { Injectable } from '@nestjs/common';
import { Observable, firstValueFrom } from 'rxjs';
import { Territory } from './territory.entity';
import { Model } from 'mongoose';
import { TerritoryDocument } from '../../schemas/territory.schema';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class TerritoryService {
  constructor(
    @InjectModel('Territory') private territoryModel: Model<TerritoryDocument>,
  ) {}

  async find(query?) {
    const res = await this.territoryModel.find(query);
    return res;
  }

  async create(territoryPayload: Territory) {
    const customer = new Territory();
    Object.assign(customer, territoryPayload);
    return await new this.territoryModel(customer).save();
  }

  async findOne(options) {
    return await this.territoryModel.findOne(options);
  }

  async list(skip, take, search, sort, group) {
    const nameExp = new RegExp(search, 'i');
    let columns = Object.keys(this.territoryModel.schema.paths);
    columns = columns.filter(column => column !== '_id' && column !== '__v');
    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const where = { $or };

    if (group) {
      const response: any = await firstValueFrom(
        this.asyncAggregate([
          { $match: where },
          { $group: { _id: '$name', warehouse: { $push: '$warehouse' } } },
          { $project: { _id: 0, name: '$_id', warehouse: 1 } },
        ]),
      );

      return {
        length: response.length || 0,
        docs: response.splice(skip, take) || [],
        offset: skip,
      };
    }

    const results = await this.territoryModel
      .find(where)
      .skip(skip)
      .limit(take);

    return {
      docs: results || [],
      length: await this.territoryModel.countDocuments(where),
      offset: skip,
    };
  }

  asyncAggregate(query) {
    const promise = this.territoryModel.aggregate(query);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.territoryModel.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.territoryModel.updateOne(query, options);
  }
}
