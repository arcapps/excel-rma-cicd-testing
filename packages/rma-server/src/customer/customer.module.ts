import { Module } from '@nestjs/common';
import { CreditLimitLedgerEntitiesModule } from '../credit-limit-ledger/credit-limit-ledger-entities.module';
import {} from '../error-log/error-logs-invoice.module';
import { CustomerAggregatesManager } from './aggregates';
import { CustomerWebhookAggregateService } from './aggregates/customer-webhook-aggregate/customer-webhook-aggregate.service';
import { CustomerCommandManager } from './command';
import { CustomerWebhookController } from './controllers/customer-webhook/customer-webhook.controller';
import { CustomerController } from './controllers/customer/customer.controller';
import { TerritoryController } from './controllers/territory/territory.controller';
import { CustomerEntitiesModule } from './entity/entity.module';
import { CustomerEventManager } from './event';
import { CustomerPoliciesService } from './policies/customer-policies/customer-policies.service';
import { CustomerQueryManager } from './query';

@Module({
  imports: [CustomerEntitiesModule, CreditLimitLedgerEntitiesModule],
  controllers: [
    CustomerController,
    CustomerWebhookController,
    TerritoryController,
  ],
  providers: [
    ...CustomerAggregatesManager,
    ...CustomerQueryManager,
    ...CustomerEventManager,
    ...CustomerCommandManager,
    // Disconnected Credit Limit Cron Job Scheduler temporarily
    // ...CustomerSchedulers,
    CustomerPoliciesService,
    CustomerWebhookAggregateService,
  ],
  exports: [CustomerEntitiesModule, ...CustomerAggregatesManager],
})
export class CustomerModule {}
