import { IEvent } from '@nestjs/cqrs';
import { Customer } from '../../schemas/customer.schema';

export class CustomerAddedEvent implements IEvent {
  constructor(public customer: Customer, public clientHttpRequest: any) {}
}
