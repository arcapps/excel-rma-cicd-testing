import { HttpService } from '@nestjs/axios';
import { Injectable, NotImplementedException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { from, of, throwError } from 'rxjs';
import { map, retry, switchMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { CUSTOMER_ALREADY_EXISTS } from '../../../constants/app-strings';
import { FRAPPE_API_GET_PAYMENT_TERM_TEMPLATE_ENDPOINT } from '../../../constants/routes';
import { CreditLimitLedgerService } from '../../../credit-limit-ledger/credit-limit-ledger-service/credit-limit-ledger.service';
import { Customer } from '../../../customer/entity/customer/customer.entity';
import { ErrorLogService } from '../../../error-log/error-log-service/error-log.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import {
  BrandWiseCreditLimitDto,
  CustomerWebhookDto,
  PaymentTemplateTermsInterface,
} from '../../entity/customer/customer-webhook-interface';
import { CustomerService } from '../../entity/customer/customer.service';

@Injectable()
export class CustomerWebhookAggregateService extends AggregateRoot {
  constructor(
    private readonly clientTokenManager: ClientTokenManagerService,
    private readonly creditLimitLedgerSvc: CreditLimitLedgerService,
    private readonly customerService: CustomerService,
    private readonly errorLogService: ErrorLogService,
    private readonly http: HttpService,
    private readonly settingsService: SettingsService,
  ) {
    super();
  }

  customerCreated(customerWebhookPayload: CustomerWebhookDto) {
    return from(
      this.customerService.findOne({
        where: { name: customerWebhookPayload.name },
      }),
    ).pipe(
      switchMap(existingCustomer => {
        if (existingCustomer) {
          return of({ message: CUSTOMER_ALREADY_EXISTS });
        }

        const newCustomer = this.mapCustomer(customerWebhookPayload);
        newCustomer.brand_limits =
          customerWebhookPayload.custom_brand_wise_allocations.map(item => {
            return {
              brand_id: item.brand,
              limit: item.limit,
            };
          });

        if (customerWebhookPayload.payment_terms) {
          this.syncCustomerCreditDays(newCustomer);
        }

        this.customerService
          .create(newCustomer)
          .then(() => {})
          .catch(() => {});
        return of({});
      }),
    );
  }

  mapCustomer(customerPayload: CustomerWebhookDto) {
    const customer = new Customer();
    Object.assign(customer, customerPayload);
    customer.credit_limits = customerPayload.credit_limits
      ? customer.credit_limits
      : [];
    customer.uuid = uuidv4();
    customer.isSynced = customerPayload.payment_terms ? true : false;
    customer.others_credit_limits = customerPayload.custom_other_brands_limit;
    customer.excel_remaining_balance = customerPayload.excel_remaining_balance;
    return customer;
  }

  syncCustomerCreditDays(customer: Customer) {
    return this.settingsService
      .find()
      .pipe(
        switchMap(settings => {
          if (!settings.authServerURL) {
            return throwError(() => new NotImplementedException());
          }
          return this.clientTokenManager.getServiceAccountApiHeaders().pipe(
            switchMap(headers => {
              const url =
                settings.authServerURL +
                FRAPPE_API_GET_PAYMENT_TERM_TEMPLATE_ENDPOINT +
                customer.payment_terms;
              return this.http.get(url, { headers }).pipe(
                map(data => data.data.data),
                switchMap(
                  (response: {
                    template_name: string;
                    terms: PaymentTemplateTermsInterface[];
                  }) => {
                    const creditDays: number = this.mapCustomerCreditDays(
                      response.terms,
                    );
                    return this.customerService.updateOne(
                      { uuid: customer.uuid },
                      {
                        $set: {
                          credit_days: creditDays,
                          isSynced: true,
                        },
                      },
                    );
                  },
                ),
              );
            }),
            retry(3),
          );
        }),
      )
      .subscribe({
        next: () => {},
        error: err => {
          this.errorLogService.createErrorLog(err, 'Customer', 'webhook', {});
        },
      });
  }

  mapCustomerCreditDays(customerCredit: PaymentTemplateTermsInterface[]) {
    let credit_days: number;
    for (const eachCustomerCredit of customerCredit) {
      if (eachCustomerCredit.invoice_portion === 100) {
        credit_days = eachCustomerCredit.credit_days;
        break;
      }
    }
    return credit_days;
  }

  customerDeleted(customer: CustomerWebhookDto) {
    return from(this.customerService.deleteOne({ name: customer.name }));
  }

  customerUpdated(payload: CustomerWebhookDto) {
    return from(
      this.customerService.findOne({ where: { name: payload.name } }),
    ).pipe(
      switchMap(mongoCustomer => {
        if (!mongoCustomer) {
          this.customerCreated(payload).subscribe({
            next: () => {},
            error: () => {},
          });
          return of();
        }
        if (payload.payment_terms) {
          this.syncCustomerCreditDays(mongoCustomer);
        }

        payload.isSynced = true;
        mongoCustomer.payment_terms = payload.payment_terms;
        mongoCustomer.credit_limits = payload.credit_limits;
        mongoCustomer.customer_group = payload.customer_group;
        mongoCustomer.customer_name = payload.customer_name;
        mongoCustomer.customer_type = payload.customer_type;
        mongoCustomer.name = payload.name;
        mongoCustomer.owner = payload.owner;
        mongoCustomer.territory = payload.territory;
        mongoCustomer.sales_team = payload.sales_team;

        const brandLimitChanges: BrandWiseCreditLimitDto[] = [];

        return this.settingsService.find().pipe(
          switchMap(async settings => {
            if (settings.brandWiseCreditLimit) {
              this.addOrUpdateBrandLimits(
                mongoCustomer,
                payload,
                brandLimitChanges,
              );

              this.removeOldBrandLimits(
                mongoCustomer,
                payload,
                brandLimitChanges,
              );

              // Validate other brand limit
              if (
                payload.custom_other_brands_limit !==
                  mongoCustomer.others_credit_limits &&
                mongoCustomer.excel_remaining_balance >
                  payload.custom_other_brands_limit
              ) {
                mongoCustomer.others_credit_limits =
                  payload.custom_other_brands_limit;
              }

              mongoCustomer.excel_remaining_balance =
                payload.excel_remaining_balance;
            }

            return this.customerService
              .updateOne({ uuid: mongoCustomer.uuid }, { $set: mongoCustomer })
              .then(async success => {
                if (settings.brandWiseCreditLimit) {
                  await this.creditLimitLedgerSvc.updateCustomerLedger(
                    mongoCustomer,
                    brandLimitChanges,
                    payload.custom_source,
                  );
                }

                return success;
              })
              .catch(() => {});
          }),
        );
      }),
    );
  }

  private removeOldBrandLimits(
    mongoCustomer: Customer,
    erpNextCustomer: CustomerWebhookDto,
    brandLimitUpdates: BrandWiseCreditLimitDto[],
  ) {
    for (const mongoBrandLimit of mongoCustomer.brand_limits) {
      const erpNextBrandLimit =
        erpNextCustomer.custom_brand_wise_allocations.find(
          item => item.brand === mongoBrandLimit.brand_id,
        );

      if (!erpNextBrandLimit) {
        brandLimitUpdates.push({
          brand: mongoBrandLimit.brand_id,
          limit: -1 * mongoBrandLimit.limit,
        });

        mongoCustomer.brand_limits = mongoCustomer.brand_limits.filter(
          x => x.brand_id !== mongoBrandLimit.brand_id,
        );
      }
    }
  }

  private addOrUpdateBrandLimits(
    mongoCustomer: Customer,
    erpNextCustomer: CustomerWebhookDto,
    brandLimitChanges: BrandWiseCreditLimitDto[],
  ) {
    for (const erpNextBrandLimit of erpNextCustomer.custom_brand_wise_allocations) {
      const mongoBrandLimit = mongoCustomer.brand_limits.find(item => {
        return item.brand_id === erpNextBrandLimit.brand;
      });

      if (mongoBrandLimit) {
        brandLimitChanges.push({
          brand: mongoBrandLimit.brand_id,
          limit: erpNextBrandLimit.limit - mongoBrandLimit.limit,
        });

        mongoBrandLimit.limit = erpNextBrandLimit.limit;
      } else {
        brandLimitChanges.push({
          brand: erpNextBrandLimit.brand,
          limit: erpNextBrandLimit.limit,
        });

        mongoCustomer.brand_limits.push({
          brand_id: erpNextBrandLimit.brand,
          limit: erpNextBrandLimit.limit,
        });
      }
    }
  }

  // addBrandWiseCreditLimits(
  //   customer: Customer,
  //   customerWebhookPayload: CustomerWebhookDto,
  // ) {
  //   if (customerWebhookPayload.custom_brand_wise_allocations?.length) {
  //     customer.brand_limits =
  //       customerWebhookPayload.custom_brand_wise_allocations.map(item => {
  //         return {
  //           brand_id: item.brand,
  //           limit: item.limit,
  //         };
  //       });
  //   }
  // }
}
