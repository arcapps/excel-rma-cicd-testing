import {
  Body,
  Controller,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { FrappeWebhookPipe } from '../../../auth/guards/webhook.pipe';
import { CustomerWebhookAggregateService } from '../../aggregates/customer-webhook-aggregate/customer-webhook-aggregate.service';
import { CustomerWebhookDto } from '../../entity/customer/customer-webhook-interface';

@Controller('customer')
export class CustomerWebhookController {
  constructor(
    private readonly customerWebhookAggregate: CustomerWebhookAggregateService,
  ) {}

  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  customerCreated(@Body() customerPayload: CustomerWebhookDto) {
    return this.customerWebhookAggregate.customerCreated(customerPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  customerUpdated(@Body() customerPayload: CustomerWebhookDto) {
    return this.customerWebhookAggregate.customerUpdated(customerPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  customerDeleted(@Body() customerPayload: CustomerWebhookDto) {
    return this.customerWebhookAggregate.customerDeleted(customerPayload);
  }
}
