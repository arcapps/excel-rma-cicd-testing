import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export type TerritoryDocument = HydratedDocument<Territory>;

@Schema({ collection: 'territory' })
export class Territory {
  //     @Prop()
  //   _id: mongoose.Schema.Types.ObjectId;

  @Prop()
  uuid: string;

  @Prop()
  name: string;

  @Prop()
  warehouse: string;

  constructor() {
    if (!this.uuid) this.uuid = uuidv4();
  }
}

export const TerritorySchema = SchemaFactory.createForClass(Territory);
