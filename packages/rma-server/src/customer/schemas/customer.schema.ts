import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export type CustomerDocument = HydratedDocument<Customer>;

export class BrandWiseCreditLimit {
  brand_id: string;
  limit: number;
}

export class CustomerCreditLimit {
  credit_limit: number;
  company: string;
}

@Schema({ collection: 'customer' })
export class Customer {
  @Prop()
  uuid: string;

  @Prop({ unique: true })
  name: string;

  @Prop()
  company: string;

  @Prop()
  owner: string;

  @Prop()
  sales_team: any[];

  @Prop()
  customer_name: string;

  @Prop()
  customer_type: string;

  @Prop()
  gst_category: string;

  @Prop()
  customer_group: string;

  @Prop()
  payment_terms: string;

  @Prop()
  credit_days: number;

  @Prop()
  territory: string;

  @Prop()
  credit_limits: CustomerCreditLimit[];

  @Prop()
  isSynced: boolean;

  @Prop()
  baseCreditLimitAmount: number;

  @Prop()
  tempCreditLimitPeriod: Date;

  @Prop()
  creditLimitSetBy: string;

  @Prop()
  creditLimitSetByFullName: string;

  @Prop()
  creditLimitUpdatedOn: Date;

  @Prop()
  excel_remaining_balance: number;

  @Prop()
  brand_limits: BrandWiseCreditLimit[];

  @Prop()
  others_credit_limits: number;

  constructor() {
    if (!this.uuid) this.uuid = uuidv4();
  }
}

export const CustomerSchema = SchemaFactory.createForClass(Customer);
