import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type ErrorLogDocument = HydratedDocument<ErrorLog>;

@Schema({ collection: 'error_log' })
export class ErrorLog {
  @Prop()
  uuid: string;

  @Prop()
  docType: string;

  @Prop()
  entity: string;

  @Prop()
  url: string;

  @Prop()
  body: string;

  @Prop()
  method: string;

  @Prop()
  user: string;

  @Prop()
  token: string;

  @Prop()
  createdOn: Date;

  @Prop()
  error: string;

  @Prop()
  message: string;
}

export const ErrorLogSchema = SchemaFactory.createForClass(ErrorLog);
