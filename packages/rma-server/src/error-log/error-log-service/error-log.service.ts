// import { InjectRepository } from '@nestjs/typeorm';
import { ErrorLog } from '../schema/error-log.schema';
import { Injectable } from '@nestjs/common';
// import { MongoRepository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { PARSE_REGEX } from '../../constants/app-strings';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ErrorLogDocument } from '../schema/error-log.schema';

@Injectable()
export class ErrorLogService {
  constructor(
    // @InjectRepository(ErrorLog)
    // private readonly errorLogRepository: MongoRepository<ErrorLog>,
    @InjectModel('ErrorLog')
    private errorLogModel: Model<ErrorLogDocument>,
  ) {}

  async find() {
    return await this.errorLogModel.find();
  }

  async create(errorLog: ErrorLog) {
    return await new this.errorLogModel(errorLog).save();
  }

  async findOne(options) {
    return await this.errorLogModel.findOne(options.where);
  }

  async list(skip, take, sort, filter_query?) {
    let sortQuery;

    try {
      sortQuery = JSON.parse(sort);
    } catch (error) {
      sortQuery = { createdOn: 'desc' };
    }

    for (const key of Object.keys(sortQuery)) {
      sortQuery[key] = sortQuery[key].toUpperCase();
      if (!sortQuery[key]) {
        delete sortQuery[key];
      }
    }

    const $and: any[] = [filter_query ? this.getFilterQuery(filter_query) : {}];

    const where: { $and: any } = { $and };
    const results = await this.errorLogModel
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);

    return {
      docs: results || [],
      length: await this.errorLogModel.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, param?): Promise<any> {
    return await this.errorLogModel.deleteOne(query, param);
  }

  async updateOne(query, param) {
    return await this.errorLogModel.updateOne(query, param);
  }

  async updateMany(query, options?) {
    return await this.errorLogModel.updateMany(query, options);
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (query[key]) {
        if (typeof query[key] === 'string') {
          query[key] = { $regex: PARSE_REGEX(query[key]), $options: 'i' };
        } else {
          delete query[key];
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }

  createErrorLog(
    error,
    docType = '',
    entity = '',
    req?: {
      token?: { accessToken?: string; fullName?: string; email?: string };
    },
  ) {
    let frappeError;
    const errorLog = new ErrorLog();
    errorLog.createdOn = new Date();
    errorLog.error = error;
    errorLog.uuid = uuidv4();
    try {
      frappeError = JSON.parse(error.response.data._server_messages);
      frappeError = JSON.parse(frappeError);
      frappeError = (frappeError as { message?: string }).message;
      errorLog.message = frappeError;
    } catch {}
    try {
      errorLog.docType = docType;
      errorLog.entity = entity;
      errorLog.error =
        error.response && error.response.data && error.response.data.exc
          ? error.response.data.exc
          : error;
      errorLog.url = error.config.url || '';
      errorLog.body = error.config.data || '';
      errorLog.method = error.config.method || '';
      errorLog.token =
        req && req.token && req.token.accessToken ? req.token.accessToken : '';
      errorLog.user = req.token && req.token.fullName ? req.token.fullName : '';
    } catch {
      errorLog.error =
        error.response && error.response.data && error.response.data.exc
          ? error.response.data.exc
          : error;
    }
    this.create(errorLog)
      .then(success => {})
      .catch(err => {});
  }
}
