import { HttpService } from '@nestjs/axios';
import { BadRequestException, Injectable } from '@nestjs/common';
import { DateTime } from 'luxon';
import { forkJoin, from, of, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import {
  ACCEPT,
  APPLICATION_JSON_CONTENT_TYPE,
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  CONTENT_TYPE,
  DEFAULT_NAMING_SERIES,
  SUBMITTED_STATUS,
} from '../../../constants/app-strings';
import {
  ERPNEXT_PURCHASE_INVOICE_ENDPOINT,
  ERPNEXT_PURCHASE_ORDER_ENDPOINT,
  FRAPPE_API_GET_USER_INFO_ENDPOINT,
  FRAPPE_CLIENT_SUBMIT_ENDPOINT,
  MAP_PO_TO_PI_ENDPOINT,
} from '../../../constants/routes';
import { DirectService } from '../../../direct/aggregates/direct/direct.service';
import { ErrorLogService } from '../../../error-log/error-log-service/error-log.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ServerSettings } from '../../../system-settings/schemas/server-settings.schema';
import { PurchaseOrderWebhookDto } from '../../entity/purchase-order/purchase-order-webhook-dto';
import { PurchaseOrder } from '../../entity/purchase-order/purchase-order.entity';
import { PurchaseOrderService } from '../../entity/purchase-order/purchase-order.service';

@Injectable()
export class PurchaseOrderWebhookAggregateService {
  constructor(
    private readonly purchaseOrderService: PurchaseOrderService,
    private readonly clientToken: ClientTokenManagerService,
    private readonly settings: SettingsService,
    private readonly http: HttpService,
    private readonly direct: DirectService,
    private readonly errorLog: ErrorLogService,
  ) {}

  purchaseOrderCreated(purchaseOrderPayload: PurchaseOrderWebhookDto) {
    return forkJoin({
      purchaseOrder: from(
        this.purchaseOrderService.findOne({
          where: { name: purchaseOrderPayload.name },
        }),
      ),
      settings: this.settings.find(),
      purchaseOrderData: this.getPurchaseOrder(purchaseOrderPayload.name),
    }).pipe(
      switchMap(({ purchaseOrder, settings, purchaseOrderData }) => {
        const provider = this.mapPurchaseOrder(purchaseOrderData);
        const created_no = purchaseOrderPayload.transaction_date;
        provider.created_on = DateTime.fromISO(
          purchaseOrderPayload.transaction_date,
        )
          .setZone(settings.timeZone)
          .toJSDate();
        provider.update_stock = settings.updatePurchaseInvoiceStock;
        return this.getUserDetails(purchaseOrderPayload.owner).pipe(
          switchMap(user => {
            provider.created_by = user.full_name;
            if (purchaseOrder) {
              this.purchaseOrderService
                .updateOne({ name: purchaseOrder.name }, { $set: provider })
                .then(success => {})
                .catch(error => {});
            } else {
              this.purchaseOrderService
                .create(provider)
                .then(success => {})
                .catch(error => {});
              this.createPurchaseInvoice(
                purchaseOrderPayload,
                settings,
                created_no,
              );
            }
            return of({});
          }),
        );
      }),
    );
  }

  mapPurchaseOrder(purchaseOrderPayload: PurchaseOrderWebhookDto) {
    const purchaseOrder = new PurchaseOrder();
    Object.assign(purchaseOrder, purchaseOrderPayload);
    purchaseOrder.uuid = uuidv4();
    purchaseOrder.isSynced = true;
    purchaseOrder.status = SUBMITTED_STATUS;
    purchaseOrder.inQueue = false;
    purchaseOrder.submitted = true;
    return purchaseOrder;
  }

  getUserDetails(email: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        return this.http
          .get(
            settings.authServerURL + FRAPPE_API_GET_USER_INFO_ENDPOINT + email,
            { headers },
          )
          .pipe(map(res => res.data.data));
      }),
    );
  }

  getPurchaseOrder(name: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        return this.http
          .get(
            settings.authServerURL + ERPNEXT_PURCHASE_ORDER_ENDPOINT + name,
            { headers },
          )
          .pipe(map(res => res.data.data));
      }),
      catchError(err => {
        return throwError(() => new BadRequestException(err));
      }),
    );
  }

  createPurchaseInvoice(
    order: PurchaseOrderWebhookDto,
    settings: ServerSettings,
    created_on: string,
  ) {
    return this.direct
      .getUserAccessToken(order.owner)
      .pipe(
        map(token => {
          return {
            [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + token.accessToken,
          };
        }),
        catchError(error => this.clientToken.getServiceAccountApiHeaders()),
        switchMap(headers => {
          return this.http
            .get(settings.authServerURL + MAP_PO_TO_PI_ENDPOINT, {
              params: { source_name: order.name },
              headers,
            })
            .pipe(
              catchError(err => of(err)),
              map(res => res.data.message),
              switchMap(invoice => {
                invoice.posting_date = created_on;
                invoice.due_date = created_on;
                invoice.set_posting_time = 1;
                headers[ACCEPT] = APPLICATION_JSON_CONTENT_TYPE;
                headers[CONTENT_TYPE] = APPLICATION_JSON_CONTENT_TYPE;
                return this.http
                  .post(
                    settings.authServerURL + ERPNEXT_PURCHASE_INVOICE_ENDPOINT,
                    {
                      ...invoice,
                      owner: order.owner,
                      naming_series: DEFAULT_NAMING_SERIES.purchase_invoice,
                    },
                    { headers },
                  )
                  .pipe(map(res => res.data.data));
              }),
              switchMap(invoice => {
                return this.http
                  .post(
                    settings.authServerURL + FRAPPE_CLIENT_SUBMIT_ENDPOINT,
                    { doc: invoice },
                    { headers },
                  )
                  .pipe(map(data => data.data.message));
              }),
            );
        }),
      )
      .subscribe({
        next: invoice => {
          this.purchaseOrderService
            .updateOne(
              { name: order.name },
              { $set: { purchase_invoice_name: invoice.name } },
            )
            .then(success => {})
            .catch(error => {});
        },
        error: error => {
          try {
            error = JSON.stringify(error?.response?.data?.exc || error);
          } catch {}
          this.errorLog.createErrorLog(
            error,
            'Purchase Invoice',
            'Purchase Order',
          );
        },
      });
  }
}
