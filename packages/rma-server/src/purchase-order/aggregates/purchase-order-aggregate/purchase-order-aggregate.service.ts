import { HttpService } from '@nestjs/axios';
import { BadRequestException, Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { DateTime } from 'luxon';
import { Observable, forkJoin, from, of, throwError } from 'rxjs';
import { catchError, concatMap, map, switchMap, toArray } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import {
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  DOC_NAMES,
  DOC_RESET_INFO,
  PURCHASE_INVOICE_STATUS,
  PURCHASE_RECEIPT_DOCTYPE_NAME,
} from '../../../constants/app-strings';
import {
  FRAPPE_CLIENT_CANCEL,
  GET_FRAPPE_LINKED_DOCS_ENDPOINT,
} from '../../../constants/routes';
import { PurchaseInvoiceService } from '../../../purchase-invoice/entity/purchase-invoice/purchase-invoice.service';
import { PurchaseOrderItemDto } from '../../../purchase-order/entity/purchase-order/purchase-order-webhook-dto';
import { PurchaseReceiptService } from '../../../purchase-receipt/entity/purchase-receipt.service';
import { SerialNoHistoryService } from '../../../serial-no/entity/serial-no-history/serial-no-history.service';
import { SerialNoService } from '../../../serial-no/entity/serial-no/serial-no.service';
import { StockLedger } from '../../../stock-ledger/schema/stock-ledger.schema';
import { StockLedgerService } from '../../../stock-ledger/entity/stock-ledger/stock-ledger.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ServerSettings } from '../../../system-settings/schemas/server-settings.schema';
import { PurchaseOrder } from '../../entity/purchase-order/purchase-order.entity';
import { PurchaseOrderService } from '../../entity/purchase-order/purchase-order.service';
import { PurchaseOrderPoliciesService } from '../../policies/purchase-order-policies/purchase-order-policies.service';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';

@Injectable()
export class PurchaseOrderAggregateService extends AggregateRoot {
  constructor(
    private readonly purchaseOrderService: PurchaseOrderService,
    private readonly purchaseOrderPolicy: PurchaseOrderPoliciesService,
    private readonly serialHistoryService: SerialNoHistoryService,
    private readonly serialNoService: SerialNoService,
    private readonly purchaseInvoiceService: PurchaseInvoiceService,
    private readonly serverSettings: SettingsService,
    private readonly http: HttpService,
    private readonly purchaseReceiptService: PurchaseReceiptService,
    private readonly stockLedgerService: StockLedgerService,
  ) {
    super();
  }

  async retrievePurchaseOrder(params) {
    return await this.purchaseOrderService.findOne({ where: params });
  }

  getPurchaseOrderList(
    offset: number,
    limit: number,
    sort: string,
    filter_query: any,
  ) {
    return this.purchaseOrderService.list(offset, limit, sort, filter_query);
  }

  getERPNextLinkedDocs(
    docTypeName,
    docName,
    docInfo,
    settings: ServerSettings,
    clienthttpReq,
  ): Observable<{ message: { [key: string]: DocInfoInterface[] } }> {
    return this.http
      .post(
        settings.authServerURL + GET_FRAPPE_LINKED_DOCS_ENDPOINT,
        {
          doctype: docTypeName,
          name: docName,
          linkinfo: docInfo,
        },
        {
          headers: {
            [AUTHORIZATION]:
              BEARER_HEADER_VALUE_PREFIX + clienthttpReq.token.accessToken,
          },
        },
      )
      .pipe(map(data => data.data));
  }
  resetOrder(name: string, req) {
    return this.serverSettings.find().pipe(
      switchMap(settings => {
        return this.purchaseOrderPolicy
          .validatePurchaseOrderReset(name, settings, req)
          .pipe(
            switchMap(() => {
              return this.getERPNextLinkedDocs(
                DOC_NAMES.PURCHASE_INVOICE,
                name,
                DOC_RESET_INFO[DOC_NAMES.PURCHASE_INVOICE],
                settings,
                req,
              ).pipe(
                // switchMap((docs: any) => {
                //   const docsToProcess = docs.message.docs;
                //   if (docsToProcess.length > 0) {
                //     const observables = docsToProcess.map((doc: any) => {
                //       return this.cancelERPNextDocs(
                //         {
                //           [DOC_NAMES.PURCHASE_RECEIPT]: [doc.name],
                //         },
                //         req,
                //         settings,
                //       );
                //     });

                //     // Use forkJoin to wait for all observables to complete
                //     return forkJoin(observables);
                //   } else {
                //     return of({});
                //   }
                // }),
                switchMap(docs => {
                  const docsToProcess = docs.message.docs;

                  // Define a recursive function to process one document at a time
                  const processNextDoc = index => {
                    if (index < docsToProcess.length) {
                      const doc = docsToProcess[index];
                      return this.cancelDoc(
                        doc.doctype,
                        doc.name,
                        settings,
                        req,
                      ).pipe(
                        catchError(error => {
                          return of(null);
                        }),
                        switchMap(() => processNextDoc(index + 1)), // Process the next document
                      );
                    } else {
                      // All documents have been processed
                      return of({});
                    }
                  };

                  // Start processing the documents from index 0
                  return processNextDoc(0);
                }),
                switchMap(() => {
                  return this.cancelDoc(
                    DOC_NAMES.PURCHASE_INVOICE,
                    name,
                    settings,
                    req,
                  );
                }),
                switchMap(() => {
                  return this.purchaseOrderService.findOne({
                    where: { purchase_invoice_name: name },
                  });
                }),
                switchMap((purchaseOrder: PurchaseOrder) => {
                  return this.cancelDoc(
                    DOC_NAMES.PURCHASE_ORDER,
                    purchaseOrder.name,
                    settings,
                    req,
                  ).pipe(
                    switchMap(() => {
                      return forkJoin({
                        resetSerials: from(
                          this.serialNoService.deleteMany({
                            purchase_invoice_name:
                              purchaseOrder.purchase_invoice_name,
                          }),
                        ),

                        resetSerialHistory: from(
                          this.serialHistoryService.deleteMany({
                            parent_document:
                              purchaseOrder.purchase_invoice_name,
                          }),
                        ),
                        updatePurchaseOrder: from(
                          this.purchaseOrderService.updateOne(
                            { name: purchaseOrder.name },
                            {
                              $set: {
                                docstatus: 2,
                                status: PURCHASE_INVOICE_STATUS.CANCELED,
                              },
                            },
                          ),
                        ),
                        updatePurchaseInvoice: from(
                          this.purchaseInvoiceService.updateOne(
                            { name },
                            {
                              $set: {
                                docstatus: 2,
                                status: PURCHASE_INVOICE_STATUS.CANCELED,
                              },
                            },
                          ),
                        ),
                        updatePurchaseReceipt: from(
                          this.purchaseReceiptService.updateMany(
                            {
                              purchase_invoice_name:
                                purchaseOrder.purchase_invoice_name,
                            },
                            {
                              $set: {
                                docstatus: 2,
                                status: PURCHASE_INVOICE_STATUS.CANCELED,
                              },
                            },
                          ),
                        ),
                        purchaseOrderReset: this.createPurchaseResetLedger(
                          purchaseOrder,
                          req.token,
                          settings,
                        ),
                      });
                    }),
                  );
                }),
                switchMap(() => of(true)),
              );
            }),
          );
      }),
    );
  }
  createPurchaseResetLedger(
    purchaseOrder: PurchaseOrder,
    token,
    settings: ServerSettings,
  ) {
    return from(purchaseOrder.items).pipe(
      concatMap((item: PurchaseOrderItemDto) => {
        return this.createStockLedgerPayload(
          {
            pr_no: purchaseOrder.purchase_invoice_name,
            purchaseReciept: item,
          },
          token,
          settings,
        ).pipe(
          switchMap(() => {
            // return from(this.stockLedgerService.create(response));
            return from(
              this.stockLedgerService.deleteMany({
                voucher_no: purchaseOrder.purchase_invoice_name,
              }),
            );
          }),
        );
      }),
      toArray(),
    );
  }

  getDeliveredQuantity(purchase_invoice_name: string, item_code: string) {
    return from(
      this.purchaseReceiptService.find({ purchase_invoice_name, item_code }),
    ).pipe(
      switchMap(purchaseReciept => {
        if (purchaseReciept.length) {
          return of(purchaseReciept.find(x => x).qty);
        }
        return of(0);
      }),
    );
  }

  createStockLedgerPayload(
    payload: { pr_no: string; purchaseReciept: PurchaseOrderItemDto },
    token,
    settings: ServerSettings,
  ) {
    return forkJoin({
      purchaseDelvieredQty: this.getDeliveredQuantity(
        payload.pr_no,
        payload.purchaseReciept.item_code,
      ),
      fiscalYear: this.serverSettings.getFiscalYear(settings),
    }).pipe(
      switchMap(delieverdState => {
        payload.purchaseReciept.qty = delieverdState.purchaseDelvieredQty;
        const date = new DateTime(settings.timeZone).toJSDate();
        const stockPayload = new StockLedger();
        stockPayload.name = uuidv4();
        stockPayload.modified = date;
        stockPayload.modified_by = token.email;
        stockPayload.warehouse = payload.purchaseReciept.warehouse;
        stockPayload.item_code = payload.purchaseReciept.item_code;
        stockPayload.actual_qty = -payload.purchaseReciept.qty;
        stockPayload.incoming_rate = payload.purchaseReciept.rate;
        stockPayload.valuation_rate = 0;
        stockPayload.batch_no = '';
        stockPayload.stock_uom = payload.purchaseReciept.stock_uom;
        stockPayload.posting_date = date;
        stockPayload.posting_time = date;
        stockPayload.voucher_type = PURCHASE_RECEIPT_DOCTYPE_NAME;
        stockPayload.voucher_no = payload.pr_no;
        stockPayload.voucher_detail_no = '';
        stockPayload.outgoing_rate = 0;
        stockPayload.qty_after_transaction = stockPayload.actual_qty;
        stockPayload.stock_value =
          stockPayload.qty_after_transaction * stockPayload.valuation_rate || 0;
        stockPayload.stock_value_difference =
          stockPayload.actual_qty * stockPayload.valuation_rate || 0;
        stockPayload.company = settings.defaultCompany;
        stockPayload.fiscal_year = delieverdState.fiscalYear;
        return of(stockPayload);
      }),
    );
  }
  // function for calculate valuation
  calculateValuationRate(
    preQty,
    incomingQty,
    incomingRate,
    preValuation,
    totalQty,
  ) {
    let result =
      (preQty * preValuation + incomingQty * incomingRate) / totalQty;
    result = Math.round(result);
    return result;
  }

  cancelERPNextDocs(docs: { [key: string]: string[] }, req, settings) {
    return of({}).pipe(
      switchMap(obj => {
        return from(Object.keys(docs)).pipe(
          concatMap((docType: string) => {
            if (!docs[docType]?.length) {
              return of(true);
            }
            return from(docs[docType]).pipe(
              concatMap(doc => {
                return this.cancelDoc(docType, doc, settings, req);
              }),
              switchMap(() => of(true)),
            );
          }),
          catchError(err => {
            if (
              err?.response?.data?.exc &&
              err?.response?.data?.exc.includes(
                'Cannot edit cancelled document',
              )
            ) {
              return of(true);
            }
            return throwError(
              () => new BadRequestException(err?.response?.data?.exc),
            );
          }),
        );
      }),
      toArray(),
    );
  }

  cancelDoc(doctype, docName, settings: ServerSettings, req) {
    const doc = {
      doctype,
      name: docName,
    };
    return this.http.post(settings.authServerURL + FRAPPE_CLIENT_CANCEL, doc, {
      headers: {
        [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + req.token.accessToken,
      },
    });
  }
}

export interface DocInfoInterface {
  posting_date: string;
  grand_total: number;
  name: string;
  modified: string;
  docstatus: number;
  doctype: string;
}
