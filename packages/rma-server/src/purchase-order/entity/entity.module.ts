import { Module } from '@nestjs/common';
import { PurchaseOrderService } from './purchase-order/purchase-order.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PurchaseOrderSchema } from '../schema/purchase-order.schema';

@Module({
  // imports: [TypeOrmModule.forFeature([PurchaseOrder])],
  imports: [
    MongooseModule.forFeature([
      { name: 'PurchaseOrder', schema: PurchaseOrderSchema },
    ]),
  ],
  providers: [PurchaseOrderService],
  exports: [PurchaseOrderService],
})
export class PurchaseOrderEntitiesModule {}
