import { HttpService } from '@nestjs/axios';
import { QueryBus } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { PurchaseOrderAggregateService } from '../../aggregates/purchase-order-aggregate/purchase-order-aggregate.service';
import { PurchaseOrderController } from './purchase-order.controller';

describe('PurchaseOrder Controller', () => {
  let controller: PurchaseOrderController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PurchaseOrderController],
      providers: [
        { provide: QueryBus, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
        { provide: TokenCacheService, useValue: {} },
        { provide: HttpService, useValue: {} },
        { provide: SettingsService, useValue: {} },
        { provide: QueryBus, useValue: {} },
        { provide: PurchaseOrderAggregateService, useValue: {} },
      ],
    })
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();

    controller = module.get<PurchaseOrderController>(PurchaseOrderController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
