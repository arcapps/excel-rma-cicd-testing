import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { PurchaseReceiptService } from './purchase-receipt.service';

describe('RequestStateService', () => {
  let service: PurchaseReceiptService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PurchaseReceiptService,
        {
          provide: getModelToken('PurchaseReceipt'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<PurchaseReceiptService>(PurchaseReceiptService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
