// import { InjectRepository } from '@nestjs/typeorm';
// import { PurchaseReceipt } from './purchase-receipt.entity';
import { Injectable } from '@nestjs/common';
// import { MongoRepository } from 'typeorm';
import { PARSE_REGEX } from '../../constants/app-strings';
import { Observable } from 'rxjs';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  PurchaseReceipt,
  PurchaseReceiptDocument,
} from '../schema/purchase-receipt.schema';

@Injectable()
export class PurchaseReceiptService {
  constructor(
    // @InjectRepository(PurchaseReceipt)
    // private readonly purchaseReceiptRepository: MongoRepository<PurchaseReceipt>,
    @InjectModel('PurchaseReceipt')
    private purchaseReceiptModel: Model<PurchaseReceiptDocument>,
  ) {}

  async find(options?) {
    return await this.purchaseReceiptModel.find(options.where);
  }

  async create(purchaseReceipt: PurchaseReceipt) {
    const res = await new this.purchaseReceiptModel(purchaseReceipt).save();
    return res;
  }

  async findOne(options) {
    return await this.purchaseReceiptModel.findOne(options.where);
  }

  asyncAggregate(query) {
    const promise = this.purchaseReceiptModel.aggregate(query);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  async list(skip, take, sort, filter_query?) {
    let sortQuery;

    try {
      sortQuery = JSON.parse(sort);
    } catch (error) {
      sortQuery = { createdOn: 'desc' };
    }

    for (const key of Object.keys(sortQuery)) {
      sortQuery[key] = sortQuery[key].toUpperCase();
      if (!sortQuery[key]) {
        delete sortQuery[key];
      }
    }

    const $and: any[] = [filter_query ? this.getFilterQuery(filter_query) : {}];

    const where: { $and: any } = { $and };
    const results = await this.purchaseReceiptModel
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);

    return {
      docs: results || [],
      length: await this.purchaseReceiptModel.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, param?): Promise<any> {
    return await this.purchaseReceiptModel.deleteOne(query, param);
  }

  async updateOne(query, param) {
    return await this.purchaseReceiptModel.updateOne(query, param);
  }

  async updateMany(query, options?) {
    return await this.purchaseReceiptModel.updateMany(query, options);
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (query[key]) {
        if (typeof query[key] === 'string') {
          query[key] = { $regex: PARSE_REGEX(query[key]), $options: 'i' };
        } else {
          delete query[key];
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }

  async insertMany(query, options?) {
    return await this.purchaseReceiptModel.insertMany(query, options);
  }
}
