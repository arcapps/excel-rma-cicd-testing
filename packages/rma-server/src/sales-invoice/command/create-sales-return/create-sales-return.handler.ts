import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { firstValueFrom } from 'rxjs';
import { SalesInvoiceAggregateService } from '../../aggregates/sales-invoice-aggregate/sales-invoice-aggregate.service';
import { CreateSalesReturnCommand } from './create-sales-return.command';

@CommandHandler(CreateSalesReturnCommand)
export class CreateSalesReturnHandler
  implements ICommandHandler<CreateSalesReturnCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: SalesInvoiceAggregateService,
  ) {}
  async execute(command: CreateSalesReturnCommand) {
    const { createReturnPayload, req } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await firstValueFrom(
      this.manager.createSalesReturn(createReturnPayload, req),
    );
    aggregate.commit();
  }
}
