import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { firstValueFrom } from 'rxjs';
import { SalesInvoiceAggregateService } from '../../aggregates/sales-invoice-aggregate/sales-invoice-aggregate.service';
import { AddSalesInvoiceCommand } from './add-sales-invoice.command';

@CommandHandler(AddSalesInvoiceCommand)
export class AddSalesInvoiceHandler
  implements ICommandHandler<AddSalesInvoiceCommand>
{
  constructor(
    private publisher: EventPublisher,
    private manager: SalesInvoiceAggregateService,
  ) {}
  async execute(command: AddSalesInvoiceCommand) {
    const { salesInvoicePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const salesInvoice = await firstValueFrom(
      aggregate.addSalesInvoice(salesInvoicePayload, clientHttpRequest),
    );
    aggregate.commit();
    return salesInvoice;
  }
}
