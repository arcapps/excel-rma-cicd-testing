import { IsNotEmpty, IsOptional } from 'class-validator';

import { Type } from 'class-transformer';
import { SalesInvoiceDto } from './sales-invoice-dto';
import { AssignSerialDto } from '../../../serial-no/entity/serial-no/assign-serial-dto';

export class AssignDto {
  @IsNotEmpty()
  @Type(() => SalesInvoiceDto)
  salesInvoice: SalesInvoiceDto;

  @IsNotEmpty()
  @Type(() => AssignSerialDto)
  assignPayload: AssignSerialDto;
}
