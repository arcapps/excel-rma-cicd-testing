import { IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Type } from 'class-transformer';

export class ReferenceDto {
  @IsOptional()
  @IsString()
  reference_doctype: string;

  @IsOptional()
  @IsString()
  reference_name: string;

  @IsOptional()
  @IsInt()
  allocated_amount: number;
}

export class PaymentDto {
  @IsOptional()
  @IsInt()
  doc_status: number;

  @IsNotEmpty()
  @IsString()
  party_type: string;

  @IsOptional()
  @IsString()
  party: string;

  @IsInt()
  paid_amount: number;

  @IsInt()
  received_amount: number;

  @IsOptional()
  @IsInt()
  target_exchange_rate: number;

  @IsNotEmpty()
  @IsString()
  mode_of_payment: string;

  @IsNotEmpty()
  @IsString()
  paid_to: string;

  @IsNotEmpty()
  @IsString()
  account_paid_to: string;

  @IsOptional()
  @Type(() => ReferenceDto)
  references: ReferenceDto[];
}
