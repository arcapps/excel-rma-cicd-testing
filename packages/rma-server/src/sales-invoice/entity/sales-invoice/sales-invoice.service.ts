import { Injectable } from '@nestjs/common';
// import { InjectRepository } from '@nestjs/typeorm';
import { Observable } from 'rxjs';
// import { switchMap } from 'rxjs/operators';
// import { MongoRepository } from 'typeorm';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { ALL_TERRITORIES } from '../../../constants/app-strings';
import { SalesInvoice } from '../../schema/sales-invoice.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SalesInvoiceDocument } from '../../schema/sales-invoice.schema';

@Injectable()
export class SalesInvoiceService {
  constructor(
    @InjectModel('SalesInvoice')
    private salesInvoiceModel: Model<SalesInvoiceDocument>,
  ) {}

  async find() {
    return await this.salesInvoiceModel.find();
  }

  async create(salesInvoice: SalesInvoice) {
    return await new this.salesInvoiceModel(salesInvoice).save();
  }

  async findOne(option, param?, flag = false) {
    const select: any[] = this.getColumns();
    flag ? select.splice(select.indexOf('delivery_note_items'), 1) : select;
    return await this.salesInvoiceModel.findOne(option.where).select(select);
  }

  findById(id): Observable<any> {
    const promise = this.salesInvoiceModel.findOne({ uuid: id });
    return new Observable<SalesInvoice>(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  async list(skip, take, sort, filter_query, territories: string[]) {
    let sortQuery;
    let dateQuery = {};
    try {
      sortQuery = JSON.parse(sort);
    } catch (error) {
      sortQuery = { 'timeStamp.created_on': 'desc' };
    }
    sortQuery =
      Object.keys(sortQuery).length === 0
        ? { 'timeStamp.created_on': 'desc' }
        : sortQuery;
    for (const key of Object.keys(sortQuery)) {
      sortQuery[key] = sortQuery[key].toUpperCase();
      if (sortQuery[key] === 'ASC') {
        sortQuery[key] = 1;
      }
      if (sortQuery[key] === 'DESC') {
        sortQuery[key] = -1;
      }
      if (!sortQuery[key]) {
        delete sortQuery[key];
      }
    }

    if (filter_query?.fromDate && filter_query?.toDate) {
      dateQuery = {
        'timeStamp.created_on': {
          $gte: new Date(filter_query.fromDate),
          $lte: new Date(filter_query.toDate),
        },
      };
    }

    const salesTeamQuery = filter_query.sales_team
      ? {
          sales_team: { $elemMatch: { sales_person: filter_query.sales_team } },
        }
      : {};

    const customerQuery =
      territories?.length && !territories?.includes(ALL_TERRITORIES)
        ? { territory: { $in: territories } }
        : {};

    const $and: any[] = [
      filter_query ? this.getFilterQuery(filter_query) : {},
      dateQuery,
      salesTeamQuery,
      customerQuery,
    ];

    // const $group = this.getKeys();

    const where: { $and: any } = { $and };

    // const results = await firstValueFrom(
    //   this.aggregateList(skip, take, where, sortQuery, $group),
    // );
    // const projection = this.getKeys();
    const results = await this.salesInvoiceModel
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);

    return {
      docs: results || [],
      length: await this.salesInvoiceModel.count(where),
      offset: skip,
    };
  }

  aggregateList($skip = 0, $limit = 10, $match, $sort, $group) {
    return this.asyncAggregate(
      [
        { $match },
        {
          $unwind: {
            path: '$delivery_note_items',
            preserveNullAndEmptyArrays: true,
          },
        },
        { $group },
        { $sort },
        { $skip },
        { $limit },
      ],
      {
        allowDiskUse: true,
      },
    );
  }

  async deleteOne(query, param?): Promise<any> {
    return await this.salesInvoiceModel.deleteOne(query, param);
  }

  async updateOne(query, param) {
    return await this.salesInvoiceModel.updateOne(query, param);
  }

  async updateOneWithOptions(query, param, options?) {
    return await this.salesInvoiceModel.updateOne(query, param, options);
  }

  async updateMany(query, options?) {
    return await this.salesInvoiceModel.updateMany(query, options);
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (typeof query[key] !== 'undefined') {
        if (key === 'status' && query[key] === 'All') {
          delete query[key];
        } else if (key === 'isCampaign' && query[key] === true) {
          query[key] = true;
        } else if (key === 'territory' && !query[key]) {
          delete query[key];
        } else if (key === 'sales_team') {
          delete query[key];
        } else if (key === 'isCampaign' && query[key] === false) {
          query[key] = false;
        } else if (key === 'fromDate' || key === 'toDate') {
          delete query[key];
        } else {
          query[key] = { $regex: query[key], $options: 'i' };
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }

  asyncAggregate(query, options?) {
    const promise = this.salesInvoiceModel.aggregate(query);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  async count(query) {
    return await this.salesInvoiceModel.count(query);
  }

  async findAndModify(query, params) {
    return await this.salesInvoiceModel.findOneAndUpdate(query, params, {
      sort: { 'timeStamp.created_no': 1 },
    });
  }

  // getKeys() {
  //   const group: any = {};
  //   const keys = this.getColumns();
  //   keys.splice(keys.indexOf('_id'), 1);
  //   keys.splice(keys.indexOf('delivery_note_items'), 1);

  //   group._id = '$' + '_id';
  //   group.delivered_by = { $addToSet: '$delivery_note_items.deliveredBy' };
  //   keys.forEach(key => {
  //     group[key] = {
  //       $first: '$' + key,
  //     };
  //   });
  //   return group;
  // }

  getKeys() {
    const group: any = {};
    const keys = [
      'name',
      'status',
      'posting_date',
      'posting_time',
      'customer_name',
      'total',
      'territory',
      'remarks',
      'uuid',
    ];
    const deliveredByProjection = {
      delivered_by: { $addToSet: '$delivery_note_items.deliveredBy' },
      timeStamp: { $first: '$timeStamp.created_on' },
    };

    group._id = '$' + '_id';
    keys.forEach(key => {
      group[key] = {
        $first: '$' + key,
      };
    });

    // Add the deliveredByProjection to the group
    Object.assign(group, deliveredByProjection);

    return group;
  }

  getColumns() {
    return Object.keys(this.salesInvoiceModel.schema.paths);
  }

  async findPosDrafts() {
    return await this.salesInvoiceModel.find({ type: 'pos', status: 'Draft' });
  }
}
