import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CreditLimitLedgerService } from '../../credit-limit-ledger/credit-limit-ledger-service/credit-limit-ledger.service';
import { CreditLimitLedgerSchema } from '../../credit-limit-ledger/schema/credit-limit-ledger.schema';
import { SalesInvoiceSchema } from '../schema/sales-invoice.schema';
import { SalesInvoiceService } from './sales-invoice/sales-invoice.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'SalesInvoice', schema: SalesInvoiceSchema },
    ]),
    MongooseModule.forFeature([
      { name: 'CreditLimitLedger', schema: CreditLimitLedgerSchema },
    ]),
  ],
  providers: [SalesInvoiceService, CreditLimitLedgerService],
  exports: [SalesInvoiceService],
})
export class SalesInvoiceEntitiesModule {}
