import { IEvent } from '@nestjs/cqrs';
import { SalesInvoice } from '../../schema/sales-invoice.schema';

export class SalesInvoiceAddedEvent implements IEvent {
  constructor(
    public salesInvoice: SalesInvoice,
    public clientHttpRequest: any,
  ) {}
}
