import { IEvent } from '@nestjs/cqrs';
import { SalesInvoice } from '../../schema/sales-invoice.schema';

export class SalesInvoiceSubmittedEvent implements IEvent {
  constructor(public salesInvoice: SalesInvoice) {}
}
