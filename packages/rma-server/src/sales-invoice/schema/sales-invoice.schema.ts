import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type SalesInvoiceDocument = HydratedDocument<SalesInvoice>;

export class Timestamp {
  created_on: Date;
}

@Schema({ collection: 'sales_invoice' })
export class SalesInvoice {
  @Prop()
  uuid: string;

  @Prop()
  created_on: Date;

  @Prop()
  name: string;

  @Prop()
  naming_series: string;

  @Prop()
  customer_name: string;

  @Prop()
  outstanding_amount: string;

  @Prop()
  is_return: boolean;

  @Prop()
  issue_credit_note: boolean;

  @Prop()
  title: string;

  @Prop()
  customer: string;

  @Prop()
  company: string;

  @Prop()
  posting_date: string;

  @Prop()
  posting_time: string;

  @Prop()
  set_posting_time: number;

  @Prop()
  due_date: string;

  @Prop()
  address_display: string;

  @Prop()
  contact_person: string;

  @Prop()
  contact_display: string;

  @Prop()
  contact_email: string;

  @Prop()
  territory: string;

  @Prop()
  delivery_warehouse: string;

  @Prop()
  update_stock: boolean;

  @Prop()
  total_qty: number;

  @Prop()
  base_total: number;

  @Prop()
  base_net_total: number;

  @Prop()
  total: number;

  @Prop()
  net_total: number;

  @Prop()
  pos_total_qty: number;

  @Prop()
  return_delivery_note: string;

  @Prop()
  items: Item[];

  @Prop({ type: [], default: [] })
  delivery_note_items: any[];

  @Prop({ type: [String], default: [] })
  delivery_note_names: string[];

  @Prop({ type: Array, default: [] })
  returned_items: any[];

  @Prop({ type: Object, default: {} })
  delivered_items_map: any;

  @Prop({ type: Object, default: {} })
  bundle_items_map: any;

  @Prop({ type: Object, default: {} })
  returned_items_map: any;

  @Prop()
  pricing_rules: [];

  @Prop()
  packed_items: [];

  @Prop()
  timesheets: [];

  @Prop()
  taxes: Tax[];

  @Prop()
  advances: [];

  @Prop()
  payment_schedule: [];

  @Prop()
  payments: Payments[];

  @Prop()
  sales_team: [];

  @Prop()
  status: string;

  @Prop()
  createdBy: string;

  @Prop()
  createdByEmail: string;

  @Prop()
  submitted: boolean;

  @Prop()
  credit_note: string;

  @Prop()
  timeStamp: Timestamp;

  @Prop()
  inQueue: boolean;

  @Prop()
  isSynced: boolean;

  @Prop()
  isCampaign: boolean;

  @Prop()
  remarks: string;

  @Prop()
  has_bundle_item: boolean;

  @Prop()
  modified: string;

  @Prop()
  delivery_status: string;

  @Prop()
  transactionLock: number;

  @Prop()
  is_pos: boolean;

  @Prop()
  pos_profile: string;
}

export const SalesInvoiceSchema = SchemaFactory.createForClass(SalesInvoice);

export class Tax {
  name: string;
  charge_type: string;
  tax_amount: number;
  total: number;
  account_head: string;
  description: string;
  rate: number;
}

export class Item {
  name: string;
  owner: string;
  item_code: string;
  has_serial_no?: number;
  has_bundle_item?: boolean;
  is_stock_item?: number;
  item_name: string;
  qty: number;
  rate: number;
  amount: number;
  serial_no?: string;
  excel_serials: string;
  price_list_rate?: number;
  brand?: string;
}

export class Payments {
  mode_of_payment?: string;
  default?: boolean;
  amount?: number;
  account?: string;
  excel_serials: string;
}
