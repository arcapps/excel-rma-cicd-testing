import { IQuery } from '@nestjs/cqrs';

export class RetrieveDeliveryNotesQuery implements IQuery {
  constructor(
    public readonly salesInvoiceName: string,
    public readonly req: any,
  ) {}
}
