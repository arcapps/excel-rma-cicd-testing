import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { firstValueFrom, from, switchMap } from 'rxjs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { SalesInvoiceListQueryDto } from '../../../constants/listing-dto/sales-invoice-list-query';
import { CancelSalesReturnCommand } from '../../../sales-invoice/command/cancel-sales-return/cancel-sales-return.command';
import { SalesReturnCancelDto } from '../../../sales-invoice/entity/sales-invoice/sales-return-cancel-dto';
import { SalesInvoiceAggregateService } from '../../aggregates/sales-invoice-aggregate/sales-invoice-aggregate.service';
import { SalesInvoiceResetAggregateService } from '../../aggregates/sales-invoice-reset-aggregate/sales-invoice-reset-aggregate.service';
import { AddSalesInvoiceCommand } from '../../command/add-sales-invoice/add-sales-invoice.command';
import { CreateSalesReturnCommand } from '../../command/create-sales-return/create-sales-return.command';
import { RemoveSalesInvoiceCommand } from '../../command/remove-sales-invoice/remove-sales-invoice.command';
import { SubmitSalesInvoiceCommand } from '../../command/submit-sales-invoice/submit-sales-invoice.command';
import { UpdateSalesInvoiceCommand } from '../../command/update-sales-invoice/update-sales-invoice.command';
import { SalesInvoiceDto } from '../../entity/sales-invoice/sales-invoice-dto';
import { SalesInvoiceUpdateDto } from '../../entity/sales-invoice/sales-invoice-update-dto';
import { CreateSalesReturnDto } from '../../entity/sales-invoice/sales-return-dto';
import { RetrieveSalesInvoiceQuery } from '../../query/get-sales-invoice/retrieve-sales-invoice.query';
import { RetrieveSalesInvoiceListQuery } from '../../query/list-sales-invoice/retrieve-sales-invoice-list.query';
import { RetrieveDeliveryNotesQuery } from '../../query/get-delivery-notes/get-delivery-notes.query';
import { AssignSerialNoCommand } from '../../../serial-no/command/assign-serial-no/assign-serial-no.command';
import { AssignDto } from '../../entity/sales-invoice/pos-invoice-dto';
import { ValidateSerialsQuery } from '../../../serial-no/query/validate-serial/validate-serial.query';
@Controller('sales_invoice')
export class SalesInvoiceController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly salesInvoiceAggregate: SalesInvoiceAggregateService,
    private readonly salesInvoiceResetAggregate: SalesInvoiceResetAggregateService,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async create(@Body() salesInvoicePayload: SalesInvoiceDto, @Req() req) {
    return await this.commandBus.execute(
      new AddSalesInvoiceCommand(salesInvoicePayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveSalesInvoiceCommand(uuid));
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateClient(@Body() updatePayload: SalesInvoiceUpdateDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdateSalesInvoiceCommand(updatePayload, req),
    );
  }

  @Post('v1/submit/:uuid')
  @UseGuards(TokenGuard)
  @UsePipes()
  submitSalesInvoice(@Param('uuid') uuid: string, @Req() req) {
    return this.commandBus.execute(new SubmitSalesInvoiceCommand(uuid, req));
  }

  @Post('v1/pos-submit')
  @UseGuards(TokenGuard)
  @UsePipes()
  async submitPOSInvoice(@Body() assignDto: AssignDto, @Req() req) {
    const array: any = [];
    for (const x of assignDto.assignPayload.items) {
      if (x.has_serial_no) {
        const validation: any = {
          item_code: x?.item_code,
          serials: x?.serial_no,
          warehouse: assignDto?.assignPayload.set_warehouse,
        };
        const response = await this.queryBus.execute(
          new ValidateSerialsQuery(validation, req, undefined),
        );
        array.push(response);
      }
    }
    if (array.length) {
      const allEmpty = array.every(item => item.notFoundSerials.length === 0);
      if (!allEmpty) {
        return array;
      }
    }
    const draft = await this.commandBus.execute(
      new AddSalesInvoiceCommand(assignDto.salesInvoice, req),
    );
    return from(
      this.commandBus.execute(new SubmitSalesInvoiceCommand(draft?.uuid, req)),
    ).pipe(
      switchMap(() => {
        return this.salesInvoiceAggregate.retrieveSalesInvoice(draft.uuid, req);
      }),
      switchMap(value => {
        assignDto.assignPayload.sales_invoice_name = value.name;
        const newPayload = assignDto.assignPayload.items.map(x => ({
          against_sales_invoice: value.name,
          ...x,
        }));
        assignDto.assignPayload.items = newPayload;
        return this.commandBus.execute(
          new AssignSerialNoCommand(assignDto.assignPayload, req),
        );
      }),
    );
  }

  @Post('v1/draft')
  @UseGuards(TokenGuard)
  @UsePipes()
  async submitDraft(@Body() saleInvoiceDto: SalesInvoiceDto, @Req() req) {
    saleInvoiceDto.type = 'pos';
    return await this.commandBus.execute(
      new AddSalesInvoiceCommand(saleInvoiceDto, req),
    );
  }

  @Get('v1/list/pos-draft')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listPosDraft(@Req() req) {
    return await this.salesInvoiceAggregate.getPosDraft();
  }

  @Post('v1/cancel/:uuid')
  @UseGuards(TokenGuard)
  cancelSalesInvoice(@Param('uuid') uuid: string, @Req() req) {
    return this.salesInvoiceResetAggregate.cancel(uuid, req);
  }

  @Put('v1/cancel_return/:creditNoteName')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe())
  cancelSalesReturn(@Body() cancelReturnDto: SalesReturnCancelDto, @Req() req) {
    return this.commandBus.execute(
      new CancelSalesReturnCommand(cancelReturnDto, req),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getSalesInvoice(@Param('uuid') uuid, @Req() req) {
    return await this.queryBus.execute(
      new RetrieveSalesInvoiceQuery(uuid, req),
    );
  }

  @Get('v1/get-delivery-notes/:name')
  @UseGuards(TokenGuard)
  async getDeliveryNotes(@Param('name') salesInvoiceName, @Req() req) {
    return await this.queryBus.execute(
      new RetrieveDeliveryNotesQuery(salesInvoiceName, req),
    );
  }

  @Get('v1/get-delivery-notes-items/:name')
  @UseGuards(TokenGuard)
  async getDeliveryNoteItems(@Param('name') deliveryNoteName, @Req() req) {
    return await firstValueFrom(
      this.salesInvoiceAggregate.getDeliveryNoteItems(deliveryNoteName, req),
    );
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listSalesInvoice(@Query() query: SalesInvoiceListQueryDto, @Req() req) {
    const { offset, limit, sort, filter_query } = query;
    let filter = {};
    try {
      filter = JSON.parse(filter_query);
    } catch {
      filter;
    }
    return await this.queryBus.execute(
      new RetrieveSalesInvoiceListQuery(offset, limit, sort, filter, req),
    );
  }

  @Post('v1/create_return')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  createReturn(@Body() createReturnPayload: CreateSalesReturnDto, @Req() req) {
    return this.commandBus.execute(
      new CreateSalesReturnCommand(createReturnPayload, req),
    );
  }

  @Post('v1/update_outstanding_amount/:name')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateOutstandingAmount(@Param('name') invoice_name: string) {
    return await firstValueFrom(
      this.salesInvoiceAggregate.updateOutstandingAmount(invoice_name),
    );
  }

  @Post('v1/update_delivery_status')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateDeliveryStatus(@Body() payload) {
    return await this.salesInvoiceAggregate.updateDeliveryStatus(payload);
  }

  @Post('v1/update_mrp_rate/:name')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateSalesInvoiceItemMRPRate(
    @Param('name') invoice_name: string,
    @Req() req,
  ) {
    return this.salesInvoiceAggregate.updateSalesInvoiceItemMRPRate(
      invoice_name,
      req,
    );
  }
}
