import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { SERVICE } from '../../constants/app-strings';

export type ServerSettingsDocument = HydratedDocument<ServerSettings>;

export class CustomerCreditLimit {
  credit_limit: number;
  company: string;
}

export interface BrandSettings {
  faviconURL: string;
}

@Schema({ collection: 'server_settings' })
export class ServerSettings {
  // @Prop()
  // _id: mongoose.Schema.Types.ObjectId;

  @Prop()
  uuid: string;

  @Prop()
  appURL: string;

  @Prop()
  warrantyAppURL: string;

  @Prop()
  posAppURL: string;

  @Prop()
  authServerURL: string;

  @Prop()
  frontendClientId: string;

  @Prop()
  backendClientId: string;

  @Prop()
  serviceAccountUser: string;

  @Prop()
  serviceAccountSecret: string;

  @Prop()
  profileURL: string;

  @Prop()
  headerImageURL: string;

  @Prop()
  footerImageURL: string;

  @Prop()
  headerWidth: number;

  @Prop()
  footerWidth: number;

  @Prop()
  tokenURL: string;

  @Prop()
  authorizationURL: string;

  @Prop()
  revocationURL: string;

  @Prop()
  service: string = SERVICE;

  @Prop()
  cloudStorageSettings: string;

  @Prop()
  callbackProtocol: string;

  @Prop()
  scope: string[];

  @Prop()
  webhookApiKey: string;

  @Prop()
  frontendCallbackURLs: string[];

  @Prop()
  backendCallbackURLs: string[];

  @Prop()
  defaultCompany: string;

  @Prop()
  sellingPriceList: string;

  @Prop()
  timeZone: string;

  @Prop()
  validateStock: boolean;

  @Prop()
  transferWarehouse: string;

  @Prop()
  debtorAccount: string;

  @Prop()
  serviceAccountApiKey: string;

  @Prop()
  serviceAccountApiSecret: string;

  @Prop()
  posProfile: string;

  @Prop({ type: Object })
  brand: BrandSettings;

  @Prop()
  backdatedInvoices: boolean;

  @Prop()
  backdatedInvoicesForDays: number;

  @Prop()
  updateSalesInvoiceStock: boolean;

  @Prop()
  updatePurchaseInvoiceStock: boolean;

  @Prop()
  brandWiseCreditLimit: boolean;

  @Prop()
  displayCategory: boolean;

  @Prop()
  displayPaymentData: boolean;

  @Prop()
  displayPromotionalOffer: boolean;

  @Prop()
  displaySerial: boolean;

  @Prop()
  displayStock: boolean;

  constructor() {
    if (!this.uuid) this.uuid = uuidv4();
  }
}

export const ServerSettingsSchema =
  SchemaFactory.createForClass(ServerSettings);
