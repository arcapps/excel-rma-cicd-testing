import { Controller, Get } from '@nestjs/common';
import { HealthCheck } from '@nestjs/terminus';
// import { HealthCheckAggregateService } from '../../aggregates/health-check/health-check.service';

@Controller('healthz')
export class HealthController {
  constructor() {} // private checkService: HealthCheckAggregateService, // private health: HealthCheckService,

  @Get()
  @HealthCheck()
  healthCheck() {
    return {};
  }
}
