import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { ServerSettingsService } from './server-settings.service';

describe('ServerSettingsService', () => {
  let service: ServerSettingsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ServerSettingsService,
        {
          provide: getModelToken('ServerSettings'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ServerSettingsService>(ServerSettingsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
