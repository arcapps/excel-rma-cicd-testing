import { IsBoolean, IsNotEmpty } from 'class-validator';

export class AccessSettingDto {
  @IsNotEmpty()
  @IsBoolean()
  flag: boolean;
}
