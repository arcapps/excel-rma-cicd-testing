import { Injectable } from '@nestjs/common';
import {
  ServerSettings,
  ServerSettingsDocument,
} from '../../schemas/server-settings.schema';
import { settingsAlreadyExists } from '../../../constants/exceptions';

import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class ServerSettingsService {
  constructor(
    @InjectModel('ServerSettings')
    private serverSettingsModel: Model<ServerSettingsDocument>,
  ) {}

  async save(params) {
    let serverSettings;
    if (params.uuid) {
      const exists: number = await this.count();
      serverSettings = await this.findOne({ where: { uuid: params.uuid } });
      serverSettings.appURL = params.appURL;
      if (exists > 0 && !serverSettings) {
        throw settingsAlreadyExists;
      }
      serverSettings.save();
    } else {
      Object.assign(serverSettings, params);
    }
    const createdService = new this.serverSettingsModel(serverSettings);
    return await createdService.save(serverSettings);
  }

  async find(): Promise<ServerSettings> {
    const settings = await this.serverSettingsModel.find().exec();
    return settings.length ? settings[0] : null;
  }

  async findOne(params) {
    return await this.serverSettingsModel.findOne(params);
  }

  async updateOne(query, params) {
    return await this.serverSettingsModel.updateOne(query, params);
  }

  async updateMany(query, params) {
    return await this.serverSettingsModel.updateMany(query, params);
  }

  async count() {
    return this.serverSettingsModel.count();
  }
}
