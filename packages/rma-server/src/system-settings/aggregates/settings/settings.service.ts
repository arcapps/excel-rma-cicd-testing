import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotImplementedException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { randomBytes } from 'crypto';
import { Observable, forkJoin, from, of, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
// import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import {
  ACCEPT,
  APPLICATION_JSON_CONTENT_TYPE,
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  CONTENT_TYPE,
  HUNDRED_NUMBER_STRING,
  TOKEN_HEADER_VALUE_PREFIX,
} from '../../../constants/app-strings';
import {
  COMPANY_NOT_FOUND_ON_FRAPPE,
  DEFAULT_COMPANY_ALREADY_EXISTS,
  PLEASE_RUN_SETUP,
} from '../../../constants/messages';
import {
  CUSTOMER_AFTER_INSERT_ENDPOINT,
  CUSTOMER_ON_TRASH_ENDPOINT,
  CUSTOMER_ON_UPDATE_ENDPOINT,
  DELIVERY_NOTE_AFTER_INSERT_ENDPOINT,
  DELIVERY_NOTE_ON_TRASH_ENDPOINT,
  DELIVERY_NOTE_ON_UPDATE_ENDPOINT,
  EXCEL_BACKGROUND_AFTER_INSERT_ENDPOINT,
  EXCEL_PRODUCT_BUNDLE_AFTER_UPDATE_ENDPOINT,
  FRAPPE_API_COMPANY_ENDPOINT,
  FRAPPE_API_FISCAL_YEAR_ENDPOINT,
  FRAPPE_API_GET_GLOBAL_DEFAULTS,
  FRAPPE_API_GET_SYSTEM_SETTINGS,
  FRAPPE_API_GET_USER_INFO_ENDPOINT,
  FRAPPE_API_GET_USER_PERMISSION_ENDPOINT,
  ITEM_AFTER_INSERT_ENDPOINT,
  ITEM_ON_TRASH_ENDPOINT,
  ITEM_ON_UPDATE_ENDPOINT,
  PURCHASE_INVOICE_ON_CANCEL_ENDPOINT,
  PURCHASE_INVOICE_ON_SUBMIT_ENDPOINT,
  PURCHASE_ORDER_ON_SUBMIT_ENDPOINT,
  PURCHASE_RECEIPT_ON_CANCEL_ENDPOINT,
  SALES_INVOICE_ON_CANCEL_ENDPOINT,
  SALES_INVOICE_ON_SUBMIT_ENDPOINT,
  SUPPLIER_AFTER_INSERT_ENDPOINT,
  SUPPLIER_ON_TRASH_ENDPOINT,
  SUPPLIER_ON_UPDATE_ENDPOINT,
  // TOKEN_ADD_ENDPOINT,
  // TOKEN_DELETE_ENDPOINT,
} from '../../../constants/routes';
import {
  dataImportAfterInsertWebhookData,
  deliveryNoteNoAfterInsertWebhookData,
  deliveryNoteOnTrashWebhookData,
  deliveryNoteOnUpdateWebhookData,
  // getBearerTokenAfterInsertWebhookData,
  // getBearerTokenOnTrashWebhookData,
  getCustomerAfterInsertWebhookData,
  getCustomerOnTrashWebhookData,
  getCustomerOnUpdateWebhookData,
  getItemAfterInsertWebhookData,
  getItemOnTrashWebhookData,
  getItemOnUpdateWebhookData,
  getSupplierAfterInsertWebhookData,
  getSupplierOnTrashWebhookData,
  getSupplierOnUpdateWebhookData,
  itemBundleAfterUpdateWebhookData,
  purchaseInvoiceOnCancelWebhookData,
  purchaseInvoiceOnSubmitWebhookData,
  purchaseOrderOnSubmitWebhookData,
  purchaseReceiptOnCancelWebhookData,
  salesInvoiceOnCancelWebhookData,
  salesInvoiceOnSubmitWebhookData,
} from '../../../constants/webhook-data';
import {
  FrappeGlobalDefaultsInterface,
  FrappeSystemSettingsInterface,
} from '../../../system-settings/entities/server-settings/server-defaults-interface';
import { ServerSettings } from '../../schemas/server-settings.schema';
import { ServerSettingsService } from '../../entities/server-settings/server-settings.service';
import { TokenCache } from '../../../auth/schemas/tokenCache.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ConnectService } from '../../../auth/aggregates/connect/connect.service';
import { AccessSettingDto } from '../../../system-settings/entities/server-settings/access-item-dto';

@Injectable()
export class SettingsService extends AggregateRoot {
  constructor(
    @InjectModel('ServerSettings')
    private serverSettingsModel: Model<ServerSettings>,
    private readonly serverSettingsService: ServerSettingsService,
    private readonly clientToken: ClientTokenManagerService,
    private readonly http: HttpService,
    private readonly connectService: ConnectService,
  ) {
    super();
  }

  find(): Observable<ServerSettings> {
    const promise = this.serverSettingsService.find();
    return new Observable<ServerSettings>(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  updateMany(query, params) {
    return from(this.serverSettingsService.updateMany(query, params));
  }

  getAuthorizationHeaders(token: TokenCache) {
    const headers: any = {};
    headers[AUTHORIZATION] = BEARER_HEADER_VALUE_PREFIX + token.accessToken;
    headers[ACCEPT] = APPLICATION_JSON_CONTENT_TYPE;
    return headers;
  }

  async updateAccessSetting(accessSettingDto: AccessSettingDto) {
    const result = await this.serverSettingsModel.updateOne(
      {},
      { $set: accessSettingDto },
    );
    return result;
  }

  getFiscalYear(serverSettings: ServerSettings) {
    const url = `${serverSettings.authServerURL}${FRAPPE_API_FISCAL_YEAR_ENDPOINT}`;
    const headers = {};
    headers[AUTHORIZATION] = TOKEN_HEADER_VALUE_PREFIX;
    headers[AUTHORIZATION] += serverSettings.serviceAccountApiKey + ':';
    headers[AUTHORIZATION] += serverSettings.serviceAccountApiSecret;
    return this.http.get(url, { headers }).pipe(
      map(data => data.data.data),
      switchMap((response: { name: string }[]) => {
        return of(response.find(fiscalYear => fiscalYear).name);
      }),
    );
  }
  async updateFrappeWebhookKey() {
    const settings = await this.serverSettingsService.find();
    if (!settings) throw new NotImplementedException(PLEASE_RUN_SETUP);
    settings.webhookApiKey = this.randomBytes();
    const createdService = new this.serverSettingsModel(settings);
    await createdService.save();
    return settings;
  }

  randomBytes(length: number = 64) {
    return randomBytes(length).toString('hex');
  }

  setupWebhooks() {
    let serverSettings: ServerSettings;
    const headers = {};

    headers[CONTENT_TYPE] = APPLICATION_JSON_CONTENT_TYPE;
    headers[ACCEPT] = APPLICATION_JSON_CONTENT_TYPE;

    return this.find().pipe(
      switchMap(settings => {
        serverSettings = settings;
        return this.clientToken.getServiceAccountApiHeaders();
      }),
      switchMap(authHeaders => {
        headers[AUTHORIZATION] = authHeaders[AUTHORIZATION];
        return forkJoin([
          // Item Webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              getItemAfterInsertWebhookData(
                serverSettings.appURL + ITEM_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              getItemOnUpdateWebhookData(
                serverSettings.appURL + ITEM_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              getItemOnTrashWebhookData(
                serverSettings.appURL + ITEM_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Customer Webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              getCustomerAfterInsertWebhookData(
                serverSettings.appURL + CUSTOMER_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              getCustomerOnUpdateWebhookData(
                serverSettings.appURL + CUSTOMER_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              getCustomerOnTrashWebhookData(
                serverSettings.appURL + CUSTOMER_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Supplier Webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              getSupplierAfterInsertWebhookData(
                serverSettings.appURL + SUPPLIER_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              getSupplierOnUpdateWebhookData(
                serverSettings.appURL + SUPPLIER_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              getSupplierOnTrashWebhookData(
                serverSettings.appURL + SUPPLIER_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // OAuth Bearer Token Webhooks
          // this.http
          //   .post(
          //     serverSettings.authServerURL + '/api/resource/Webhook',
          //     getBearerTokenAfterInsertWebhookData(
          //       serverSettings.appURL + TOKEN_ADD_ENDPOINT,
          //       serverSettings.webhookApiKey,
          //     ),
          //     { headers },
          //   )
          //   .pipe(map(res => res.data)),
          // this.http
          //   .post(
          //     serverSettings.authServerURL + '/api/resource/Webhook',
          //     getBearerTokenAfterInsertWebhookData(
          //       serverSettings.appURL + TOKEN_ADD_ENDPOINT,
          //       serverSettings.webhookApiKey,
          //       'on_update',
          //     ),
          //     { headers },
          //   )
          //   .pipe(map(res => res.data)),
          // this.http
          //   .post(
          //     serverSettings.authServerURL + '/api/resource/Webhook',
          //     getBearerTokenOnTrashWebhookData(
          //       serverSettings.appURL + TOKEN_DELETE_ENDPOINT,
          //       serverSettings.webhookApiKey,
          //     ),
          //     { headers },
          //   )
          //   .pipe(map(res => res.data)),

          // Delivery Note Webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              deliveryNoteOnUpdateWebhookData(
                serverSettings.appURL + DELIVERY_NOTE_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              deliveryNoteOnTrashWebhookData(
                serverSettings.appURL + DELIVERY_NOTE_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              deliveryNoteNoAfterInsertWebhookData(
                serverSettings.appURL + DELIVERY_NOTE_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Purchase invoice webhook

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              purchaseInvoiceOnSubmitWebhookData(
                serverSettings.appURL + PURCHASE_INVOICE_ON_SUBMIT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              purchaseInvoiceOnCancelWebhookData(
                serverSettings.appURL + PURCHASE_INVOICE_ON_CANCEL_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Sales invoice webhook

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              salesInvoiceOnCancelWebhookData(
                serverSettings.appURL + SALES_INVOICE_ON_CANCEL_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              salesInvoiceOnSubmitWebhookData(
                serverSettings.appURL + SALES_INVOICE_ON_SUBMIT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              purchaseOrderOnSubmitWebhookData(
                serverSettings.appURL + PURCHASE_ORDER_ON_SUBMIT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              purchaseReceiptOnCancelWebhookData(
                serverSettings.appURL + PURCHASE_RECEIPT_ON_CANCEL_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              dataImportAfterInsertWebhookData(
                serverSettings.appURL + EXCEL_BACKGROUND_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              itemBundleAfterUpdateWebhookData(
                serverSettings.appURL +
                  EXCEL_PRODUCT_BUNDLE_AFTER_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),
        ]);
      }),
      catchError(error => {
        return throwError(() => new InternalServerErrorException(error));
      }),
    );
  }

  getUserRoles(settings: ServerSettings, token: any) {
    return this.http
      .get(
        settings.authServerURL +
          FRAPPE_API_GET_USER_INFO_ENDPOINT +
          token.email,
        { headers: this.getAuthorizationHeaders(token) },
      )
      .pipe(map(res => res.data.data));
  }

  getUserTerritories(settings: ServerSettings, token: any) {
    const params = {
      filters: JSON.stringify([
        ['allow', '=', 'Territory'],
        ['user', '=', token.email],
      ]),
      fields: JSON.stringify(['for_value']),
      limit_page_length: HUNDRED_NUMBER_STRING,
    };
    return this.http
      .get(settings.authServerURL + FRAPPE_API_GET_USER_PERMISSION_ENDPOINT, {
        headers: this.getAuthorizationHeaders(token),
        params,
      })
      .pipe(
        map(res => res.data.data),
        switchMap((userTerritories: { for_value: string }[]) => {
          const territory = [
            ...this.connectService.mapUserTerritory(userTerritories),
          ];
          return this.connectService.getTerritoryWarehouse(territory).pipe(
            switchMap((warehouses: string[]) => {
              if (!warehouses || !warehouses.length) {
                return this.connectService.getAllWarehouses(
                  settings.authServerURL,
                  this.getAuthorizationHeaders(token),
                );
              }
              return of({ warehouses, territory });
            }),
          );
        }),
      );
  }

  getUserProfile(req) {
    return this.find().pipe(
      switchMap(settings => {
        return forkJoin([
          this.getUserRoles(settings, req.token),
          this.getUserTerritories(settings, req.token),
        ]).pipe(
          map((res: any) => {
            return {
              roles: this.connectService.mapUserRoles(res[0].roles),
              territory: res[1].territory,
              warehouse: res[1].warehouses,
            };
          }),
        );
        //   return this.http
        //     .get(
        //       settings.authServerURL +
        //         FRAPPE_API_GET_USER_INFO_ENDPOINT +
        //         req.token.email,
        //       { headers: this.getAuthorizationHeaders(req.token) },
        //     )
        //     .pipe(map(res => res.data));
      }),
      catchError(err => {
        return throwError(() => new BadRequestException(err));
      }),
    );
    // return await this.tokenService.findOne({
    //   accessToken: req.token.accessToken,
    // });
  }

  setDefaultCompany(companyName: string, req) {
    return this.find().pipe(
      switchMap(settings => {
        if (settings.defaultCompany) {
          return throwError(
            new BadRequestException(DEFAULT_COMPANY_ALREADY_EXISTS),
          );
        }
        return this.assignCompany(settings, companyName, req);
      }),
    );
  }

  updateDefaultCompany(companyName: string, req) {
    return this.find().pipe(
      switchMap(settings => {
        return this.assignCompany(settings, companyName, req);
      }),
    );
  }

  assignCompany(settings: ServerSettings, companyName: string, req) {
    return this.http
      .get(
        settings.authServerURL +
          FRAPPE_API_COMPANY_ENDPOINT +
          `/${companyName}`,
        { headers: this.getAuthorizationHeaders(req.token) },
      )
      .pipe(
        switchMap(company => {
          this.serverSettingsService
            .updateOne(
              { uuid: settings.uuid },
              { $set: { defaultCompany: companyName } },
            )
            .then(success => {})
            .catch(error => {});
          return of();
        }),
        catchError(err => {
          return throwError(
            new BadRequestException(COMPANY_NOT_FOUND_ON_FRAPPE),
          );
        }),
      );
  }

  relayListCompanies(query) {
    return this.clientToken.getServiceAccountApiHeaders().pipe(
      switchMap(headers => {
        return from(this.serverSettingsService.find()).pipe(
          switchMap(settings => {
            const url = settings.authServerURL + FRAPPE_API_COMPANY_ENDPOINT;
            return this.http
              .get(url, {
                headers,
                params: query,
              })
              .pipe(map(res => res.data));
          }),
        );
      }),
    );
  }

  relayListDefaults() {
    return this.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(
            () => new NotImplementedException(PLEASE_RUN_SETUP),
          );
        }
        return this.clientToken.getServiceAccountApiHeaders().pipe(
          switchMap(headers => {
            return this.http
              .get(settings.authServerURL + FRAPPE_API_GET_GLOBAL_DEFAULTS, {
                headers,
              })
              .pipe(
                map(data => data.data.data),
                switchMap((globalDefaults: FrappeGlobalDefaultsInterface) => {
                  return this.http
                    .get(
                      settings.authServerURL + FRAPPE_API_GET_SYSTEM_SETTINGS,
                      { headers },
                    )
                    .pipe(
                      map(data => data.data.data),
                      switchMap(
                        (systemSettings: FrappeSystemSettingsInterface) => {
                          return of({
                            default_company: globalDefaults.default_company,
                            country: globalDefaults.country,
                            default_currency: globalDefaults.default_currency,
                            time_zone: systemSettings.time_zone,
                            transferWarehouse: settings.transferWarehouse,
                            brand: settings.brand,
                            update_sales_invoice_stock:
                              settings.updateSalesInvoiceStock,
                            update_purchase_invoice_stock:
                              settings.updatePurchaseInvoiceStock,
                            backdated_permissions: settings.backdatedInvoices,
                            backdated_permissions_for_days:
                              settings.backdatedInvoicesForDays,
                          });
                        },
                      ),
                    );
                }),
              );
          }),
        );
      }),
    );
  }
}
