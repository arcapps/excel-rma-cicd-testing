import { IEvent } from '@nestjs/cqrs';
import { PurchaseInvoice } from '../../schemas/purchase-invoice.schema';

export class PurchaseInvoiceUpdatedEvent implements IEvent {
  constructor(public updatePayload: PurchaseInvoice) {}
}
