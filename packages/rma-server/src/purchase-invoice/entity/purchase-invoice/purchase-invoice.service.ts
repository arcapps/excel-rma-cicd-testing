import { Injectable } from '@nestjs/common';
import { firstValueFrom, Observable } from 'rxjs';
import {
  PurchaseInvoice,
  PurchaseInvoiceDocument,
} from '../../schemas/purchase-invoice.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class PurchaseInvoiceService {
  constructor(
    @InjectModel('PurchaseInvoice')
    private purchaseInvoiceModel: Model<PurchaseInvoiceDocument>,
  ) {}

  async find(query?) {
    return await this.purchaseInvoiceModel.find(query);
  }

  async create(purchaseInvoice: PurchaseInvoice) {
    return await new this.purchaseInvoiceModel(purchaseInvoice).save();
  }

  async findOne(options) {
    return await this.purchaseInvoiceModel.findOne(options.where);
  }

  aggregateList($skip = 0, $limit = 10, $match, $sort) {
    return this.asyncAggregate([{ $match }, { $sort }, { $skip }, { $limit }]);
  }

  asyncAggregate(query) {
    const promise = this.purchaseInvoiceModel.aggregate(query);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  async list(skip, take, sort, filter_query?) {
    let sortQuery;
    let dateQuery = {};

    try {
      sortQuery = JSON.parse(sort);
    } catch (error) {
      sortQuery = { created_on: 'desc' };
    }
    sortQuery =
      Object.keys(sortQuery).length === 0 ? { created_on: 'desc' } : sortQuery;

    for (const key of Object.keys(sortQuery)) {
      sortQuery[key] = sortQuery[key].toUpperCase();
      if (sortQuery[key] === 'ASC') {
        sortQuery[key] = 1;
      }
      if (sortQuery[key] === 'DESC') {
        sortQuery[key] = -1;
      }
      if (!sortQuery[key]) {
        delete sortQuery[key];
      }
    }

    if (filter_query && filter_query.fromDate && filter_query.toDate) {
      dateQuery = {
        created_on: {
          $gte: new Date(filter_query.fromDate),
          $lte: new Date(filter_query.toDate),
        },
      };
    }

    const $and: any[] = [
      filter_query ? this.getFilterQuery(filter_query) : {},
      dateQuery,
    ];

    const where: { $and: any } = { $and };

    const results = await firstValueFrom(
      this.aggregateList(skip, take, where, sortQuery),
    );

    return {
      docs: results || [],
      length: await this.purchaseInvoiceModel.count(where),
      offset: skip,
    };
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (query[key]) {
        if (key === 'status' && query[key] === 'All') {
          delete query[key];
        } else if (key === 'fromDate' || key === 'toDate') {
          delete query[key];
        } else {
          query[key] = { $regex: query[key], $options: 'i' };
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.purchaseInvoiceModel.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.purchaseInvoiceModel.updateOne(query, options);
  }

  async insertMany(query, options?) {
    return await this.purchaseInvoiceModel.insertMany(query, options);
  }
}
