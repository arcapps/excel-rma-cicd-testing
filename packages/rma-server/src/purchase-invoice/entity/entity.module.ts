import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
// import { PurchaseInvoice } from './purchase-invoice/purchase-invoice.entity';
import { PurchaseInvoiceService } from './purchase-invoice/purchase-invoice.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PurchaseInvoiceSchema } from '../schemas/purchase-invoice.schema';

@Module({
  // TypeOrmModule.forFeature([PurchaseInvoice]),
  imports: [
    MongooseModule.forFeature([
      { name: 'PurchaseInvoice', schema: PurchaseInvoiceSchema },
    ]),
  ],
  providers: [PurchaseInvoiceService],
  exports: [PurchaseInvoiceService],
})
export class PurchaseInvoiceEntitiesModule {}
