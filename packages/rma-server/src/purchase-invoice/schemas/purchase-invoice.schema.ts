import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';

export type PurchaseInvoiceDocument = HydratedDocument<PurchaseInvoice>;

@Schema({ collection: 'purchase_invoice' })
export class PurchaseInvoice {
  @Prop()
  uuid: string;

  @Prop()
  created_on: Date;

  @Prop()
  created_by: string;

  @Prop()
  docstatus: number;

  @Prop()
  is_paid: number;

  @Prop()
  is_return: number;

  @Prop()
  update_stock: number;

  @Prop()
  total_qty: number;

  @Prop()
  base_total: number;

  @Prop()
  total: number;

  @Prop()
  total_advance: number;

  @Prop()
  outstanding_amount: number;

  @Prop()
  paid_amount: number;

  @Prop({ unique: true })
  name: string;

  @Prop()
  title: string;

  @Prop()
  supplier: string;

  @Prop()
  supplier_name: string;

  @Prop()
  naming_series: string;

  @Prop()
  due_date: string;

  @Prop()
  company: string;

  @Prop()
  posting_date: string;

  @Prop()
  posting_time: string;

  @Prop()
  supplier_address: string;

  @Prop()
  address_display: string;

  @Prop()
  buying_price_list: string;

  @Prop()
  in_words: string;

  @Prop()
  credit_to: string;

  @Prop()
  against_expense_account: string;

  @Prop()
  items: PurchaseInvoiceItem[];

  @Prop()
  pricing_rules: any[];

  @Prop()
  supplied_items: any[];

  @Prop()
  taxes: any[];

  @Prop()
  advances: PurchaseInvoiceAdvances[];

  @Prop()
  payment_schedule: PurchaseInvoicePaymentSchedule[];

  @Prop()
  status: string;

  @Prop()
  submitted: boolean;

  @Prop()
  inQueue: boolean;

  @Prop()
  isSynced: boolean;

  @Prop()
  deliveredBy: string[];

  @Prop()
  purchase_receipt_names: string[];

  @Prop({ type: mongoose.Schema.Types.Mixed })
  purchase_receipt_items_map: any = {};
}

export class PurchaseReceiptMetaData {
  purchase_document_type: string;
  purchase_document_no: string;
  purchase_invoice_name: string;
  amount: number;
  cost_center: string;
  expense_account: string;
  item_code: string;
  item_name: string;
  name: string;
  qty: number;
  rate: number;
  serial_no: string[];
  warehouse: string;
  deliveredBy: string;
  deliveredByEmail: string;
}

export class PurchaseInvoicePaymentSchedule {
  name: string;
  due_date: string;
  invoice_portion: number;
  payment_amount: number;
}

export class PurchaseInvoiceAdvances {
  name: string;
  parenttype: string;
  reference_type: string;
  reference_name: string;
  reference_row: string;
  advance_amount: number;
  allocated_amount: number;
}

export class PurchaseInvoiceItem {
  name: string;
  item_code: string;
  item_name: string;
  description: string;
  item_group: string;
  image: string;
  warehouse: string;
  serial_no: string;
  has_serial_no: number;
  expense_account: string;
  cost_center: string;
  received_qty: number;
  qty: number;
  rejected_qty: number;
  rate: number;
  amount: number;
}

export const PurchaseInvoiceSchema =
  SchemaFactory.createForClass(PurchaseInvoice);
