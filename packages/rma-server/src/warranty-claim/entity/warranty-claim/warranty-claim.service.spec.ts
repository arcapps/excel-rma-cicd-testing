import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { WarrantyClaimService } from './warranty-claim.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('WarrantyClaimService', () => {
  let service: WarrantyClaimService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WarrantyClaimService,
        {
          provide: getModelToken('WarrantyClaim'),
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<WarrantyClaimService>(WarrantyClaimService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
