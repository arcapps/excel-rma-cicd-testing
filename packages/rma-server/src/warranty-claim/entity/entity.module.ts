import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
// import { WarrantyClaim } from './warranty-claim/warranty-claim.entity';
import { WarrantyClaimService } from './warranty-claim/warranty-claim.service';
import { MongooseModule } from '@nestjs/mongoose';
import { WarrantyClaimSchema } from '../schema/warranty-claim.schema';

@Module({
  // imports: [TypeOrmModule.forFeature([WarrantyClaim])],
  imports: [
    MongooseModule.forFeature([
      { name: 'WarrantyClaim', schema: WarrantyClaimSchema },
    ]),
  ],
  providers: [WarrantyClaimService],
  exports: [WarrantyClaimService],
})
export class WarrantyClaimEntitiesModule {}
