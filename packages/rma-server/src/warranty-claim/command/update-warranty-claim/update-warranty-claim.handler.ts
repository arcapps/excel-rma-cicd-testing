import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { firstValueFrom } from 'rxjs';
import { WarrantyClaimAggregateService } from '../../aggregates/warranty-claim-aggregate/warranty-claim-aggregate.service';
import { UpdateWarrantyClaimCommand } from './update-warranty-claim.command';

@CommandHandler(UpdateWarrantyClaimCommand)
export class UpdateWarrantyClaimCommandHandler
  implements ICommandHandler<UpdateWarrantyClaimCommand>
{
  constructor(
    private publisher: EventPublisher,
    private manager: WarrantyClaimAggregateService,
  ) {}

  async execute(command: UpdateWarrantyClaimCommand) {
    const { updatePayload, req } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    switch (updatePayload.set) {
      case 'Bulk':
        await firstValueFrom(this.manager.appendBulkClaim(updatePayload, req));
        break;

      default:
        await this.manager.update(updatePayload);
        break;
    }
    aggregate.commit();
  }
}
