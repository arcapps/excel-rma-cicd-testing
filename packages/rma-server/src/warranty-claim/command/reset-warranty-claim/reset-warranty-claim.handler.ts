import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { firstValueFrom } from 'rxjs';
import { WarrantyClaimAggregateService } from '../../aggregates/warranty-claim-aggregate/warranty-claim-aggregate.service';
import { ResetWarrantyClaimCommand } from './reset-warranty-claim.command';

@CommandHandler(ResetWarrantyClaimCommand)
export class ResetWarrantyClaimCommandHandler
  implements ICommandHandler<ResetWarrantyClaimCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: WarrantyClaimAggregateService,
  ) {}
  async execute(command: ResetWarrantyClaimCommand) {
    const { payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await firstValueFrom(this.manager.cancelWarrantyClaim(payload));
    aggregate.commit();
  }
}
