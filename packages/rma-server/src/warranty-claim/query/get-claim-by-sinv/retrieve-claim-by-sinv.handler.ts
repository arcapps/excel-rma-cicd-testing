import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveWarrantyClaimBySINVQuery } from './retrieve-claim-by-sinv.query';
import { WarrantyClaimAggregateService } from '../../aggregates/warranty-claim-aggregate/warranty-claim-aggregate.service';

@QueryHandler(RetrieveWarrantyClaimBySINVQuery)
export class RetrieveWarrantyClaimBySINVQueryHandler
  implements IQueryHandler<RetrieveWarrantyClaimBySINVQuery>
{
  constructor(private readonly manager: WarrantyClaimAggregateService) {}

  async execute(query: RetrieveWarrantyClaimBySINVQuery) {
    const { uuid } = query;
    return this.manager.retrieveWarrantyClaimBySINV(uuid);
  }
}
