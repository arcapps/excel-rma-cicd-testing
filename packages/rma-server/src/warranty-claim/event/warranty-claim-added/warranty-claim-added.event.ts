import { IEvent } from '@nestjs/cqrs';
import { WarrantyClaim } from '../../schema/warranty-claim.schema';

export class WarrantyClaimAddedEvent implements IEvent {
  constructor(
    public warrantyClaim: WarrantyClaim,
    public clientHttpRequest: any,
  ) {}
}
