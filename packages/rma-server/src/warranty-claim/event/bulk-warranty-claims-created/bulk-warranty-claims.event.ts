import { IEvent } from '@nestjs/cqrs';
import { WarrantyClaim } from '../../schema/warranty-claim.schema';

export class BulkWarrantyClaimsCreatedEvent implements IEvent {
  constructor(public warrantyClaimsPayload: WarrantyClaim[]) {}
}
