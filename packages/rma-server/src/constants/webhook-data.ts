export function getBearerTokenOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'OAuth Bearer Token',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function getBearerTokenAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
  webhookDocevent = 'after_insert',
) {
  return {
    webhook_doctype: 'OAuth Bearer Token',
    webhook_docevent: webhookDocevent,
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function getSupplierAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Supplier',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function getSupplierOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Supplier',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function getSupplierOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Supplier',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function getCustomerAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Customer',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getCustomerWebhookData(),
  };
}

export function getCustomerOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Customer',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getCustomerWebhookData(),
  };
}

export function getCustomerOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Customer',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getCustomerWebhookData(),
  };
}

export function getItemAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Item',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getItemInsertWebhookData(),
  };
}

export function getItemOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Item',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getItemUpdateWebhookData(),
  };
}

export function getItemOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Item',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getItemTrashWebhookData(),
  };
}

export function deliveryNoteNoAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Delivery Note',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function deliveryNoteOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Delivery Note',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function deliveryNoteOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Delivery Note',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function purchaseInvoiceOnSubmitWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Purchase Invoice',
    webhook_docevent: 'on_submit',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getPurchaseInvoiceWebhookData(),
  };
}

export function salesInvoiceOnSubmitWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Sales Invoice',
    webhook_docevent: 'on_submit',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getSalesInvoiceWebhookData(),
  };
}

export function salesInvoiceOnCancelWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Sales Invoice',
    webhook_docevent: 'on_cancel',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function purchaseOrderOnSubmitWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Purchase Order',
    webhook_docevent: 'on_submit',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getPurchaseOrderWebhookData(),
  };
}

export function purchaseInvoiceOnCancelWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Purchase Invoice',
    webhook_docevent: 'on_cancel',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function purchaseReceiptOnCancelWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Purchase Receipt',
    webhook_docevent: 'on_cancel',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getWebhookDataFields(),
  };
}

export function dataImportAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Data Import',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      { fieldname: 'name', key: 'name' },
      { fieldname: 'doctype', key: 'doctype' },
    ],
  };
}

export function itemBundleAfterUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Product Bundle',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: getItemBundleWebhookData(),
  };
}

export function getWebhookDataFields() {
  return [
    { fieldname: 'name', key: 'name' },
    { fieldname: 'doctype', key: 'doctype' },
  ];
}

export function getPurchaseInvoiceWebhookData() {
  return [
    { fieldname: 'name', key: 'name' },
    { fieldname: 'doctype', key: 'doctype' },
    { fieldname: 'docstatus', key: 'docstatus' },
    { fieldname: 'is_paid', key: 'is_paid' },
    { fieldname: 'is_return', key: 'is_return' },
    { fieldname: 'update_stock', key: 'update_stock' },
    { fieldname: 'total_qty', key: 'total_qty' },
    { fieldname: 'base_total', key: 'base_total' },
    { fieldname: 'total', key: 'total' },
    { fieldname: 'total_advance', key: 'total_advance' },
    { fieldname: 'outstanding_amount', key: 'outstanding_amount' },
    { fieldname: 'title', key: 'title' },
    { fieldname: 'paid_amount', key: 'paid_amount' },
    { fieldname: 'supplier', key: 'supplier' },
    { fieldname: 'naming_series', key: 'naming_series' },
    { fieldname: 'due_date', key: 'due_date' },
    { fieldname: 'company', key: 'company' },
    { fieldname: 'posting_date', key: 'posting_date' },
    { fieldname: 'posting_time', key: 'posting_time' },
    { fieldname: 'supplier_address', key: 'supplier_address' },
    { fieldname: 'address_display', key: 'address_display' },
    { fieldname: 'buying_price_list', key: 'buying_price_list' },
    { fieldname: 'in_words', key: 'in_words' },
    { fieldname: 'credit_to', key: 'credit_to' },
    { fieldname: 'against_expense_account', key: 'against_expense_account' },
    { fieldname: 'pricing_rules', key: 'pricing_rules' },
    { fieldname: 'supplied_items', key: 'supplied_items' },
    { fieldname: 'taxes', key: 'taxes' },
    // { fieldname: 'items', key: 'items' },
    { fieldname: 'advances', key: 'advances' },
    { fieldname: 'payment_schedule', key: 'payment_schedule' },
    { fieldname: 'owner', key: 'owner' },
    { fieldname: 'supplier_name', key: 'supplier_name' },
  ];
}

export function getSalesInvoiceWebhookData() {
  return [
    { fieldname: 'name', key: 'name' },
    { fieldname: 'doctype', key: 'doctype' },
    { fieldname: 'customer_name', key: 'customer_name' },
    { fieldname: 'due_date', key: 'due_date' },
    { fieldname: 'territory', key: 'territory' },
    { fieldname: 'total_qty', key: 'total_qty' },
    { fieldname: 'total', key: 'total' },
    { fieldname: 'update_stock', key: 'update_stock' },
    { fieldname: 'posting_time', key: 'posting_time' },
    { fieldname: 'customer', key: 'customer' },
    { fieldname: 'title', key: 'title' },
    { fieldname: 'company', key: 'company' },
    { fieldname: 'posting_date', key: 'posting_date' },
    { fieldname: 'set_posting_time', key: 'set_posting_time' },
    { fieldname: 'outstanding_balance', key: 'outstanding_amount' },
    { fieldname: 'net_total', key: 'net_total' },
    { fieldname: 'base_net_total', key: 'base_net_total' },
    { fieldname: 'base_total', key: 'base_total' },
    { fieldname: 'items', key: 'items' },
    { fieldname: 'is_return', key: 'is_return' },
  ];
}

export function getPurchaseOrderWebhookData() {
  return [
    { fieldname: 'name', key: 'name' },
    { fieldname: 'doctype', key: 'doctype' },
    { fieldname: 'transaction_date', key: 'transaction_date' },
    { fieldname: 'owner', key: 'owner' },
    // { fieldname: 'items', key: 'items' },
  ];
}

export function getCustomerWebhookData() {
  return [
    { fieldname: 'name', key: 'name' },
    { fieldname: 'doctype', key: 'doctype' },
    { fieldname: 'customer_name', key: 'customer_name' },
    { fieldname: 'territory', key: 'territory' },
    { fieldname: 'customer_type', key: 'customer_type' },
    { fieldname: 'credit_limits', key: 'credit_limits' },
    { fieldname: 'sales_team', key: 'sales_team' },
    { fieldname: 'owner', key: 'owner' },
    { fieldname: 'customer_group', key: 'customer_group' },
    { fieldname: 'payment_terms', key: 'payment_terms' },
    {
      fieldname: 'custom_other_brands_limit',
      key: 'custom_other_brands_limit',
    },
    {
      fieldname: 'custom_brand_wise_allocations',
      key: 'custom_brand_wise_allocations',
    },
    { fieldname: 'excel_remaining_balance', key: 'excel_remaining_balance' },
    { fieldname: 'custom_source', key: 'custom_source' },
  ];
}

export function getItemBundleWebhookData() {
  return [
    { fieldname: 'name', key: 'name' },
    { fieldname: 'doctype', key: 'doctype' },
    { fieldname: 'owner', key: 'owner' },
    { fieldname: 'items', key: 'items' },
    { fieldname: 'new_item_code', key: 'new_item_code' },
  ];
}

export function getItemInsertWebhookData() {
  return [
    { fieldname: 'name', key: 'name' },
    { fieldname: 'doctype', key: 'doctype' },
    { fieldname: 'owner', key: 'owner' },
    { fieldname: 'item_code', key: 'item_code' },
    { fieldname: 'item_name', key: 'item_name' },
    { fieldname: 'item_group', key: 'item_group' },
    { fieldname: 'has_excel_serials', key: 'has_excel_serials' },
    // { fieldname: 'brand', key: 'brand' },
    // { filedname: 'stock_uom', key: 'stock_uom' },
  ];
}

export function getItemUpdateWebhookData() {
  return [
    { fieldname: 'name', key: 'name' },
    { fieldname: 'doctype', key: 'doctype' },
    { fieldname: 'owner', key: 'owner' },
    { fieldname: 'item_code', key: 'item_code' },
    { fieldname: 'item_name', key: 'item_name' },
    { fieldname: 'item_group', key: 'item_group' },
    // { fieldname: 'brand', key: 'brand' },
    // { filedname: 'stock_uom', key: 'stock_uom' },
  ];
}

export function getItemTrashWebhookData() {
  return [
    { fieldname: 'name', key: 'name' },
    { fieldname: 'doctype', key: 'doctype' },
    { fieldname: 'owner', key: 'owner' },
    { fieldname: 'item_code', key: 'item_code' },
    { fieldname: 'item_name', key: 'item_name' },
    { fieldname: 'item_group', key: 'item_group' },
  ];
}
