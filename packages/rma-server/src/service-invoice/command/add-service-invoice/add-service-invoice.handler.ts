import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { firstValueFrom } from 'rxjs';
import { ServiceInvoiceAggregateService } from '../../aggregates/service-invoice-aggregate/service-invoice-aggregate.service';
import { AddServiceInvoiceCommand } from './add-service-invoice.command';

@CommandHandler(AddServiceInvoiceCommand)
export class AddServiceInvoiceCommandHandler
  implements ICommandHandler<AddServiceInvoiceCommand>
{
  constructor(
    private publisher: EventPublisher,
    private manager: ServiceInvoiceAggregateService,
  ) {}
  async execute(command: AddServiceInvoiceCommand) {
    const { serviceInvoicePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await firstValueFrom(
      aggregate.addServiceInvoice(serviceInvoicePayload, clientHttpRequest),
    );
    aggregate.commit();
  }
}
