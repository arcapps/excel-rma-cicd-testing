import { IEvent } from '@nestjs/cqrs';
import { ServiceInvoice } from '../../schema/service-invoice.schema';

export class ServiceInvoiceUpdatedEvent implements IEvent {
  constructor(public updatePayload: ServiceInvoice) {}
}
