import { IEvent } from '@nestjs/cqrs';
import { ServiceInvoice } from '../../schema/service-invoice.schema';

export class ServiceInvoiceRemovedEvent implements IEvent {
  constructor(public serviceInvoice: ServiceInvoice) {}
}
