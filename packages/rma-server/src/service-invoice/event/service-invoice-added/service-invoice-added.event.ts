import { IEvent } from '@nestjs/cqrs';
import { ServiceInvoice } from '../../schema/service-invoice.schema';

export class ServiceInvoiceAddedEvent implements IEvent {
  constructor(
    public serviceInvoice: ServiceInvoice,
    public clientHttpRequest: any,
  ) {}
}
