import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { Item } from '../../sales-invoice/schema/sales-invoice.schema';

export type ServiceInvoiceDocument = HydratedDocument<ServiceInvoice>;

@Schema({ collection: 'service_invoice' })
export class ServiceInvoice {
  @Prop()
  uuid: string;

  @Prop()
  warrantyClaimUuid: string;

  @Prop()
  name: string;

  @Prop()
  invoice_no: string;

  @Prop()
  status: string;

  @Prop()
  date: Date;

  @Prop()
  customer_third_party: string;

  @Prop()
  invoice_amount: number;

  @Prop()
  outstanding_amount: number;

  @Prop()
  claim_no: string;

  @Prop()
  remarks: string;

  @Prop()
  branch: string;

  @Prop()
  created_by: string;

  @Prop()
  submitted_by: string;

  @Prop()
  posting_date: Date;

  @Prop()
  customer_name: string;

  @Prop()
  customer_address: string;

  @Prop()
  customer_contact: string;

  @Prop()
  third_party_name: string;

  @Prop()
  third_party_address: string;

  @Prop()
  third_party_contact: string;

  @Prop()
  total: number;

  @Prop()
  items: Item[];

  @Prop()
  customer: string;

  @Prop()
  total_qty: number;

  @Prop()
  contact_email: string;

  @Prop()
  due_date: Date;

  @Prop()
  delivery_warehouse: string;

  @Prop()
  docstatus: number;
}

export const ServiceInvoiceSchema =
  SchemaFactory.createForClass(ServiceInvoice);
