// import { InjectRepository } from '@nestjs/typeorm';
// import { ServiceInvoice } from './service-invoice.entity';
import { Injectable } from '@nestjs/common';
// import { MongoRepository } from 'typeorm';
// import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PARSE_REGEX } from '../../../constants/app-strings';
import { InjectModel } from '@nestjs/mongoose';
import {
  ServiceInvoice,
  ServiceInvoiceDocument,
} from '../../schema/service-invoice.schema';
import { Model } from 'mongoose';

@Injectable()
export class ServiceInvoiceService {
  constructor(
    // @InjectRepository(ServiceInvoice)
    // private readonly serviceInvoiceRepository: MongoRepository<ServiceInvoice>,
    @InjectModel('ServiceInvoice')
    private serviceInvoiceModel: Model<ServiceInvoiceDocument>,
  ) {}

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (query[key]) {
        if (['fromDate', 'toDate'].includes(key)) {
          delete query[key];
        }
        if (key === 'status' && query[key] === 'All') {
          delete query[key];
        } else {
          if (typeof query[key] === 'string') {
            query[key] = { $regex: PARSE_REGEX(query[key]), $options: 'i' };
          } else {
            delete query[key];
          }
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }

  async find(query?) {
    return await this.serviceInvoiceModel.find(query);
  }

  async create(serviceInvoice: ServiceInvoice) {
    const serviceInvoiceObject = new ServiceInvoice();
    Object.assign(serviceInvoiceObject, serviceInvoice);
    return await new this.serviceInvoiceModel(serviceInvoice).save();
  }

  async findOne(options) {
    return await this.serviceInvoiceModel.findOne(options.where);
  }

  async list(skip, take, search, sort) {
    let dateQuery = {};
    let service_vouchers = {};
    let sortQuery = {};
    try {
      sortQuery = JSON.parse(sort);
    } catch {
      sortQuery = { posting_date: 'desc' };
    }

    for (const key of Object.keys(sortQuery)) {
      sortQuery[key] = sortQuery[key].toUpperCase();
      if (!sortQuery[key]) {
        delete sortQuery[key];
      }
    }
    try {
      search = JSON.parse(search);
    } catch {
      search = {};
    }

    if (search?.fromDate && search?.toDate) {
      dateQuery = {
        posting_date: {
          $gte: search.fromDate,
          $lte: search.toDate,
        },
      };
    }

    if (search.service_vouchers) {
      service_vouchers = search.service_vouchers;
      service_vouchers = {
        ...search.service_vouchers,
        docstatus: search.service_vouchers.docstatus,
      };
    }

    const $and: any[] = [
      search ? this.getFilterQuery(search) : {},
      dateQuery,
      service_vouchers,
    ];

    const where: { $and: any } = { $and };

    const results = await this.serviceInvoiceModel
      .find()
      .where(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);

    return {
      docs: results || [],
      length: await this.serviceInvoiceModel.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.serviceInvoiceModel.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.serviceInvoiceModel.updateOne(query, options);
  }

  asyncAggregate(query): Observable<any> {
    const promise = this.serviceInvoiceModel.aggregate(query);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }
}
