import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { ServiceInvoiceService } from './service-invoice.service';

describe('ServiceInvoiceService', () => {
  let service: ServiceInvoiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ServiceInvoiceService,
        {
          provide: getModelToken('ServiceInvoice'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ServiceInvoiceService>(ServiceInvoiceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
