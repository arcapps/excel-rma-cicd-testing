import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { firstValueFrom, forkJoin, from, of, throwError } from 'rxjs';
import { catchError, finalize, map, retry, switchMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import {
  ACCEPT,
  APPLICATION_JSON_CONTENT_TYPE,
  APP_JSON,
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  CONTENT_TYPE,
  INVALID_FILE,
  NON_SERIAL_ITEM,
} from '../../../constants/app-strings';
import { SERIAL_NO_NOT_FOUND } from '../../../constants/messages';
import {
  CUSTOMER_PROJECT_QUERY,
  ITEM_PROJECT_QUERY,
  SUPPLIER_PROJECT_QUERY,
} from '../../../constants/query';
import {
  FRAPPE_API_GET_STOCK_ENDPOINT,
  FRAPPE_API_SERIAL_NO_ENDPOINT,
  INVOICE_LIST,
  POST_DELIVERY_NOTE_ENDPOINT,
} from '../../../constants/routes';
import {
  lockDocumentTransaction,
  unlockDocumentTransaction,
} from '../../../constants/transaction-helper';
import { DeliveryNoteAggregateService } from '../../../delivery-note/aggregates/delivery-note-aggregate/delivery-note-aggregate.service';
import { ErrorLogService } from '../../../error-log/error-log-service/error-log.service';
import { SalesInvoiceService } from '../../../sales-invoice/entity/sales-invoice/sales-invoice.service';
import { SerialNoHistoryService } from '../../../serial-no/entity/serial-no-history/serial-no-history.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { AssignSerialDto } from '../../entity/serial-no/assign-serial-dto';
import {
  SerialNoDto,
  ValidateReturnSerialsDto,
  ValidateSerialsDto,
} from '../../entity/serial-no/serial-no-dto';
import { SerialNo } from '../../entity/serial-no/serial-no.entity';
import { SerialNoService } from '../../entity/serial-no/serial-no.service';
import { AssignSerialNoPoliciesService } from '../../policies/assign-serial-no-policies/assign-serial-no-policies.service';
import { SerialNoPoliciesService } from '../../policies/serial-no-policies/serial-no-policies.service';
import { DeliveryNoteService } from '../../../delivery-note/entity/delivery-note-service/delivery-note.service';
import { getParsedPostingDate } from '../../../constants/agenda-job';
import { ItemService } from '../../../item/entity/item/item.service';
import { ServerSettings } from '../../../system-settings/schemas/server-settings.schema';
import { WarrantyClaimService } from '../../../warranty-claim/entity/warranty-claim/warranty-claim.service';

@Injectable()
export class SerialNoAggregateService extends AggregateRoot {
  constructor(
    private readonly serialNoService: SerialNoService,
    private readonly http: HttpService,
    private readonly settingsService: SettingsService,
    private readonly serialNoPolicyService: SerialNoPoliciesService,
    private readonly serialNoHistoryService: SerialNoHistoryService,
    private readonly assignSerialNoPolicyService: AssignSerialNoPoliciesService,
    private readonly deliveryNoteAggregateService: DeliveryNoteAggregateService,
    private readonly deliveryNoteService: DeliveryNoteService,
    private readonly errorLogService: ErrorLogService,
    private readonly salesInvoiceService: SalesInvoiceService,
    private readonly clientToken: ClientTokenManagerService,
    private readonly itemService: ItemService,
    private readonly warrantyClaimService: WarrantyClaimService,
  ) {
    super();
  }

  validateNewSerialNo(serialNoPayload: SerialNoDto, clientHttpRequest) {
    return this.serialNoPolicyService.validateSerial(serialNoPayload).pipe(
      switchMap(() => {
        return this.serialNoPolicyService.validateItem(serialNoPayload).pipe(
          switchMap(() => {
            const serialNo = new SerialNo();
            Object.assign(serialNo, serialNoPayload);
            serialNo.uuid = uuidv4();
            serialNo.isSynced = false;
            this.syncNewSerialNo(serialNo, clientHttpRequest);
            return of(serialNo);
          }),
        );
      }),
    );
  }

  async retrieveSerialNoHistory(serial_no: string) {
    return await this.serialNoHistoryService.find({ where: { serial_no } });
  }

  async retrieveSerialNo(serial_no: string) {
    return this.serialNoService
      .asyncAggregate([
        { $match: { serial_no } },
        {
          $lookup: {
            from: 'item',
            localField: 'item_code',
            foreignField: 'item_code',
            as: 'item',
          },
        },
        {
          $lookup: {
            from: 'supplier',
            localField: 'supplier',
            foreignField: 'name',
            as: 'supplier',
          },
        },
        {
          $lookup: {
            from: 'customer',
            localField: 'customer',
            foreignField: 'name',
            as: 'customer',
          },
        },
        {
          $unwind: this.unwindQuery('$item'),
        },
        {
          $unwind: this.unwindQuery('$supplier'),
        },
        {
          $unwind: this.unwindQuery('$customer'),
        },
        {
          $project: {
            uuid: 1,
            name: 1,
            isSynced: 1,
            serial_no: 1,
            item_code: 1,
            supplier: SUPPLIER_PROJECT_QUERY,
            customer: CUSTOMER_PROJECT_QUERY,
            item: ITEM_PROJECT_QUERY,
          },
        },
      ])
      .pipe(
        switchMap((serial: any[]) => {
          if (!serial || !serial.length) {
            return throwError(() => new NotFoundException());
          }
          return of(serial);
        }),
      );
  }

  async getSerialNoList(offset, limit, sort, filterQuery) {
    return this.serialNoService.list(offset, limit, sort, filterQuery);
  }

  syncNewSerialNo(serialNo: SerialNo, clientHttpRequest) {
    return this.settingsService
      .find()
      .pipe(
        switchMap(settings => {
          if (!settings.authServerURL) {
            return throwError(() => new NotImplementedException());
          }
          const body = {
            serial_no: serialNo.serial_no,
            item_code: serialNo.item_code,
            warranty_expiry_date: serialNo.warranty_expiry_date,
            company: serialNo.company,
          };

          return this.http.post(
            settings.authServerURL + FRAPPE_API_SERIAL_NO_ENDPOINT,
            body,
            {
              headers: {
                [AUTHORIZATION]:
                  BEARER_HEADER_VALUE_PREFIX +
                  clientHttpRequest.token.accessToken,
                [CONTENT_TYPE]: APPLICATION_JSON_CONTENT_TYPE,
                [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
              },
            },
          );
        }),
        retry(3),
      )
      .subscribe({
        next: () => {
          return this.serialNoService
            .updateOne(
              { uuid: serialNo.uuid },
              {
                $set: {
                  isSynced: true,
                },
              },
            )
            .then(() => {})
            .catch(() => {});
        },
        error: err => {
          this.errorLogService.createErrorLog(
            err,
            'Serial No',
            'serialNo',
            clientHttpRequest,
          );
        },
      });
  }

  unwindQuery(key) {
    return {
      path: key,
      preserveNullAndEmptyArrays: true,
    };
  }

  validateErpNextStock(payload, req) {
    return this.settingsService.find().pipe(
      switchMap((settings: ServerSettings) => {
        const itemObservables = [];
        for (const item of payload.items) {
          const params = {
            fields: '["*"]',
            filters: `[["item_code","=", "${item.item_code}"],["warehouse","=","${payload.set_warehouse}"]]`,
          };

          const itemObservable = this.http
            .get(settings.authServerURL + FRAPPE_API_GET_STOCK_ENDPOINT, {
              params,
              headers: {
                [AUTHORIZATION]:
                  BEARER_HEADER_VALUE_PREFIX + req.token.accessToken,
                [CONTENT_TYPE]: APP_JSON,
                [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
              },
            })
            .pipe(
              switchMap(res => {
                const frappeRes = res?.data?.data;
                if (frappeRes.length && frappeRes[0]?.actual_qty < item.qty) {
                  return throwError(
                    () =>
                      new BadRequestException(
                        // eslint-disable-max-len @typescript-eslint/ban-ts-comment
                        `Only ${frappeRes[0]?.actual_qty} items are available in ${frappeRes[0]?.warehouse}`,
                      ),
                  );
                }
                return of({});
              }),
              catchError(error => {
                return throwError(() => new BadRequestException(error));
              }),
            );

          itemObservables.push(itemObservable);
        }

        return forkJoin(itemObservables); // Combine multiple observables into one
      }),
      catchError(err => {
        return throwError(() => new BadRequestException(err));
      }),
    );
  }

  // Can be improved
  assignSerial(assignPayload: AssignSerialDto, clientHttpRequest) {
    let salesInvoice;
    return lockDocumentTransaction(this.salesInvoiceService, {
      name: assignPayload.sales_invoice_name,
    }).pipe(
      switchMap(obj => {
        return of(obj).pipe(
          switchMap(() => {
            return this.validateErpNextStock(assignPayload, clientHttpRequest);
          }),
          switchMap(() => {
            return this.assignSerialNoPolicyService.validateSerial(
              assignPayload,
            );
          }),
          switchMap(() => {
            return this.assignSerialNoPolicyService.validateStock(
              assignPayload,
            );
          }),
          switchMap(() => {
            return this.assignSerialNoPolicyService.validateDeliveryNote(
              assignPayload,
            );
          }),

          switchMap(() => {
            return this.settingsService.find().pipe(
              switchMap(settings => {
                const salesInvoicePromise = this.salesInvoiceService.findOne({
                  where: {
                    name: assignPayload.sales_invoice_name,
                  },
                });
                const salesInvoiceObservable = from(salesInvoicePromise); // Convert promise to observable

                return salesInvoiceObservable.pipe(
                  switchMap(salesInvoiceRes => {
                    salesInvoice = salesInvoiceRes;
                    salesInvoiceRes.name = assignPayload.sales_invoice_name;
                    const payload = this.createDeliveryNotePayload(
                      salesInvoiceRes,
                      assignPayload,
                    );

                    return this.deliveryNoteService.create(payload).pipe(
                      switchMap(deliveryNote => {
                        return this.syncDeliveryNote(
                          settings,
                          deliveryNote,
                          salesInvoiceRes,
                          clientHttpRequest,
                          false,
                          assignPayload,
                        ).pipe(
                          catchError(err => {
                            if (err?.response && err?.response?.message) {
                              const errorMessageMatch =
                                err.response.message.match(
                                  /NegativeStockError: (.+?)\\n/,
                                );

                              if (
                                errorMessageMatch &&
                                errorMessageMatch.length >= 2
                              ) {
                                const errorMessage = errorMessageMatch[1];
                                return throwError(
                                  () => new BadRequestException(errorMessage),
                                );
                              }
                            }
                            return from(
                              this.deliveryNoteService.deleteOne({
                                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                                //  @ts-ignore
                                _id: deliveryNote._id,
                              }),
                            ).pipe(
                              switchMap(res => {
                                return throwError(
                                  () => new BadRequestException(err),
                                );
                              }),
                            );
                          }),
                        );
                      }),
                    );
                  }),
                );
              }),
            );
          }),
        );
      }),
      switchMap(() => {
        return forkJoin({
          erp_invoice: this.retrieveSalesDoc(assignPayload.sales_invoice_name),
          mongo_invoice: this.salesInvoiceService.findOne({
            where: { name: assignPayload.sales_invoice_name },
          }),
        }).pipe(
          switchMap(({ erp_invoice, mongo_invoice }) => {
            // console.log('ERP INV',erp_invoice)
            // if (Object.keys(mongo_invoice.bundle_items_map).length !== 0) {
            //   if (erp_invoice.bundle_items.length) {
            //     // IF BUNDLE ITEMS EXISTS
            //     if (
            //       !erp_invoice.bundle_items.includes(
            //         assignPayload.items[0].item_code,
            //       )
            //     ) {
            //       erp_invoice.bundle_items.forEach(item => {
            //         erp_invoice.bundle_items.push({
            //           item_code: item.item_code,
            //           qty: item.qty,
            //           item_name: item.item_name,
            //           serial_no: '',
            //         });
            //       });
            //     } else {
            //       erp_invoice.bundle_items.forEach(bundle_item => {
            //         assignPayload.items.forEach(item => {
            //           if (item.item_code === bundle_item.item_code) {
            //             bundle_item.serial_no =
            //               bundle_item.serial_no
            //               // +
            //               // ', ' +
            //               // item.serial_no?.join(', ');
            //           }
            //         });
            //       });
            //     }
            //   } else {
            //     // IF BUNDLE ITEMS DO NOT EXIST
            //     assignPayload.items.forEach(payload_item => {
            //       if (!erp_invoice.items.includes(payload_item.item_code)) {
            //         erp_invoice.bundle_items.push({
            //           item_code: payload_item.item_code,
            //           qty: payload_item.qty,
            //           item_name: payload_item.item_name,
            //           serial_no: '',
            //         });
            //       } else {
            //         erp_invoice.items.forEach(erp_item => {
            //           if (payload_item.item_code === erp_item.item_code) {
            //             if (
            //               payload_item.has_serial_no === 0 &&
            //               !erp_item.excel_serials
            //             ) {
            //               erp_item.excel_serials =''
            //                 // payload_item.serial_no?.join('');
            //             } else {
            //               erp_item.excel_serials =''

            //             }
            //           }
            //         });
            //       }
            //     });
            //   }
            // } else {
            //   assignPayload.items.forEach(payload_item => {
            //     erp_invoice.items.forEach(erp_item => {
            //       if (payload_item.item_code === erp_item.item_code) {
            //         if (payload_item.has_serial_no === 0) {
            //           erp_item.excel_serials = NON_SERIAL_ITEM;
            //         } else {
            //           erp_item.excel_serials = ''
            //         }
            //       }
            //     });
            //   });
            // }
            return this.updateErpInvoice(
              assignPayload.sales_invoice_name,
              erp_invoice,
              assignPayload,
              salesInvoice,
            );
          }),
        );
      }),
      // note: Finalize is a bit tricky it will get triggered on success+failure for
      // the same level pipe as well as its parent.
      // eg. here it will trigger on both validateSerial, createDeliveryNote fail+success and also for of(obj);
      finalize(() =>
        unlockDocumentTransaction(this.salesInvoiceService, {
          name: assignPayload.sales_invoice_name,
        }),
      ),
    );
  }

  syncDeliveryNote(
    settings,
    deliveryNote,
    salesInvoice,
    clientHttpReq,
    isReturn,
    assignPayload,
  ) {
    if (!settings || !settings.authServerURL) {
      this.deliveryNoteService
        .updateOne(
          { _id: deliveryNote._id },
          {
            $set: {
              inQueue: false,
              'timeStamp.created_on': getParsedPostingDate(deliveryNote),
            },
          },
        )
        .then(() => {})
        .catch(err => {
          () => new BadRequestException(err);
        });
      return throwError(() => new NotImplementedException());
    }
    const bodyForWebhook = this.mapDeliveryNote(
      deliveryNote,
      salesInvoice,
      isReturn,
      assignPayload,
    );
    return this.http
      .post(
        settings.authServerURL + POST_DELIVERY_NOTE_ENDPOINT,
        bodyForWebhook,
        {
          headers: {
            [AUTHORIZATION]:
              BEARER_HEADER_VALUE_PREFIX + clientHttpReq.token.accessToken,
            [CONTENT_TYPE]: APP_JSON,
            [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
          },
        },
      )
      .pipe(
        map(data => {
          return data.data.data;
        }),
        switchMap(successResponse => {
          return this.deliveryNoteAggregateService
            .createDeliveryNote(
              assignPayload,
              clientHttpReq,
              successResponse.name,
            )
            .pipe(
              switchMap(() => {
                return from(
                  this.deliveryNoteService.updateOne(
                    { _id: deliveryNote._id },
                    {
                      $set: {
                        isSynced: true,
                        submitted: true,
                        inQueue: false,
                        name: successResponse.name,
                      },
                    },
                  ),
                );
              }),
            );
        }),
        catchError(err => {
          this.errorLogService.createErrorLog(
            err,
            'Delivery Note',
            'deliverynote',
            clientHttpReq,
          );
          return throwError(
            new BadRequestException(err.response ? err.response.data.exc : err),
          );
        }),
      );
  }

  mapDeliveryNote(deliveryNote, salesInvoice, isReturn, assignPayload) {
    const payload = { ...assignPayload };
    const itemsTemp = payload.items?.map(item => {
      if (item.has_serial_no) {
        return {
          ...item,
          against_sales_invoice: salesInvoice.name,
          excel_serials: item?.serial_no?.join(', '),
          serial_no: undefined,
        };
      } else {
        return {
          ...item,
          serial_no: undefined,
          excel_serials: 'Non Serial Item',
          billed_amt: item.amount,
        };
      }
    });
    return {
      // source: salesInvoice.name,
      docstatus: 1,
      customer: deliveryNote.customer,
      customer_name: deliveryNote.customer_name,
      set_warehouse: assignPayload.set_warehouse,
      company: deliveryNote.company,
      posting_date: assignPayload.posting_date,
      posting_time: assignPayload.posting_time,
      is_return: isReturn ? 1 : 0,
      total_qty: deliveryNote.total_qty,
      total: deliveryNote.total,
      territory: salesInvoice.territory,
      items: itemsTemp,
      pricing_rules: deliveryNote.pricing_rules,
      taxes: deliveryNote.taxes,
      packed_items: deliveryNote.packed_items,
      sales_team: deliveryNote.sales_team,
      set_posting_time: assignPayload.posting_date ? 1 : 0,
      bundle_items: [],
      status: 'Completed',
    };
  }

  createDeliveryNotePayload(salesInvoice, payload) {
    return {
      sales_invoice_name: salesInvoice.name,
      set_warehouse: salesInvoice.delivery_warehouse,
      total_qty: payload.total_qty,
      total: payload.total,
      posting_date: salesInvoice.posting_date,
      posting_time: salesInvoice.posting_time,
      customer: salesInvoice.customer,
      customer_name: salesInvoice.customer_name,
      company: salesInvoice.company,
      items: payload.items,
    };
  }

  fetchErpInvoice(name, settings, headers) {
    const url = settings.authServerURL + INVOICE_LIST + '/' + name;
    return this.http.get(url, { headers }).pipe(
      catchError(err => {
        return throwError(new BadRequestException(err));
      }),
    );
  }

  updateErpInvoice(
    name: string,
    erp_invoice: any,
    assignPayload: any,
    salesInvoiceRes: any,
  ) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settingsService.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        const bundleItemChild = [];
        const bundleItems = salesInvoiceRes.items.filter(
          bundleItem => bundleItem.has_bundle_item === true,
        );
        const itemsTemp = bundleItems.map(item => item.item_code);

        return from(
          this.itemService.find({
            item_code: { $in: itemsTemp },
          }),
        ).pipe(
          switchMap(res => {
            res.forEach((queryItem, index) => {
              const response = queryItem.bundle_items
                .map(bundleItem =>
                  assignPayload.items.find(
                    x => bundleItem?.item_code === x?.item_code,
                  ),
                )
                .filter(entry => entry !== undefined);

              if (response.length) {
                response.forEach(resp => {
                  const alreadyExist = bundleItemChild.find(
                    x => x.item_code === resp.item_code,
                  );
                  if (!alreadyExist) {
                    const childItem = assignPayload.items.find(
                      x => x.item_code === resp.item_code,
                    );
                    bundleItemChild.push(childItem);
                  }
                });
                // queryItem.bundle_items.forEach(bundleItem => {
                //   const childItem = assignPayload.items.find(
                //     x =>
                //       bundleItem.item_code === x.item_code &&
                //       x.qty >= bundleItem.qty,
                //   );
                //   if (childItem) {
                //     const checkNotAlreadyExist = bundleItemChild.some(
                //       x => x.item_code === childItem.item_code,
                //     );
                //     if (!checkNotAlreadyExist) {
                //       bundleItemChild.push(childItem);
                //     }
                //   }
                // });
              }
            });

            // erp_invoice.bundle_items = bundleItemChild
            let updatedBundleItems = [];
            if (erp_invoice?.bundle_items?.length) {
              updatedBundleItems = bundleItemChild.map(item => {
                const oldItems = erp_invoice.bundle_items.find(
                  oldItem => oldItem.item_name === item.item_name,
                );
                const newItemSerial = Array.isArray(item.serial_no)
                  ? item.serial_no.join(', ')
                  : item.serial_no;
                const oldItemsSerial = oldItems?.serial_no
                  ? ', ' + oldItems.serial_no
                  : '';
                return {
                  ...item,
                  qty: item.qty + (oldItems?.qty || 0),
                  serial_no: newItemSerial + oldItemsSerial,
                  // serial_no: Array.isArray(item.serial_no)
                  //   ? item?.serial_no.join(', ') + ', ' + oldItems?.serial_no
                  //   : item?.serial_no + ', ' + oldItems?.serial_no,
                };
              });
              const previouslyAssignedItems = erp_invoice.bundle_items.filter(
                x =>
                  !updatedBundleItems.some(
                    updated => updated.item_code === x.item_code,
                  ),
              );

              updatedBundleItems = [
                ...previouslyAssignedItems,
                ...updatedBundleItems,
              ];
            } else {
              updatedBundleItems = bundleItemChild.map(item => {
                return {
                  ...item,
                  serial_no: item?.serial_no ? item.serial_no.join(', ') : '',
                };
              });
            }
            if (updatedBundleItems.length) {
              erp_invoice.bundle_items = updatedBundleItems;
            }
            const updatedItems = [];
            erp_invoice.items.forEach(prevItem => {
              const updatedItem = assignPayload.items.find(
                oldItem => oldItem.item_code === prevItem.item_code,
              );
              if (updatedItem && updatedItem?.has_serial_no === 0) {
                updatedItems.push({
                  ...prevItem,
                  serial_no: '',
                  excel_serials: NON_SERIAL_ITEM,
                });
              } else if (updatedItem && updatedItem?.has_serial_no === 1) {
                updatedItems.push({
                  ...prevItem,
                  serial_no: '',
                  excel_serials: prevItem.excel_serials
                    ? prevItem.excel_serials +
                      ', ' +
                      updatedItem.serial_no.join(', ')
                    : updatedItem.serial_no.join(', '),
                });
              } else {
                updatedItems.push(prevItem);
              }
            });

            erp_invoice.items = updatedItems;
            const url = settings.authServerURL + INVOICE_LIST + '/' + name;

            return this.http.put(url, erp_invoice, { headers }).pipe(
              catchError(err => {
                return throwError(new BadRequestException(err));
              }),
            );
          }),
        );
      }),
    );
  }

  validateReturnSerials(payload: ValidateReturnSerialsDto) {
    return this.serialNoPolicyService.validateReturnSerials(payload);
  }

  validateBulkReturnSerialFile(file) {
    return from(this.getJsonData(file)).pipe(
      switchMap((data: ValidateReturnSerialsDto) => {
        if (!data) {
          return throwError(() => new BadRequestException(INVALID_FILE));
        }
        return this.validateReturnSerials(data);
      }),
    );
  }

  validateSerials(payload: ValidateSerialsDto) {
    return this.serialNoPolicyService.validateSerials(payload);
  }

  validateBulkSerialFile(file) {
    return from(this.getJsonData(file)).pipe(
      switchMap((data: ValidateSerialsDto) => {
        if (!data) {
          return throwError(() => new BadRequestException(INVALID_FILE));
        }
        return this.validateSerials(data);
      }),
    );
  }

  getJsonData(file: any) {
    return of(JSON.parse(file.buffer));
  }

  async getPurchaseInvoiceDeliveredSerials(
    purchase_invoice_name,
    search,
    skip = 0,
    take = 10,
    clientHttpRequest,
  ) {
    return await this.serialNoService.listPurchasedSerial(
      purchase_invoice_name,
      skip,
      take,
      search,
    );
  }

  getSalesInvoiceDeliveryNoteSerials(
    find,
    search,
    offset,
    limit,
    clientHttpRequest,
  ) {
    return this.serialNoService.listDeliveredSerial(
      find,
      search,
      offset,
      limit,
    );
  }

  async getSalesInvoiceReturnSerials(
    sales_invoice_name: string,
    offset: number,
    limit: number,
  ) {
    try {
      const salesInvoice = await this.salesInvoiceService.findOne({
        where: { name: sales_invoice_name },
      });

      const serialNumbers = [];
      const salesReturnName = [];
      salesInvoice.returned_items.forEach(item => {
        serialNumbers.push(...item.serial_no.split('\n'));
        salesReturnName.push(...item.sales_return_name.split('\n'));
      });

      return await this.serialNoService.listReturnInvoicesSerials(
        serialNumbers,
        salesReturnName,
        offset,
        limit,
      );
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }
  async getSerialDocument(serial: string) {
    try {
      const serial_document = await this.serialNoService.findOne({
        where: { serial_no: serial },
      });
      return serial_document;
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }

  async retrieveItemsBySerialOrItemCode(
    serialOrItemCode: string[],
  ): Promise<any[]> {
    const items = [];

    for (const item of serialOrItemCode) {
      // Assuming this.serialNoService.asyncAggregate returns a Promise
      const result = await firstValueFrom(
        this.serialNoService.asyncAggregate([
          { $match: { serial_no: item } },
          {
            $lookup: {
              from: 'item',
              localField: 'item_code',
              foreignField: 'item_code',
              as: 'item',
            },
          },
          {
            $unwind: this.unwindQuery('$item'),
          },
          // Other aggregation stages (e.g., $lookup, $unwind) can be added here
        ]),
      );

      items.push(result);
    }

    return items.flat();
  }

  retrieveDirectSerialNo(serial_no: string) {
    return from(this.serialNoService.findOne({ where: { serial_no } })).pipe(
      switchMap(serial => {
        return from(
          this.warrantyClaimService.findOne({
            where: { serial_no },
          }),
        ).pipe(
          switchMap(serialHistory => {
            if (!serial) {
              return throwError(
                () => new NotFoundException(SERIAL_NO_NOT_FOUND),
              );
            }

            if (serialHistory) {
              const history = serialHistory.status_history;
              if (
                history &&
                history[history.length - 1].delivery_status === 'Repaired'
              ) {
                serial.claim_no = undefined;
              }
            }
            return of(serial);
          }),
        );

        // return from(this.salesInvoiceService.findOne({
        //   where: {
        //     name: serial.sales_invoice_name
        //   }
        // })).pipe(
        //   switchMap(res => {
        //     if(res && serial){
        //       serial.customer = res.customer_name;
        //     }

        // })
        // )
      }),
    );
  }

  retrieveSalesDoc(name: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settingsService.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        const url = settings.authServerURL + INVOICE_LIST + '/' + name;
        return this.http
          .get(url, {
            headers,
          })
          .pipe(map(res => res.data.data));
      }),
    );
  }

  async listSerialQuantity(
    offset: number,
    limit: number,
    sort: any,
    query: any,
  ) {
    return await this.serialNoService.listSerialQuantity(
      offset || 0,
      limit || 10,
      sort,
      query,
    );
  }
}
