import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveSerialNoDocumentQuery } from './retrieve-serial-no-document.query';
import { SerialNoAggregateService } from '../../aggregates/serial-no-aggregate/serial-no-aggregate.service';

@QueryHandler(RetrieveSerialNoDocumentQuery)
export class RetrieveSerialNoDocumentHandler
  implements IQueryHandler<RetrieveSerialNoDocumentQuery>
{
  constructor(private readonly manager: SerialNoAggregateService) {}

  async execute(query: RetrieveSerialNoDocumentQuery) {
    const { serial_no } = query;
    return await this.manager.getSerialDocument(serial_no);
  }
}
