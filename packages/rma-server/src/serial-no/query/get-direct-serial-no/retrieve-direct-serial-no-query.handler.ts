import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { firstValueFrom } from 'rxjs';
import { SerialNoAggregateService } from '../../aggregates/serial-no-aggregate/serial-no-aggregate.service';
import { RetrieveDirectSerialNoQuery } from './retrieve-direct-serial-no.query';

@QueryHandler(RetrieveDirectSerialNoQuery)
export class RetrieveDirectSerialNoHandler
  implements IQueryHandler<RetrieveDirectSerialNoQuery>
{
  constructor(private readonly manager: SerialNoAggregateService) {}

  async execute(query: RetrieveDirectSerialNoQuery) {
    const { serial_no } = query;
    return firstValueFrom(this.manager.retrieveDirectSerialNo(serial_no));
  }
}
