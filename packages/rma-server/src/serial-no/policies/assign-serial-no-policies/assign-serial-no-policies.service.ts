import {
  Injectable,
  BadRequestException,
  NotImplementedException,
} from '@nestjs/common';
import { SerialNoService } from '../../entity/serial-no/serial-no.service';
import { from, of, throwError } from 'rxjs';
import { switchMap, mergeMap, toArray, map, concatMap } from 'rxjs/operators';
import {
  PLEASE_SETUP_DEFAULT_COMPANY,
  INVALID_COMPANY,
  SALES_INVOICE_NOT_FOUND,
  SERIAL_SHOULD_BE_EQUAL_TO_QUANTITY,
  INVALID_ITEM,
  INVALID_ITEM_CODE,
} from '../../../constants/messages';
// import { DateTime } from 'luxon';
import { AssignSerialDto } from '../../entity/serial-no/assign-serial-dto';
import { SalesInvoiceService } from '../../../sales-invoice/entity/sales-invoice/sales-invoice.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ItemService } from '../../../item/entity/item/item.service';
import { ItemDto } from '../../../sales-invoice/entity/sales-invoice/sales-invoice-dto';
// import { getParsedPostingDate } from '../../../constants/agenda-job';
import { DateTime } from 'luxon';
import { StockLedgerService } from '../../../stock-ledger/entity/stock-ledger/stock-ledger.service';
import { getParsedPostingDate } from '../../../constants/agenda-job';
import { ValidateSerialsResponse } from '../serial-no-policies/serial-no-policies.service';
import { ValidateSerialsDto } from '../../entity/serial-no/serial-no-dto';

@Injectable()
export class AssignSerialNoPoliciesService {
  constructor(
    private readonly serialNoService: SerialNoService,
    private readonly settingsService: SettingsService,
    private readonly salesInvoiceService: SalesInvoiceService,
    private readonly itemService: ItemService,
    private readonly stockLedgerService: StockLedgerService,
  ) {}

  validateSerial(serialProvider: AssignSerialDto) {
    return from(
      this.salesInvoiceService.findOne({
        where: { name: serialProvider.sales_invoice_name },
      }),
    ).pipe(
      switchMap(salesInvoice => {
        if (!salesInvoice) {
          return throwError(
            () => new BadRequestException(SALES_INVOICE_NOT_FOUND),
          );
        }
        return this.settingsService.find().pipe(
          switchMap(settings => {
            const serial_no = new Set();
            const serial_hash: { [key: string]: { serials: string[] } } = {};
            let total_qty = 0;

            serialProvider.items.forEach(element => {
              if (element.has_serial_no) {
                if (serial_hash[element.item_code]) {
                  serial_hash[element.item_code].serials.push(
                    ...element.serial_no,
                  );
                } else {
                  serial_hash[element.item_code] = {
                    serials: element.serial_no,
                  };
                }
                total_qty += element.qty;
                element.serial_no.forEach(serial => {
                  serial_no.add(serial);
                });
              }
            });

            const serial_no_array: any[] = Array.from(serial_no);

            if (!settings.defaultCompany) {
              return throwError(
                new NotImplementedException(PLEASE_SETUP_DEFAULT_COMPANY),
              );
            }

            if (total_qty !== serial_no_array.length) {
              return throwError(
                new BadRequestException(
                  this.getMessage(
                    SERIAL_SHOULD_BE_EQUAL_TO_QUANTITY,
                    total_qty,
                    serial_no_array.length,
                  ),
                ),
              );
            }

            if (serialProvider.company !== settings.defaultCompany) {
              return throwError(() => new BadRequestException(INVALID_COMPANY));
            }

            return from(Object.keys(serial_hash)).pipe(
              mergeMap(item_code => {
                const inputDatetimeStr = DateTime.fromJSDate(
                  getParsedPostingDate(serialProvider),
                )
                  .setZone(settings.timeZone)
                  .toJSDate();
                const inputDatetime = new Date(inputDatetimeStr);
                const currentDate = new Date();
                // Increment 1 hour
                if (inputDatetime < currentDate) {
                  // Set the time to the end of the day
                  inputDatetime.setHours(23, 59, 59, 999); // 23:59:59.999
                } else {
                  // Increment 1 hour
                  inputDatetime.setHours(inputDatetime.getHours() + 1);
                }
                return from(
                  this.serialNoService.count({
                    serial_no: { $in: serial_hash[item_code].serials },
                    item_code,
                    warehouse: serialProvider.set_warehouse,
                    'warranty.purchasedOn': {
                      $lte: inputDatetime,
                    },
                  }),
                ).pipe(
                  switchMap(count => {
                    if (count !== serial_hash[item_code].serials.length) {
                      return throwError(
                        new BadRequestException(
                          this.getMessage(
                            `
                      Invalid serials provided for ${item_code}
                      at warehouse : ${serialProvider.set_warehouse} `,
                            serial_hash[item_code].serials.length,
                            count,
                          ),
                        ),
                      );
                    }
                    return of(true);
                  }),
                );
              }),
              toArray(),
            );
          }),
        );
      }),
    );
  }

  validateItem(item: string[]) {
    return from(this.itemService.count({ item_code: { $in: item } })).pipe(
      switchMap((itemCount: any) => {
        if (itemCount !== item.length) {
          return throwError(
            new BadRequestException(
              this.getMessage(
                `${INVALID_ITEM}, ${INVALID_ITEM_CODE}`,
                item.length,
                itemCount,
              ),
            ),
          );
        }
        return of(true);
      }),
    );
  }

  validateItemRate(item: ItemDto) {
    return from(
      this.itemService.findOne({ where: { item_code: item.item_code } }),
    ).pipe(
      switchMap(existingItem => {
        if (existingItem.minimumPrice > item.rate) {
          return throwError(
            new BadRequestException(
              `Price for Item: ${item.item_name}, is not acceptable.`,
            ),
          );
        }
        return of(true);
      }),
    );
  }

  // non-serial-items
  validateStock(payload: AssignSerialDto) {
    return from(payload.items).pipe(
      mergeMap(item => {
        return from(
          this.stockLedgerService.listStockLedger(
            0,
            1,
            { item_code: item.item_code, warehouse: payload.set_warehouse },
            undefined,
          ),
        ).pipe(
          map((res: any) => res),
          switchMap(response => {
            // eslint-disable-next-line
            //  console.log('RES of stock ledger query', response);
            // eslint-disable-next-line
            //  console.log('RES of stock ledger query', response);
            const doc = response.docs[0];
            // eslint-disable-next-line
            //  console.log('Doc: ', doc);
            if (doc.stock_availability < item.qty) {
              return throwError(
                new BadRequestException(
                  `Insufficient stock for ${doc.item.item_name} at ${doc.warehouse}`,
                ),
              );
            }
            return of(true);
          }),
        );
      }),
      toArray(),
    );
  }

  validateDeliveryNote(assignPayload: AssignSerialDto) {
    return from(assignPayload.items).pipe(
      concatMap(item => {
        if (!item.has_serial_no) {
          return of(true);
        }
        const serials = new ValidateSerialsDto();
        serials.serials = item.serial_no;
        serials.item_code = item.item_code;
        serials.warehouse = assignPayload.set_warehouse;
        return this.validateSerials(serials).pipe(
          switchMap(data => {
            return of({
              notFoundSerials: data[0]
                ? data[0].notFoundSerials
                : serials.serials,
            });
          }),
          switchMap((data: ValidateSerialsResponse) => {
            if (data && data.notFoundSerials && data.notFoundSerials.length) {
              return throwError(
                new BadRequestException(
                  `Found ${
                    data.notFoundSerials.length
                  } Invalid Serials for item: ${item.item_code} at warehouse: ${
                    assignPayload.set_warehouse
                  }, ${data.notFoundSerials.splice(0, 50).join(', ')}...`,
                ),
              );
            }
            return of(true);
          }),
        );
      }),
      toArray(),
    );
  }

  validateSerials(payload: ValidateSerialsDto) {
    if (!payload.item_code || !payload.warehouse) {
      return throwError(
        new BadRequestException(
          'Warehouse and item_code are mandatory for delivery note serial validation.',
        ),
      );
    }
    return this.serialNoService.asyncAggregate([
      {
        $match: {
          serial_no: { $in: payload.serials },
          item_code: payload.item_code,
          $or: [
            { warehouse: payload.warehouse },
            // { 'queue_state.purchase_receipt.warehouse': payload.warehouse },
          ],
          'warranty.soldOn': { $exists: false },
          // 'queue_state.delivery_note': { $exists: false },
        },
      },
      {
        $group: {
          _id: 'validSerials',
          foundSerials: { $push: '$serial_no' },
        },
      },
      {
        $project: {
          notFoundSerials: {
            $setDifference: [payload.serials, '$foundSerials'],
          },
        },
      },
    ]);
  }

  getMessage(notFoundMessage, expected, found) {
    return `${notFoundMessage}, expected ${expected || 0} found ${found || 0}`;
  }
}
