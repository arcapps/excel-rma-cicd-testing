import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type SalesNoDocument = HydratedDocument<SerialNo>;

export class Warranty {
  purchaseWarrantyDate: string;
  salesWarrantyDate: Date;
  purchasedOn: Date;
  soldOn: Date;
}
export class QueueState {
  purchase_receipt: {
    parent: string;
    warehouse: string;
  };
  delivery_note: {
    parent: string;
    warehouse: string;
  };
  stock_entry: {
    parent: string;
    source_warehouse: string;
    target_warehouse: string;
  };
}
@Schema({ collection: 'serial_no' })
export class SerialNo {
  @Prop()
  uuid: string;

  @Prop()
  isSynced: boolean;

  @Prop()
  warranty_expiry_date: string;

  @Prop()
  modified: boolean;

  @Prop()
  name: string;

  @Prop()
  owner: string;

  @Prop()
  creation: string;

  @Prop({ index: true })
  sales_invoice_name: string;

  @Prop({ index: true, unique: true })
  serial_no: string;

  @Prop({ index: true })
  item_code: string;

  @Prop()
  item_name: string;

  @Prop()
  description: string;

  @Prop()
  item_group: string;

  @Prop()
  purchase_time: string;

  @Prop()
  purchase_rate: number;

  @Prop()
  supplier: string;

  @Prop()
  customer: string;

  @Prop({ index: true })
  warehouse: string;

  @Prop()
  delivery_note: string;

  @Prop()
  purchase_document_no: string;

  @Prop()
  sales_return_name: string;

  @Prop()
  purchase_document_type: string;

  @Prop()
  sales_document_type: string;

  @Prop()
  sales_document_no: string;

  @Prop()
  company: string;

  @Prop()
  warranty: Warranty;

  @Prop()
  purchase_date: string;

  @Prop()
  queue_state: QueueState;

  @Prop({ index: true })
  purchase_invoice_name: string;

  @Prop()
  brand: string;

  @Prop()
  claim_no: string;
}

export const SerialNoSchema = SchemaFactory.createForClass(SerialNo);
