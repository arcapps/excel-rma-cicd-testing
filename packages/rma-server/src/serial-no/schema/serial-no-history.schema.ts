import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type SalesNoHistoryDocument = HydratedDocument<SerialNoHistory>;

export enum EventType {
  SerialPurchased = 'Serial Purchased',
  SerialDelivered = 'Serial Delivered',
  SerialReturned = 'Serial Returned',
  SerialTransferCreated = 'Serial Transfer Created',
  SerialTransferAccepted = 'Serial Transfer Accepted',
  SerialTransferRejected = 'Serial Transfer Rejected',
  MaterialReceipt = 'Material Receipt',
  MaterialIssue = 'Material Issue',
  RnD_PRODUCTS = 'R&D Product',
  SENT_TO_IT_AND_NETWORK_SECTION = 'Sent to IT & Network Section',
  SENT_TO_PRINTER_IPG_SECTION = 'Sent to Printer (IPG) Section ',
  SENT_TO_SUPPORT_LENOVO_SECTION = 'Sent to Support/Lenovo Section',
  RECEIVED_FROM_CUSTOMER = 'Received from Customer',
  RECEIVED_FROM_BRANCH = 'Received from Branch',
  WORK_IN_PROGRESS = 'Work in Progress',
  TRANSFERRED = 'Transferred',
  SOLVED = 'Solved - Repairing done',
  TO_REPLACE = 'Unsolved - To Replace',
  UNSOLVED = 'Unsolved - Return to Owner',
  DELIVER_TO_CUSTOMER = 'Deliver to Customer',
  SENT_TO_ENG_DEPT = 'Sent to Eng. Dept',
  SENT_TO_REPAIR_DEPT = 'Sent to Repair Dept',
}
@Schema({ collection: 'serial_no_history' })
export class SerialNoHistory {
  @Prop()
  eventDate: Date;

  @Prop({ type: String, enum: EventType })
  eventType: EventType;

  @Prop({ index: true })
  serial_no: string;

  @Prop()
  document_no: string;

  @Prop()
  naming_series: string;

  @Prop()
  readable_document_no: string;

  @Prop()
  transaction_from: string;

  @Prop()
  transaction_to: string;

  @Prop()
  document_type: string;

  @Prop({ index: true })
  parent_document: string;

  @Prop()
  created_on: string;

  @Prop()
  created_by: string;
}

export const SerialNoHistorySchema =
  SchemaFactory.createForClass(SerialNoHistory);

export class SerialNoHistoryInterface {
  naming_series?: string;
  readable_document_no?: string;
  eventDate?: Date;
  eventType?: EventType;
  serial_no?: string;
  document_no?: string;
  transaction_from?: string;
  transaction_to?: string;
  document_type?: string;
  parent_document?: string;
  created_on?: string;
  created_by?: string;
}
