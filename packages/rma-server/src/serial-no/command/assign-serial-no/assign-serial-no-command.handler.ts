import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AssignSerialNoCommand } from './assign-serial-no.command';
import { SerialNoAggregateService } from '../../aggregates/serial-no-aggregate/serial-no-aggregate.service';
import { firstValueFrom } from 'rxjs';

@CommandHandler(AssignSerialNoCommand)
export class AssignSerialNoHandler
  implements ICommandHandler<AssignSerialNoCommand>
{
  constructor(
    private publisher: EventPublisher,
    private manager: SerialNoAggregateService,
  ) {}

  async execute(command: AssignSerialNoCommand) {
    const { assignPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await firstValueFrom(
      this.manager.assignSerial(assignPayload, clientHttpRequest),
    );
    aggregate.commit();
  }
}
