import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
// import { SerialNo } from './serial-no/serial-no.entity';
import { SerialNoService } from './serial-no/serial-no.service';
// import { SerialNoHistory } from './serial-no-history/serial-no-history.entity';
import { SerialNoHistoryService } from './serial-no-history/serial-no-history.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SerialNoSchema } from '../schema/serial-no.schema';
import { SerialNoHistorySchema } from '../schema/serial-no-history.schema';

@Module({
  // imports: [TypeOrmModule.forFeature([SerialNo, SerialNoHistory])],
  imports: [
    MongooseModule.forFeature([
      { name: 'SerialNo', schema: SerialNoSchema },
      { name: 'SerialNoHistory', schema: SerialNoHistorySchema },
    ]),
  ],
  providers: [SerialNoService, SerialNoHistoryService],
  exports: [SerialNoService, SerialNoHistoryService],
})
export class SerialNoEntitiesModule {}
