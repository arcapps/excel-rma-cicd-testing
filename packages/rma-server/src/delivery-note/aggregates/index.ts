import { DNHookAggServ } from './delivery-webhook-agg/delivery-note-webhook-aggregate.service';
import { DeliveryNoteAggregateService } from './delivery-note-aggregate/delivery-note-aggregate.service';

export const DeliveryNoteAggregatesManager = [
  DNHookAggServ,
  DeliveryNoteAggregateService,
];
