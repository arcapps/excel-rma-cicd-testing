import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type DeliveryNoteDocument = HydratedDocument<DeliveryNote>;

@Schema({ collection: 'delivery_note' })
export class DeliveryNote {
  @Prop()
  uuid: string;

  @Prop()
  sales_invoice_name: string;

  @Prop()
  isSynced: boolean;

  @Prop()
  inQueue: boolean;

  @Prop()
  name: string;

  @Prop()
  modified_by: string;

  @Prop()
  docstatus: number;

  @Prop()
  title: string;

  @Prop()
  naming_series: string;

  @Prop()
  issue_credit_note: boolean;

  @Prop()
  customer: string;

  @Prop()
  customer_name: string;

  @Prop()
  company: string;

  @Prop()
  posting_date: string;

  @Prop()
  createdBy: string;

  @Prop()
  createdByEmail: string;

  @Prop()
  contact_email: string;

  @Prop()
  posting_time: string;

  @Prop()
  is_return: boolean;

  @Prop()
  currency: string;

  @Prop()
  conversion_rate: number;

  @Prop()
  total_qty: number;

  @Prop()
  base_total: number;

  @Prop()
  base_net_total: number;

  @Prop()
  total: number;

  @Prop()
  net_total: number;

  @Prop()
  base_grand_total: number;

  @Prop()
  customer_group: string;

  @Prop()
  territory: string;

  @Prop()
  set_warehouse: string;

  @Prop()
  items: DeliveryNoteItems[];

  @Prop()
  pricing_rules: DeliveryNotePricingRules[];

  @Prop()
  packed_items: DeliveryNotePackedItems[];

  @Prop()
  taxes: DeliveryNoteTaxes[];

  @Prop()
  instructions: string;

  @Prop()
  sales_team: DeliveryNoteSalesTeam[];
  credit_note_items?: any[];
}

export const DeliveryNoteSchema = SchemaFactory.createForClass(DeliveryNote);
export class DeliveryNoteItems {
  name: string;
  item_code: string;
  item_name: string;
  description: string;
  is_nil_exempt: number;
  is_non_gst: number;
  item_group: string;
  image: string;
  qty: number;
  excel_serials: string;
  has_serial_no: number;
  conversion_factor: number;
  stock_qty: number;
  price_list_rate: number;
  warranty_date?: string;
  base_price_list_rate: number;
  rate: number;
  serial_no: string;
  amount: number;
}
export class DeliveryNoteTaxes {
  name: string;
  docstatus: number;
  charge_type: string;
  account_head: string;
  description: string;
  cost_center: string;
  rate: number;
  tax_amount: number;
  total: number;
}
export class DeliveryNotePricingRules {}
export class DeliveryNotePackedItems {}
export class DeliveryNoteSalesTeam {}
