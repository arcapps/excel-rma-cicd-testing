import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { DirectService } from '../../../direct/aggregates/direct/direct.service';
import { ItemService } from '../../../item/entity/item/item.service';
import { SalesInvoiceService } from '../../../sales-invoice/entity/sales-invoice/sales-invoice.service';
import { SerialNoHistoryService } from '../../../serial-no/entity/serial-no-history/serial-no-history.service';
import { SerialNoService } from '../../../serial-no/entity/serial-no/serial-no.service';
import { StockLedgerService } from '../../../stock-ledger/entity/stock-ledger/stock-ledger.service';
import { DataImportService } from '../../../sync/aggregates/data-import/data-import.service';
import { AgendaJobService } from '../../../sync/entities/agenda-job/agenda-job.service';
import { JsonToCSVParserService } from '../../../sync/entities/agenda-job/json-to-csv-parser.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { AGENDA_TOKEN } from '../../../system-settings/providers/agenda.provider';
import { DeliveryNoteJobHelperService } from '../../schedular/delivery-note-job-helper/delivery-note-job-helper.service';
import { DeliveryNoteJobService } from './delivery-note-job.service';

describe('DeliveryNoteJobService', () => {
  let service: DeliveryNoteJobService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DeliveryNoteJobService,
        { provide: AGENDA_TOKEN, useValue: {} },
        {
          provide: DirectService,
          useValue: {},
        },
        {
          provide: AgendaJobService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: SerialNoService,
          useValue: {},
        },
        {
          provide: SalesInvoiceService,
          useValue: {},
        },
        {
          provide: SerialNoHistoryService,
          useValue: {},
        },
        {
          provide: DeliveryNoteJobHelperService,
          useValue: {},
        },
        {
          provide: StockLedgerService,
          useValue: {},
        },
        {
          provide: ItemService,
          useValue: {},
        },
        { provide: JsonToCSVParserService, useValue: {} },
        { provide: DataImportService, useValue: {} },
        { provide: ClientTokenManagerService, useValue: {} },
      ],
    }).compile();

    service = module.get<DeliveryNoteJobService>(DeliveryNoteJobService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
