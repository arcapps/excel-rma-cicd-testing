import { Injectable } from '@nestjs/common';
// import { InjectRepository } from '@nestjs/typeorm';
// import { DeliveryNote } from './delivery-note.entity';
// import { MongoRepository } from 'typeorm';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  DeliveryNote,
  // DeliveryNote,
  DeliveryNoteDocument,
} from '../../schema/delivery-note.schema';
import { PARSE_REGEX } from '../../../constants/app-strings';
import { Observable } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class DeliveryNoteService {
  constructor(
    // @InjectRepository(DeliveryNote)
    @InjectModel('DeliveryNote')
    private deliveryNoteModel: Model<DeliveryNoteDocument>, // private readonly deliveryNoteRepository: MongoRepository<DeliveryNote>,
  ) {}

  create(deliveryNotePayload): Observable<DeliveryNote> {
    if (!deliveryNotePayload.uuid) deliveryNotePayload.uuid = uuidv4();
    const promise = new this.deliveryNoteModel(deliveryNotePayload).save();
    return new Observable<DeliveryNote>(observer => {
      promise.then(
        res => {
          observer.next(res);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  async find(query?) {
    return await this.deliveryNoteModel.find(query);
  }

  async findOne(options) {
    return await this.deliveryNoteModel.findOne(options.where);
  }

  async list(skip, take, search, sort) {
    const columns = Object.keys(this.deliveryNoteModel.schema.paths);

    const $or = this.handleSearchPatterns(columns, search);

    // const $or = columns.map(field => {
    //   const filter = {};
    //   filter[field] = nameExp;
    //   return filter;
    // });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.deliveryNoteModel
      .find(where)
      .skip(skip)
      .limit(take);

    return {
      docs: results || [],
      length: await this.deliveryNoteModel.count(where),
      offset: skip,
    };
  }

  handleSearchPatterns(columns, search) {
    const searchQuery: any[] = [];

    columns.forEach(field => {
      const fieldType = this.deliveryNoteModel.schema.paths[field].instance;
      const filter = {};

      if (fieldType === 'String') {
        filter[field] = { $regex: PARSE_REGEX(search), $options: 'i' };
      } else if (fieldType === 'Number') {
        const numericSearch = parseFloat(search);
        if (!isNaN(numericSearch)) {
          filter[field] = numericSearch;
        }
      } else if (fieldType === 'Array') {
        const arraySearch = Array.isArray(search) ? search : [search];
        filter[field] = { $in: arraySearch };
      }

      // Check if the filter object is not empty before pushing it into the array
      if (Object.keys(filter).length > 0) {
        searchQuery.push(filter);
      }
    });

    return searchQuery;
  }

  async deleteOne(query, options?): Promise<any> {
    const res = await this.deliveryNoteModel.deleteOne(query, options);
    return res;
  }

  async deleteMany(params): Promise<any> {
    return await this.deliveryNoteModel.deleteMany(params);
  }

  async updateOne(query, options?) {
    return await this.deliveryNoteModel.updateOne(query, options);
  }

  async updateMany(query, params) {
    return await this.deliveryNoteModel.updateMany(query, params);
  }

  async count() {
    return await this.deliveryNoteModel.count();
  }

  async paginate(skip: number, take: number) {
    return await this.deliveryNoteModel.find().skip(skip).limit(take);
  }
}
