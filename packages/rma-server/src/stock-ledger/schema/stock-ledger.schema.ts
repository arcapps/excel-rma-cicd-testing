import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type StockLedgerDocument = HydratedDocument<StockLedger>;

@Schema({ collection: 'stock_ledger' })
export class StockLedger {
  @Prop()
  uuid: string;

  @Prop()
  name: string;

  @Prop()
  owner: string;

  @Prop()
  creation: string;

  @Prop()
  modified: string;

  @Prop()
  modified_by: string;

  @Prop()
  idx: number;

  @Prop()
  docstatus: number;

  @Prop()
  warehouse: string;

  @Prop()
  item_code: string;

  @Prop()
  reserved_qty: number;

  @Prop()
  actual_qty: number;

  @Prop()
  ordered_qty: number;

  @Prop()
  indented_qty: number;

  @Prop()
  planned_qty: number;

  @Prop()
  projected_qty: number;

  @Prop()
  reserved_qty_for_production: number;

  @Prop()
  reserved_qty_for_sub_contract: number;

  @Prop()
  ma_rate: number;

  @Prop()
  stock_uom: string;

  @Prop()
  fcfs_rate: number;

  @Prop()
  valuation_rate: number;

  @Prop()
  balance_value: number;

  @Prop()
  balance_qty: number;

  @Prop()
  stock_value: number;

  @Prop()
  doctype: string;

  @Prop()
  batch_no: string;

  @Prop({ type: () => [Date, String] })
  posting_date: Date | string;

  @Prop()
  posting_time: string;

  @Prop()
  voucher_type: string;

  @Prop()
  voucher_no: string;

  @Prop()
  voucher_detail_no: string;

  @Prop()
  incoming_rate: number;

  @Prop()
  outgoing_rate: number;

  @Prop()
  qty_after_transaction: number;

  @Prop()
  stock_value_difference: number;

  @Prop()
  company: string;

  @Prop()
  fiscal_year: string;

  @Prop()
  is_cancelled: string;

  @Prop()
  to_rename: number;

  @Prop()
  transfer_in_id: string;
}

export const StockLedgerSchema = SchemaFactory.createForClass(StockLedger);
