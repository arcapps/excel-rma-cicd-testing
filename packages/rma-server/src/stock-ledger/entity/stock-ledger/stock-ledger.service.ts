import { Injectable } from '@nestjs/common';
// import { InjectRepository } from '@nestjs/typeorm';
import { firstValueFrom, Observable } from 'rxjs';
// import { switchMap } from 'rxjs/operators';
// import { MongoRepository } from 'typeorm';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { StockLedger } from '../../schema/stock-ledger.schema';
import { InjectModel } from '@nestjs/mongoose';
import { StockLedgerDocument } from '../../schema/stock-ledger.schema';
import { Model } from 'mongoose';
@Injectable()
export class StockLedgerService {
  constructor(
    // @InjectRepository(StockLedger)
    // private readonly stockLedgerRepository: MongoRepository<StockLedger>,
    @InjectModel('StockLedger')
    private stockLedgerModel: Model<StockLedgerDocument>,
  ) {}

  async find(query?) {
    return await this.stockLedgerModel.find(query);
  }

  async listLedgerReport(
    $skip: number,
    $limit: number,
    filter_query: any,
    sort: any,
  ) {
    let $sort: any;
    let dateQuery: any = {};
    try {
      $sort = JSON.parse(sort);
    } catch (error) {
      $sort = { posting_date: 'desc' };
    }
    $sort = Object.keys($sort).length === 0 ? { posting_date: 'desc' } : $sort;
    for (const key of Object.keys($sort)) {
      $sort[key] = $sort[key].toUpperCase();
      if ($sort[key] === 'ASC') {
        $sort[key] = 1;
      }
      if ($sort[key] === 'DESC') {
        $sort[key] = -1;
      }
      if (!$sort[key]) {
        delete $sort[key];
      }
    }

    if (filter_query?.fromDate && filter_query?.toDate) {
      dateQuery = {
        posting_date: {
          $gte: new Date(filter_query.fromDate),
          $lte: new Date(filter_query.toDate),
        },
      };
    }

    const $and: any[] = [
      filter_query ? this.getFilterQuery(filter_query) : {},
      dateQuery,
    ];

    const $match: { $and: any } = { $and };
    const $lookup: any = {
      from: 'item',
      localField: 'item_code',
      foreignField: 'item_code',
      as: 'item',
    };

    const $unwind: any = '$item';

    // const $projection = {
    //   $project: {
    //     posting_date: 1,
    //     posting_time: 1,
    //     'item.item_name': 1,
    //     'item.item_code': 1,
    //     'item.item_group': 1,
    //     voucher_no: 1,
    //     voucher_type: 1,
    //     'item.brand': 1,
    //     warehouse: 1,
    //     uoms: {
    //       $map: {
    //         input: '$item.uoms',
    //         as: 'uom',
    //         in: {
    //           uom: '$$uom.uom',
    //           conversion_factor: '$$uom.conversion_factor',
    //         },
    //       },
    //     },
    //     actual_qty: 1,
    //     incoming_rate: 1,
    //     outgoing_rate: 1,
    //     valuation_rate: 1,
    //     uuid: 1,
    //   },
    // };

    const results = await firstValueFrom(
      this.asyncAggregate([
        { $lookup },
        { $unwind },
        { $match },
        // $projection,
        { $skip },
        { $limit },
        { $sort },
      ]),
    );

    const length: any = await firstValueFrom(
      this.asyncAggregate([
        { $lookup },
        { $unwind },
        { $match },
        { $sort },
        { $count: 'total' },
      ]),
    );
    // const results = await this.stockLedgerModel.find().
    // find(filter_query).
    // sort($sort).limit($limit).skip($skip);

    return {
      docs: results || [],
      length,
      offset: $skip,
    };
  }

  asyncAggregate(query) {
    const promise = this.stockLedgerModel.aggregate(query);

    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }
  async listStockLedger(
    skip: number,
    limit: number,
    filter_query: any,
    sort: any,
  ) {
    const $group: any = {
      _id: {
        warehouse: '$warehouse',
        item_code: '$item_code',
      },
      stock_availability: {
        $sum: '$actual_qty',
      },
    };

    // Here we specify the fields which we want and use _id cause we need these from $group query

    const $project: any = {
      _id: 0,
      item_code: '$_id.item_code',
      warehouse: '$_id.warehouse',
      stock_availability: 1,
    };

    if (filter_query.zero_stock) {
      filter_query.stock_availability = { $lte: 0 };
    } else {
      filter_query.stock_availability = { $gt: 0 };
    }

    const $match: any = filter_query ? this.getFilterQuery(filter_query) : {};

    const $lookup: any = {
      from: 'item',
      localField: 'item_code',
      foreignField: 'item_code',
      as: 'item',
    };

    const $unwind: any = '$item';

    const results = await firstValueFrom(
      this.asyncAggregate([
        {
          $group: {
            _id: {
              warehouse: '$warehouse',
              item_code: '$item_code',
            },
            stock_availability: {
              $sum: '$actual_qty',
            },
          },
        },
        {
          $sort: { _id: 1 }, // Sort by the _id field in ascending order
        },
        {
          $project: {
            _id: 0,
            item_code: '$_id.item_code',
            warehouse: '$_id.warehouse',
            stock_availability: 1,
          },
        },
        {
          $lookup: {
            from: 'item',
            localField: 'item_code',
            foreignField: 'item_code',
            as: 'item',
          },
        },
        { $unwind: { path: '$item' } },
        {
          $match,
        },
        { $skip: skip },
        { $limit: limit },
      ]),
    );

    const length: any = await firstValueFrom(
      this.asyncAggregate([
        { $group },
        {
          $sort: {
            warehouse: 1,
            item_code: 1,
          },
        },
        { $project },
        { $lookup },
        { $unwind },
        { $match },
        { $count: 'total' },
      ]),
    );

    return {
      docs: results || [],
      length,
      offset: skip,
    };
  }

  async distinct(query: string, options: any) {
    return await this.stockLedgerModel.distinct(query, options);
  }

  async create(stockLedger: StockLedger) {
    if (stockLedger?.posting_date) {
      stockLedger.posting_date = new Date(stockLedger.posting_date);
      // Get the current system time
      const currentTime = new Date();

      // Set the time part of the original date to the current system time
      stockLedger.posting_date.setHours(currentTime.getHours());
      stockLedger.posting_date.setMinutes(currentTime.getMinutes());
      stockLedger.posting_date.setSeconds(currentTime.getSeconds());
    }
    const res = await new this.stockLedgerModel(stockLedger).save();
    return res;
  }

  async findOne(options) {
    return await this.stockLedgerModel.findOne(options.where);
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.stockLedgerModel.deleteOne(query, options);
  }

  async deleteMany(query, options?): Promise<any> {
    const res = await this.stockLedgerModel.deleteMany(query, options);
    return res;
  }

  async updateOne(query, options?) {
    return await this.stockLedgerModel.updateOne(query, options);
  }

  async insertMany(query, options?) {
    return await this.stockLedgerModel.insertMany(query, options);
  }

  getFilterQuery(query: any) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (typeof query[key] !== 'undefined') {
        if (key === 'item_brand') {
          // query['item.brand'] = { $regex: query[key], $options: 'i' };
          query['item.brand'] = query[key];

          delete query[key];
        } else if (key === 'item_group') {
          // query['item.item_group'] = { $regex: query[key], $options: 'i' };
          query['item.item_group'] = query[key];
          delete query[key];
        } else if (key === 'zero_stock') {
          delete query[key];
        } else if (key === 'fromDate' || key === 'toDate') {
          delete query[key];
        } else if (key === 'stock_availability') {
          query[key];
        } else {
          // query[key] = { $regex: query[key], $options: 'i' };
          query[key] = query[key];
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }
}
