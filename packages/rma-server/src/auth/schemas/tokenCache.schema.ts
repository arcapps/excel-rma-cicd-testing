import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export type TokenCacheDocument = HydratedDocument<TokenCache>;

@Schema({ collection: 'token_cache' })
export class TokenCache {
  @Prop()
  uuid: string;

  @Prop()
  accessToken: string;

  @Prop()
  active: boolean;

  @Prop()
  exp: number;

  @Prop()
  email: string;

  @Prop()
  sub: string;

  @Prop()
  scope: string[];

  @Prop()
  roles: string[];

  @Prop()
  territory: string[];

  @Prop()
  warehouses: string[];

  @Prop()
  clientId: string;

  @Prop()
  refreshToken: string;

  @Prop()
  trustedClient: boolean;

  @Prop()
  status: string;

  @Prop()
  name: string;

  @Prop()
  fullName: string;

  constructor() {
    if (!this.uuid) this.uuid = uuidv4();
  }
}

export const TokenCacheSchema = SchemaFactory.createForClass(TokenCache);
