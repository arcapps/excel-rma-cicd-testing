import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { TokenCache } from '../../schemas/tokenCache.schema';
import { Model } from 'mongoose';
@Injectable()
export class TokenCacheService {
  constructor(
    @InjectModel('TokenCache') private cacheModel: Model<TokenCache>,
  ) {}

  async save(params): Promise<TokenCache> {
    const createdToken = new this.cacheModel(params);
    return await createdToken.save(params);
  }

  async find(): Promise<TokenCache[]> {
    return await this.cacheModel.find().exec();
  }

  async findOne(options): Promise<TokenCache> {
    const res = await this.cacheModel.findOne(options.where);
    return res;
  }

  async updateMany(query, params) {
    return await this.cacheModel.updateMany(query, params);
  }

  async updateOne(query, params) {
    return await this.cacheModel.updateOne(query, params);
  }

  async count() {
    return await this.cacheModel.count();
  }

  async paginate(skip: number, take: number) {
    return await this.cacheModel.find({ skip, take });
  }

  async deleteMany(params): Promise<any> {
    return await this.cacheModel.deleteMany(params);
  }
}
