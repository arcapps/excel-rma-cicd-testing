import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { TerritoryService } from '../../../customer/entity/territory/territory.service';
import { ErrorLogService } from '../../../error-log/error-log-service/error-log.service';
import { TokenCacheService } from '../../entities/token-cache/token-cache.service';
import { ClientTokenManagerService } from '../client-token-manager/client-token-manager.service';
import { ConnectService } from './connect.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';

describe('ConnectService', () => {
  let service: ConnectService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ConnectService,
        {
          provide: TokenCacheService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
        {
          provide: ErrorLogService,
          useValue: {},
        },
        {
          provide: ServerSettingsService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: TerritoryService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ConnectService>(ConnectService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
