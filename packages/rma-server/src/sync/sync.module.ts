import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { DeliveryNoteModule } from '../delivery-note/delivery-note.module';
import { DirectModule } from '../direct/direct.module';
import { PurchaseReceiptModule } from '../purchase-receipt/purchase-receipt.module';
import { StockEntryModule } from '../stock-entry/stock-entry.module';
import { SettingsService } from '../system-settings/aggregates/settings/settings.service';
import { SyncAggregateManager } from './aggregates';
import { JobQueueController } from './controllers/job-queue/job-queue.controller';
import { FrappeJobService } from './schedular/frappe-jobs-queue/frappe-jobs-queue.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ServerSettingsSchema } from '../system-settings/schemas/server-settings.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'ServerSettings', schema: ServerSettingsSchema },
    ]),
    HttpModule,
    PurchaseReceiptModule,
    StockEntryModule,
    DirectModule,
    DeliveryNoteModule,
  ],
  controllers: [JobQueueController],
  providers: [FrappeJobService, ...SyncAggregateManager, SettingsService],
  exports: [FrappeJobService, ...SyncAggregateManager],
})
export class SyncModule {}
