// import { InjectRepository } from '@nestjs/typeorm';
// import { AgendaJob } from './agenda-job.entity';
import { Injectable } from '@nestjs/common';
// import { MongoRepository } from 'typeorm';
import {
  FRAPPE_QUEUE_JOB,
  SYSTEM_MANAGER,
  FRAPPE_JOB_SELECT_FIELDS,
  AGENDA_JOB_STATUS,
} from '../../../constants/app-strings';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { AgendaJobsDocument } from '../../schema/agenda-job.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class AgendaJobService {
  constructor(
    // @InjectRepository(AgendaJob)
    // private readonly agendaJobRepository: MongoRepository<AgendaJob>,
    @InjectModel('AgendaJobs')
    private agendaJobsModel: Model<AgendaJobsDocument>,
  ) {}

  async find(query?) {
    return await this.agendaJobsModel.find(query);
  }

  async findOne(options) {
    return await this.agendaJobsModel.findOne(options.where);
  }

  async count(query) {
    return await this.agendaJobsModel.count(query);
  }

  async updateMany(query, params) {
    return await this.agendaJobsModel.updateMany(query, params);
  }

  async findOneAndUpdate(query, params) {
    return await this.agendaJobsModel.findOneAndUpdate(query, params);
  }

  async list(
    skip,
    take,
    sort,
    token: { roles: string[]; accessToken: string; email: string },
    filter_query?,
  ) {
    let sortQuery;
    filter_query = this.getFilterQuery(filter_query);
    try {
      sortQuery = JSON.parse(sort);
    } catch (error) {
      sortQuery = { _id: 1 };
    }

    const jobFilter = {
      name: { $in: [FRAPPE_QUEUE_JOB] },
    };

    if (!token.roles.includes(SYSTEM_MANAGER)) {
      jobFilter['data.token.email'] = token.email;
    }

    const $and: any[] = [jobFilter, filter_query];

    const selectFields: any = FRAPPE_JOB_SELECT_FIELDS;

    const where: { $and: any } = { $and };

    const results = await this.agendaJobsModel
      .find()
      .where(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery)
      .select(selectFields);

    return {
      docs: results || [],
      length: await this.agendaJobsModel.count(where),
      offset: skip,
    };
  }

  updateJobTokens(expiredToken, refreshedToken) {
    this.agendaJobsModel
      .updateMany(
        {
          'data.status': AGENDA_JOB_STATUS.in_queue,
          'data.token.accessToken': expiredToken,
        },
        { $set: { 'data.token.accessToken': refreshedToken } },
      )
      .then(success => {
        success;
      })
      .catch(err => {});
  }

  async updateOne(query, params) {
    return await this.agendaJobsModel.updateOne(query, params);
  }

  async deleteOne(query): Promise<any> {
    return await this.agendaJobsModel.deleteOne(query);
  }

  async deleteMany(query): Promise<any> {
    return await this.agendaJobsModel.deleteMany(query);
  }

  getFilterQuery(query) {
    try {
      return JSON.parse(query);
    } catch {
      return {};
    }
  }
}
