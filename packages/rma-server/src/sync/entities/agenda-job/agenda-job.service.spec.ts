import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { AgendaJobService } from './agenda-job.service';

describe('AgendaJobService', () => {
  let service: AgendaJobService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AgendaJobService,
        {
          provide: getModelToken('AgendaJobs'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AgendaJobService>(AgendaJobService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
