import { Module } from '@nestjs/common';
import { ProblemService } from './problem/problem.service';
// import { TypeOrmModule } from '@nestjs/typeorm';
// import { Problem } from './problem/problem-entity';
import { MongooseModule } from '@nestjs/mongoose';
import { ProblemSchema } from '../schema/problem.schema';

@Module({
  imports: [
    // TypeOrmModule.forFeature([Problem]),
    MongooseModule.forFeature([{ name: 'Problem', schema: ProblemSchema }]),
  ],
  providers: [ProblemService],
  exports: [ProblemService],
})
export class ProblemEntitiesModule {}
