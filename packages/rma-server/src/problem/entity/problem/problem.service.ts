import { Injectable } from '@nestjs/common';
import { Problem } from './problem-entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ProblemDocument } from '../../schema/problem.schema';

@Injectable()
export class ProblemService {
  constructor(
    @InjectModel('Problem')
    private problemModel: Model<ProblemDocument>,
  ) {}

  async create(problemPayload: Problem) {
    return await new this.problemModel(problemPayload).save();
  }

  async findOne(options) {
    return await this.problemModel.findOne(options.where);
  }

  async deleteOne(query, param?): Promise<any> {
    return await this.problemModel.deleteOne(query, param);
  }

  async updateOne(query, param) {
    return await this.problemModel.updateOne(query, param);
  }

  async list(skip, take, search, sort) {
    const sortQuery = { problem_name: sort };

    const nameExp = new RegExp(search, 'i');

    const $or = [{ problem_name: nameExp }];

    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.problemModel
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);
    const totalCount = await this.problemModel.countDocuments(where);

    return {
      docs: results || [],
      length: totalCount,
      offset: skip,
    };
  }
}
