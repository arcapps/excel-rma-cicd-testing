import { BaseEntity, Entity, ObjectIdColumn, ObjectId, Column } from 'typeorm';

@Entity()
export class Problem extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectId;

  @Column()
  uuid: string;

  @Column()
  problem_name: string;
}
