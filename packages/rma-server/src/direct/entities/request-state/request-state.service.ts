import { Injectable } from '@nestjs/common';
// import { InjectRepository } from '@nestjs/typeorm';
// import { MongoRepository } from 'typeorm';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
// import { DEFAULT } from '../../../constants/typeorm.connection';
import { RequestState } from './request-state.entity';
import { RequestStateDocument } from '../../schema/request-state.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class RequestStateService {
  constructor(
    // @InjectRepository(RequestState, DEFAULT)
    // private readonly requestStateRepository: MongoRepository<RequestState>,
    @InjectModel('RequestState')
    private requestModel: Model<RequestStateDocument>,
  ) {}

  async save(params) {
    return await new this.requestModel(params).save();
  }

  async find(): Promise<RequestState[]> {
    return await this.requestModel.find();
  }

  async findOne(options) {
    return await this.requestModel.findOne(options.where);
  }

  async findAndModify(query, update, options?) {
    return await this.requestModel.findOneAndUpdate(query, update);
  }

  async count() {
    return await this.requestModel.count();
  }

  async paginate(skip: number, take: number) {
    return await this.requestModel.find().skip(skip).limit(take);
  }

  async deleteMany(params): Promise<any> {
    return await this.requestModel.deleteMany(params);
  }

  async deleteOne(uuid): Promise<any> {
    return await this.requestModel.deleteOne({ uuid });
  }
}
