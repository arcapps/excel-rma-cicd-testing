import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { RequestStateService } from './request-state.service';

describe('RequestStateService', () => {
  let service: RequestStateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RequestStateService,
        {
          provide: getModelToken('RequestState'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<RequestStateService>(RequestStateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
