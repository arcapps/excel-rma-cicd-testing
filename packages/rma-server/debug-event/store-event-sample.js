const EventStore = require('geteventstore-promise');
const eventData = require('./event-sample.json');
const client = new EventStore.TCPClient({
  hostname: 'localhost',
  port: 1113,
  credentials: {
    username: 'admin',
    password: 'changeit',
  },
});

client
  .writeEvent(
    eventData.testStream,
    eventData.testEventPattern,
    eventData.payload,
  )
  .then(() => client.getEvents(eventData.testStream, undefined, 1, 'backward'))
  .then(events => {})
  .catch(error => {})
  .finally(() =>
    client
      .close()
      .then(closed => {})
      .catch(error => {}),
  );
