import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MatSnackBar } from '@angular/material/snack-bar';
import { STORAGE_TOKEN } from '../../../api/storage/storage.service';
import { MaterialModule } from '../../../material/material.module';
import { AssignClaimsComponent } from './assign-claims.component';

describe('AssignClaimsComponent', () => {
  let component: AssignClaimsComponent;
  let fixture: ComponentFixture<AssignClaimsComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AssignClaimsComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        imports: [MaterialModule],
        providers: [
          {
            provide: MatSnackBar,
            useValue: {},
          },
          {
            provide: STORAGE_TOKEN,
            useValue: {},
          },
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
