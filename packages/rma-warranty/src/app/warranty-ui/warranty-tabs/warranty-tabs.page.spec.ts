import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Location } from '@angular/common';
import { STORAGE_TOKEN } from '../../api/storage/storage.service';
import { WarrantyTabsPage } from './warranty-tabs.page';

describe('WarrantyTabsPage', () => {
  let component: WarrantyTabsPage;
  let fixture: ComponentFixture<WarrantyTabsPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [WarrantyTabsPage],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: Location,
            useValue: {},
          },
          { provide: STORAGE_TOKEN, useValue: {} },
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantyTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
