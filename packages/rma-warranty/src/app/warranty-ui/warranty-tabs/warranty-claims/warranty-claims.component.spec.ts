import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { MaterialModule } from '../../../material/material.module';
import { WarrantyService } from '../warranty.service';
import { WarrantyClaimsComponent } from './warranty-claims.component';

describe('WarrantyClaimsComponent', () => {
  let component: WarrantyClaimsComponent;
  let fixture: ComponentFixture<WarrantyClaimsComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [WarrantyClaimsComponent],
        imports: [MaterialModule, FormsModule, BrowserAnimationsModule],
        providers: [
          {
            provide: WarrantyService,
            useValue: {
              findModels: (...args) => of({}),
            },
          },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantyClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
