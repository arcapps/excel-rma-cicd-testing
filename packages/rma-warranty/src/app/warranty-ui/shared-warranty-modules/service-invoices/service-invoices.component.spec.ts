import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { STORAGE_TOKEN } from '../../../api/storage/storage.service';
import { MaterialModule } from '../../../material/material.module';
import { AddServiceInvoiceService } from './add-service-invoice/add-service-invoice.service';
import { ServiceInvoicesComponent } from './service-invoices.component';

describe('ServiceInvoicesComponent', () => {
  let component: ServiceInvoicesComponent;
  let fixture: ComponentFixture<ServiceInvoicesComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ServiceInvoicesComponent],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          NoopAnimationsModule,
          RouterTestingModule.withRoutes([]),
        ],
        providers: [
          {
            provide: AddServiceInvoiceService,
            useValue: {
              getServiceInvoiceList: (...args) => of([{}]),
              updateDocStatus: (...args) => of([{}]),
            },
          },
          {
            provide: STORAGE_TOKEN,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(ServiceInvoicesComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
