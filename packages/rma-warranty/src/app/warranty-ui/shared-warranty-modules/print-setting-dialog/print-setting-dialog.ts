import { Component, Inject } from '@angular/core';
import {
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { startWith } from 'rxjs/operators';
import { ValidateInputSelected } from '../../../common/pipes/validators';
import { WarrantyService } from '../../warranty-tabs/warranty.service';

@Component({
  selector: 'print-setting-dialog',
  templateUrl: 'print-setting-dialog.html',
})
export class PrintSettingDialog {
  printForm: UntypedFormGroup;
  printFormatList: any[];
  validateInput: any = ValidateInputSelected;
  constructor(
    public dialogRef: MatDialogRef<PrintSettingDialog>,
    public warrantyService: WarrantyService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {}

  ngOnInit() {
    this.printForm = new UntypedFormGroup({
      printFormat: new UntypedFormControl('', Validators.required),
    });

    this.printForm
      .get('printFormat')
      .valueChanges.pipe(startWith(''), this.warrantyService.getPrintFormats())
      .subscribe(res => (this.printFormatList = res));
  }

  get formControls() {
    return this.printForm.controls;
  }

  getPrintFormatOption(option) {
    if (option) {
      if (option.name) {
        return `${option.name}`;
      }
      return option.name;
    }
  }

  printDocument() {
    this.dialogRef.close(this.formControls.printFormat.value);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
