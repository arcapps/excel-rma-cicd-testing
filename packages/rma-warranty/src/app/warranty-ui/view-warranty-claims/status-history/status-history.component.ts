/* eslint-disable dot-notation */
import { Component, OnInit, Input } from '@angular/core';
import {
  WarrantyClaimsDetails,
  StatusHistoryDetails,
} from '../../../common/interfaces/warranty.interface';
import {
  Validators,
  UntypedFormControl,
  UntypedFormGroup,
} from '@angular/forms';
import { debounceTime, startWith } from 'rxjs/operators';
import { StatusHistoryService } from './status-history.service';
import { TimeService } from '../../../api/time/time.service';
import {
  CLOSE,
  CURRENT_STATUS_VERDICT,
  DELIVERY_STATUS,
  DURATION,
} from '../../../constants/app-string';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  STATUS_HISTORY_ADD_FAILURE,
  STATUS_HISTORY_REMOVE_FAILURE,
} from '../../../constants/messages';
import { StatusHistoryDataSource } from './status-history-datasource';
import { WarrantyService } from '../../warranty-tabs/warranty.service';

@Component({
  selector: 'status-history',
  templateUrl: './status-history.component.html',
  styleUrls: ['./status-history.component.scss'],
})
export class StatusHistoryComponent implements OnInit {
  @Input()
  warrantyObject: WarrantyClaimsDetails;
  statusHistoryForm = new UntypedFormGroup({
    posting_time: new UntypedFormControl('', [Validators.required]),
    posting_date: new UntypedFormControl('', [Validators.required]),
    status_from: new UntypedFormControl('', [Validators.required]),
    transfer_branch: new UntypedFormControl(''),
    current_status_verdict: new UntypedFormControl('', [Validators.required]),
    description: new UntypedFormControl(''),
    delivery_status: new UntypedFormControl(''),
  });
  territoryList: any[] = [];
  territory: any[] = [];
  currentStatus: any[] = [];
  deliveryStatus: any[] = [];
  dataSource: StatusHistoryDataSource;
  date: { date: string; time: string };
  defaultBranch: string = null;

  displayedColumns = [
    'posting_date',
    'status_from',
    'transfer_branch',
    'verdict',
    'description',
    'delivery_status',
    'status',
    'rollback',
  ];
  constructor(
    private readonly statusHistoryService: StatusHistoryService,
    private readonly time: TimeService,
    private readonly snackbar: MatSnackBar,
    private readonly warrantyService: WarrantyService,
  ) {}

  get f() {
    return this.statusHistoryForm.controls;
  }

  ngOnInit() {
    this.dataSource = new StatusHistoryDataSource(this.statusHistoryService);
    this.getTerritoryList();
    Object.keys(CURRENT_STATUS_VERDICT).forEach(verdict =>
      this.currentStatus.push(CURRENT_STATUS_VERDICT[verdict]),
    );
    Object.keys(DELIVERY_STATUS).forEach(status =>
      this.deliveryStatus.push(DELIVERY_STATUS[status]),
    );
    this.resetWarrantyDetail(this.warrantyObject?.uuid);
    this.dataSource.loadItems(this.warrantyObject?.uuid);
    this.f.transfer_branch.disable();
    this.f.transfer_branch.updateValueAndValidity();
    this.f.delivery_status.disable();
    this.f.delivery_status.updateValueAndValidity();
    this.setDefaultTerritory();
  }

  setDefaultTerritory() {
    this.warrantyService.getDefaultTerritory().subscribe({
      next: (data: any[]) => {
        if (data && data.length) {
          if (data[0].for_value) {
            this.f.status_from.setValue(data[0].for_value);
            this.defaultBranch = data[0].for_value;
          }
        }
      },
      error: () => {},
    });
  }

  getTerritoryList() {
    this.f.transfer_branch.valueChanges
      .pipe(
        debounceTime(500),
        startWith(''),
        this.statusHistoryService.getTerritoryList(),
      )
      .subscribe(res => (this.territoryList = res));

    this.statusHistoryService
      .getStorage()
      .getItem('territory')
      .then((territory: any) => {
        this.territory = territory.split(',');
        this.f.status_from.setValue(this.territory[0]);
      });
    this.selectedPostingDate({ value: new Date() });
  }

  getBranchOption(option) {
    if (option) return option.name;
  }

  async selectedPostingDate($event) {
    this.date = await this.time.getDateAndTime($event.value);
    this.f.posting_date.setValue(this.date.date);
    this.f.posting_time.setValue(
      (await this.time.getDateAndTime(new Date())).time,
    );
  }

  addStatusHistory() {
    const statusHistoryDetails = {} as StatusHistoryDetails;
    statusHistoryDetails.uuid = this.warrantyObject.uuid;
    statusHistoryDetails.time = this.f.posting_time.value;
    // statusHistoryDetails.posting_date = this.f.posting_date.value;
    statusHistoryDetails.posting_date = new Date(this.f.posting_date.value);
    statusHistoryDetails.status_from = this.f.status_from.value;
    statusHistoryDetails.verdict = this.f.current_status_verdict.value;
    statusHistoryDetails.description = this.f.description.value;
    statusHistoryDetails.delivery_status = this.f.delivery_status.value;
    this.time.getDateAndTime(new Date()).then(dateTime => {
      statusHistoryDetails.date = dateTime.date;
      this.statusHistoryService
        .getStorage()
        .getItem('territory')
        .then(territory => {
          statusHistoryDetails.delivery_branch =
            this.defaultBranch || territory?.split(',')[0];
        });
      if (
        this.f.current_status_verdict.value ===
        CURRENT_STATUS_VERDICT.TRANSFERRED
      ) {
        statusHistoryDetails.transfer_branch = this.f.transfer_branch.value.name;
        statusHistoryDetails.transfer_branch = this.f.transfer_branch.value[
          'name'
        ];
      }
      this.statusHistoryService
        .addStatusHistory(statusHistoryDetails)
        .subscribe({
          next: () => {
            this.dataSource.loadItems(this.warrantyObject?.uuid);
            this.resetWarrantyDetail(this.warrantyObject?.uuid);
            this.setInitialFormValue();
          },
          error: ({ message }) => {
            if (!message) message = STATUS_HISTORY_ADD_FAILURE;
            this.snackbar.open(message, CLOSE, {
              duration: DURATION,
            });
          },
        });
    });
  }

  setInitialFormValue() {
    this.statusHistoryForm.reset();
    this.setDefaultTerritory();
    // this.getTerritoryList();
  }

  resetWarrantyDetail(uuid: string) {
    this.statusHistoryService.getWarrantyDetail(uuid).subscribe({
      next: res => {
        this.warrantyObject = res;
      },
    });
  }

  getDate(date: string) {
    return new Date(date);
  }
  timeFunc(timeString: string): string {
    const time = new Date('1970-01-01T' + timeString);
    return time.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
  }

  removeRow() {
    this.statusHistoryService
      .removeStatusHistory(this.warrantyObject.uuid)
      .subscribe({
        next: () => {
          this.dataSource.loadItems(this.warrantyObject?.uuid);
          this.resetWarrantyDetail(this.warrantyObject?.uuid);
        },
        error: ({ message }) => {
          if (!message) message = STATUS_HISTORY_REMOVE_FAILURE;
          this.snackbar.open(message, CLOSE, {
            duration: DURATION,
          });
        },
      });
  }

  selectedCurrentStatus(option: any) {
    switch (option) {
      case CURRENT_STATUS_VERDICT.TRANSFERRED:
        this.f.transfer_branch.setValidators(Validators.required);
        this.f.transfer_branch.enable();
        this.f.transfer_branch.updateValueAndValidity();
        this.f.delivery_status.disable();
        this.f.delivery_status.updateValueAndValidity();
        break;
      case CURRENT_STATUS_VERDICT.DELIVER_TO_CUSTOMER:
        this.f.delivery_status.setValidators(Validators.required);
        this.f.transfer_branch.disable();
        this.f.transfer_branch.updateValueAndValidity();
        this.f.delivery_status.enable();
        this.f.delivery_status.updateValueAndValidity();
      default:
        this.f.transfer_branch.clearValidators();
        this.f.transfer_branch.updateValueAndValidity();
        break;
    }
  }

  check() {
    return this.dataSource?.data?.length
      ? this.dataSource.data[this.dataSource.data.length - 1]?.verdict ===
        CURRENT_STATUS_VERDICT.DELIVER_TO_CUSTOMER
        ? true
        : false
      : false;
  }
}
