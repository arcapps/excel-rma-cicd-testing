import { UntypedFormControl } from '@angular/forms';

export function ValidateInputSelected(
  formControl: UntypedFormControl,
  data: any,
) {
  if (!data) return;

  if (typeof formControl.value === 'object') {
    return true;
  }
  if (typeof data === 'object' && data.observers) {
    data.subscribe(data => {
      if (data?.includes(formControl.value)) {
        formControl.setErrors(null);
        return true;
      }

      if (typeof data[0] !== 'string') {
        let result = false;
        data.forEach(obj => {
          if (Object.keys(obj).find(key => obj[key] === formControl.value)) {
            result = true;
          }
        });

        if (result) {
          formControl.setErrors({ falseValse: null });
          formControl.updateValueAndValidity();
          return;
        }
      }
      return;
    });
  } else {
    if (data?.includes(formControl.value)) {
      return true;
    }

    if (typeof data[0] !== 'string') {
      let result = false;
      data.forEach(obj => {
        if (Object.keys(obj).find(key => obj[key] === formControl.value)) {
          result = true;
        }
      });

      if (result) {
        formControl.setErrors({ falseValse: null });
        formControl.updateValueAndValidity();
        return;
      }
    }
  }

  formControl.setErrors({ falseValse: formControl.value });
}
