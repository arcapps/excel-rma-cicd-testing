import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule } from '@ionic/angular';
import { EMPTY, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { RouterTestingModule } from '@angular/router/testing';
import { StorageService } from '../../api/storage/storage.service';
import { SerialsService } from '../../common/helpers/serials/serials.service';
import { MaterialModule } from '../../material/material.module';
import { SerialSearchPage } from './serial-search.page';
import { SerialSearchService } from './serial-search.service';

describe('SerialSearchPage', () => {
  let component: SerialSearchPage;
  let fixture: ComponentFixture<SerialSearchPage>;
  let serialSearchService: jasmine.SpyObj<SerialSearchService>;

  beforeEach(
    waitForAsync(() => {
      serialSearchService = jasmine.createSpyObj([
        'getSerialsList',
        'getItemList',
        'relayDocTypeOperation',
      ]);
      serialSearchService.getSerialsList.and.returnValue(EMPTY);
      serialSearchService.getItemList.and.returnValue(of([]));
      serialSearchService.relayDocTypeOperation.and.returnValue(
        switchMap(() => EMPTY),
      );

      TestBed.configureTestingModule({
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          ReactiveFormsModule,
          FormsModule,
          MaterialModule,
          BrowserAnimationsModule,
          RouterTestingModule,
        ],
        declarations: [SerialSearchPage],
        providers: [
          {
            provide: SerialSearchService,
            useValue: serialSearchService,
          },
          { provide: StorageService, useValue: {} },
          { provide: SerialsService, useValue: {} },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(SerialSearchPage);
      component = fixture.componentInstance;
      // Note : you can call fixture.detectChanges() as per needed on individual test's
      // this component has updated form values hence it cannot be called as default.
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
