import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { EMPTY } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { StorageService } from '../../api/storage/storage.service';
import { MaterialModule } from '../../material/material.module';
import { SerialSearchService } from '../serial-search/serial-search.service';
import { SerialInfoPage } from './serial-info.page';

describe('SerialInfoPage', () => {
  let component: SerialInfoPage;
  let fixture: ComponentFixture<SerialInfoPage>;
  let serialSearchService: jasmine.SpyObj<SerialSearchService>;

  beforeEach(
    waitForAsync(() => {
      serialSearchService = jasmine.createSpyObj([
        'getSerialsList',
        'relayDocTypeOperation',
        'getSerialData',
      ]);
      serialSearchService.getSerialsList.and.returnValue(EMPTY);
      serialSearchService.relayDocTypeOperation.and.returnValue(
        switchMap(() => EMPTY),
      );
      serialSearchService.getSerialData.and.returnValue(EMPTY);

      TestBed.configureTestingModule({
        declarations: [SerialInfoPage],
        imports: [IonicModule.forRoot(), RouterTestingModule, MaterialModule],
        providers: [
          {
            provide: SerialSearchService,
            useValue: {
              getSerialsList: (...args) => EMPTY,
              relayDocTypeOperation: doctype => EMPTY,
              getSerialData: serialNo => EMPTY,
            },
          },
          { provide: StorageService, useValue: {} },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(SerialInfoPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
