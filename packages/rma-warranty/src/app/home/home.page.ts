import { Component, OnInit } from '@angular/core';
import { LOGGED_IN } from '../constants/storage';
import { SET_ITEM, StorageService } from '../api/storage/storage.service';
import { AppService } from '../app.service';
import { TokenService } from '../auth/token/token.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  loggedIn: boolean;
  picture: string;
  state: string;
  email: string;
  name: string;
  spinner = true;

  constructor(
    private readonly location: Location,
    private readonly storageService: StorageService,
    private readonly appService: AppService,
    private readonly tokenService: TokenService,
  ) {}

  ngOnInit() {
    this.storageService.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
      if (this.loggedIn) {
        this.loadProfile();
        this.appService.getGlobalDefault();
      }
      this.spinner = false;
    });

    this.storageService.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
          if (this.loggedIn) {
            this.loadProfile();
          }
        }
      },
      error: () => {},
    });
  }

  loadProfile() {
    this.tokenService.loadProfile().subscribe({
      next: profile => {
        this.email = profile.email;
        this.name = profile.name;
        this.picture = profile.picture;
        this.spinner = false;
      },
      error: () => {},
    });
  }
  openSerialSearch() {
    const url = '/serial-info-search'; // Update with the correct route URL
    const newTab = window.open();
    newTab.location.href = this.location.prepareExternalUrl(url);
  }
}
