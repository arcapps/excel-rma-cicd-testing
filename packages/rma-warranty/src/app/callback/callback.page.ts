import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from '../auth/token/token.service';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.page.html',
  styleUrls: ['./callback.page.scss'],
})
export class CallbackPage implements OnInit {
  constructor(private router: Router, private tokenService: TokenService) {}

  ngOnInit() {
    const url = location.href;
    Promise.resolve(this.tokenService.processCode(url)).then(() =>
      this.router.navigate(['/home']),
    );
  }
}
