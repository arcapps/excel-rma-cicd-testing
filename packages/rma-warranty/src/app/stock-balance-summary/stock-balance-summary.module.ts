import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StockBalanceSummaryPageRoutingModule } from './stock-balance-summary-routing.module';

import { StockBalanceSummaryPage } from './stock-balance-summary.page';
import { MaterialModule } from '../material/material.module';
import { StockBalanceSummaryService } from './services/stock-balance-summary/stock-balance-summary.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StockBalanceSummaryPageRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  providers: [StockBalanceSummaryService],
  declarations: [StockBalanceSummaryPage],
})
export class StockBalanceSummaryPageModule {}
