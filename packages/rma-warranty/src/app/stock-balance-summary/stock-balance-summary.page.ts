import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { StockBalanceSummaryDataSource } from './stock-balance-summary-datasource';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { StockBalanceSummaryService } from './services/stock-balance-summary/stock-balance-summary.service';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DURATION } from '../constants/app-string';
// import { Observable, debounceTime, startWith, switchMap } from 'rxjs';
// import { ValidateInputSelected } from '../common/pipes/validators';
import { Observable, debounceTime, startWith, switchMap } from 'rxjs';

@Component({
  selector: 'app-stock-balance-summary',
  templateUrl: './stock-balance-summary.page.html',
  styleUrls: ['./stock-balance-summary.page.scss'],
})
export class StockBalanceSummaryPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: StockBalanceSummaryDataSource;
  displayedColumns = ['sr_no', 'item_code', 'total_inward', 'total_outward'];
  search: any;
  range = new FormGroup({
    start_date: new FormControl([Validators.required]),
    end_date: new FormControl([Validators.required]),
    item_code: new FormControl(''),
    warehouse: new FormControl(''),
  });
  warehouseList: Observable<string[]>;
  filteredItemList: Observable<string[]>;

  constructor(
    private location: Location,
    private snackBar: MatSnackBar,
    private readonly stockBalanceSummaryService: StockBalanceSummaryService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.search = '';
    this.dataSource = new StockBalanceSummaryDataSource(
      this.stockBalanceSummaryService,
    );
    if (this.range.hasError) {
      this.snackBar.open(
        'Please select dates to get stock summary records',
        'Close',
        { duration: DURATION },
      );
    }
    this.warehouseList = this.range.controls.warehouse.valueChanges.pipe(
      debounceTime(500),
      startWith(''),
      switchMap(value => {
        return this.stockBalanceSummaryService.relayDocTypeOperation(
          'Warehouse',
          value,
        );
      }),
    );
    this.filteredItemList = this.range.controls.item_code.valueChanges.pipe(
      debounceTime(500),
      startWith(''),
      switchMap(value =>
        this.stockBalanceSummaryService.getItemList(
          value,
          undefined,
          undefined,
          undefined,
          { bundle_items: { $exists: false }, has_serial_no: 1 },
        ),
      ),
    );
  }

  navigateBack() {
    this.location.back();
  }

  setFilter(event?) {
    this.search = JSON.stringify({
      start_date: this.range.controls.start_date.value,
      end_date: this.range.controls.end_date.value,
    });
    this.dataSource.loadItems(
      this.search,
      this.sort.direction,
      event?.pageIndex || 0,
      event?.pageSize || 30,
    );
  }
  openSerialSearch() {
    const url = '/serial-info-search'; // Update with the correct route URL
    window.open(url, '_blank');
  }
  // Developer Achem
  stockBalanceFilter() {
    const filterShowHideID = document.getElementById('stockBalanceFilter');
    const StockFilter = document.getElementById('StockFilter');
    const StockClose = document.getElementById('StockClose');
    if (filterShowHideID.classList.contains('active')) {
      filterShowHideID.classList.remove('active');
      StockFilter.style.display = 'block';
      StockClose.style.display = 'none';
    } else {
      filterShowHideID.classList.add('active');
      StockFilter.style.display = 'none';
      StockClose.style.display = 'block';
    }
  }
}
